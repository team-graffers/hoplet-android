package com.hoplett.app.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.hoplett.app.R;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.fragments.byproduct.FragFilter;
import com.hoplett.app.fragments.byproduct.FragProductDetail;
import com.hoplett.app.fragments.byproduct.FragStoreList;
import com.hoplett.app.fragments.bystore.FragConfirmVisit;

public class ByProductActivity extends AppCompatActivity {

	SharedPreferences preferences;
	SharedPreferences.Editor editor;

	FragmentManager manager;
	FragmentTransaction transaction;

	FragFilter fragFilter;
	FragProductDetail fragProductDetail;
	FragStoreList fragStoreList;
	FragConfirmVisit fragConfirmVisit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		/*this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
		setContentView(R.layout.activity_by_product);

		manager = getSupportFragmentManager();
		transaction = manager.beginTransaction();

		preferences = getSharedPreferences(GlobalDataCls.PreferencesName, MODE_PRIVATE);
		editor = preferences.edit();

		fragFilter = new FragFilter();
		fragConfirmVisit = new FragConfirmVisit();

		fragProductDetail = new FragProductDetail();
		fragStoreList = new FragStoreList();


		if (preferences.getString(GlobalDataCls.FragmentName, null) != null && preferences.getString(GlobalDataCls.FragmentName, null).equals("FragProductDetail")){

			editor.remove(GlobalDataCls.FragmentName);
			editor.commit();
			transaction.add(R.id.by_product_layout, fragProductDetail);
			transaction.commit();
		}else if (preferences.getString(GlobalDataCls.FragmentName, null) != null && preferences.getString(GlobalDataCls.FragmentName, null).equals("FragFilter")){

			editor.remove(GlobalDataCls.FragmentName);
			editor.commit();
			transaction.add(R.id.by_product_layout, fragFilter);
			transaction.commit();
		}else if (preferences.getString(GlobalDataCls.FragmentName, null) != null && preferences.getString(GlobalDataCls.FragmentName, null).equals("FragConfirmVisit")){

			editor.remove(GlobalDataCls.FragmentName);
			editor.commit();
			transaction.add(R.id.by_product_layout, fragConfirmVisit);
			transaction.commit();
		}else if (preferences.getString(GlobalDataCls.SignInFragName, null) != null && preferences.getString(GlobalDataCls.SignInFragName, null).equals("FragStoreList")){

			editor.remove(GlobalDataCls.SignInFragName);
			editor.commit();

			transaction.add(R.id.by_product_layout, fragStoreList);
			transaction.commit();
		}else {

			editor.remove(GlobalDataCls.FragmentName);
			editor.commit();
			transaction.add(R.id.by_product_layout, fragFilter);
			transaction.commit();
		}
	}

	@Override
	public void onBackPressed() {

		if (fragStoreList.isResumed()){
			transaction = manager.beginTransaction();
			transaction.replace(R.id.by_product_layout, fragProductDetail);
			transaction.commit();
		}else if (fragConfirmVisit.isResumed()){

			editor.putString(GlobalDataCls.FragmentName, "FragConfirm");
			editor.commit();

			Intent intent = new Intent(this, HopletHome.class);
			startActivity(intent);
		}else if (fragProductDetail.isResumed()){

			 /*if (preferences.getString(GlobalDataCls.previousFragmentName, null) != null && preferences.getString(GlobalDataCls.previousFragmentName, null).equals("FragSearchProduct")){
			 	 editor.remove(GlobalDataCls.previousFragmentName);
					editor.putString(GlobalDataCls.FragmentName, "FragSearchProduct");
			 }else {
					editor.putString(GlobalDataCls.FragmentName, "FragCategoryByProduct");
			 }
			 editor.commit();

			 Intent intent = new Intent(this, HopletHome.class);
			 intent.addCategory(Intent.CATEGORY_HOME);
			 intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			 startActivity(intent);*/

			 super.onBackPressed();
		}else if (fragFilter.isResumed()){
			super.onBackPressed();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

	}

	@Override
	protected void onResume() {
		super.onResume();
		editor.putString(GlobalDataCls.FragmentName, "FragCategoryByProduct");
		editor.commit();
	}

	public void startStoreListFragment() {

		transaction = manager.beginTransaction();
		transaction.replace(R.id.by_product_layout, fragStoreList);
		transaction.commit();
	}

	public void startProductDetailFragment() {
		transaction = manager.beginTransaction();
		transaction.replace(R.id.by_product_layout, fragProductDetail);
		transaction.commit();
	}
}
