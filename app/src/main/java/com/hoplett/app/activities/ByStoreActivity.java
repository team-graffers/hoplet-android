package com.hoplett.app.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.hoplett.app.R;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.fragments.bystore.FragConfirmVisit;
import com.hoplett.app.fragments.bystore.FragProductDetailByStore;
import com.hoplett.app.fragments.bystore.FragProductList;

public class ByStoreActivity extends AppCompatActivity {

	FragmentManager manager;
	FragmentTransaction transaction;

	SharedPreferences preferences;
	SharedPreferences.Editor editor;

	FragProductList fragProductList;
	FragProductDetailByStore fragProductDetailByStore;
	FragConfirmVisit fragConfirmVisit;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_by_store);

		preferences = getSharedPreferences(GlobalDataCls.PreferencesName, MODE_PRIVATE);
		editor = preferences.edit();

		fragProductList = new FragProductList();
		fragProductDetailByStore = new FragProductDetailByStore();
		fragConfirmVisit = new FragConfirmVisit();

		manager = getSupportFragmentManager();
		transaction = manager.beginTransaction();

		if (preferences.getString(GlobalDataCls.FragmentName, null) != null && preferences.getString(GlobalDataCls.FragmentName, null).equals("FragProductList")){

			editor.remove(GlobalDataCls.FragmentName);
			editor.commit();
			transaction.add(R.id.by_store_layout, fragProductList);
			transaction.commit();
		}else if (preferences.getString("startVisitCnfmFrag", null) != null && preferences.getString("startVisitCnfmFrag", null).equals("startVisitCnfmFrag")){

			editor.remove("startVisitCnfmFrag");
			editor.commit();
			transaction.add(R.id.by_store_layout, fragConfirmVisit);
			transaction.commit();
		}else if (preferences.getString(GlobalDataCls.SignInFragName, null) != null){
			 transaction.add(R.id.by_store_layout, fragProductDetailByStore);
			 transaction.commit();
		}
	}

	@Override
	public void onBackPressed() {
		if (fragProductDetailByStore.isResumed()){
			startProductListFragment();
		}else if (fragConfirmVisit.isResumed()){
			SharedPreferences preferences = getSharedPreferences(GlobalDataCls.PreferencesName,
							Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = preferences.edit();

			editor.putString(GlobalDataCls.FragmentName, "FragConfirm");
			editor.commit();

			Intent intent = new Intent(this, HopletHome.class);
			startActivity(intent);
		}else {
			super.onBackPressed();
		}
	}

	public void startProductListFragment(){
		transaction = manager.beginTransaction();
		transaction.replace(R.id.by_store_layout, fragProductList);
		transaction.commit();
	}

	public void startProductDetailFragment(){
		transaction = manager.beginTransaction();
		transaction.replace(R.id.by_store_layout, fragProductDetailByStore);
		transaction.commit();
	}

	 public void startVisitCnfmFrag() {
			transaction = manager.beginTransaction();
			transaction.replace(R.id.by_store_layout, fragConfirmVisit);
			transaction.commit();
	 }
}
