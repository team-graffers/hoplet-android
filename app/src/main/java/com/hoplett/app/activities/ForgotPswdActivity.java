package com.hoplett.app.activities;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.hoplett.app.R;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class ForgotPswdActivity extends AppCompatActivity {

    ImageButton backBtn;
    Button sendOtpBtn, continueShoppingBtn, submitBtn, btnResendOtp;
    LinearLayout layout, layout2, layout3;
    EditText emailForgotPswd, otp, otp2, otp3;
    EditText pswd, pswdCnfm;
    TextView otpInstruction;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pswd);

        preferences = getSharedPreferences(GlobalDataCls.PreferencesName, MODE_PRIVATE);
        editor = preferences.edit();

        backBtn = (ImageButton) findViewById(R.id.back_btn_forgot_pswd);
        sendOtpBtn = (Button) findViewById(R.id.send_otp_btn);
        continueShoppingBtn = (Button) findViewById(R.id.continue_shopping_btn);
        submitBtn = (Button) findViewById(R.id.continue__btn);
        btnResendOtp = (Button) findViewById(R.id.resend_otp_btn);

        layout = (LinearLayout) findViewById(R.id.lay1);
        layout2 = (LinearLayout) findViewById(R.id.lay2);
        layout3 = (LinearLayout) findViewById(R.id.lay3);

        emailForgotPswd = (EditText) findViewById(R.id.email_forgot_pswd);
        otp = (EditText) findViewById(R.id.otp_forgot_pswd1);
        otp2 = (EditText) findViewById(R.id.otp_forgot_pswd2);
        otp3 = (EditText) findViewById(R.id.otp_forgot_pswd3);

        pswd = (EditText) findViewById(R.id.pswd_txt);
        pswdCnfm = (EditText) findViewById(R.id.cnfm_pswd);

        otpInstruction = (TextView) findViewById(R.id.otp_instruction_txt);

        btnClick();
    }

    private void btnClick() {

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layout2.getVisibility() == View.VISIBLE) {
                    layout2.setVisibility(View.GONE);
                    layout.setVisibility(View.VISIBLE);
                } else if (layout3.getVisibility() == View.VISIBLE) {
                    layout3.setVisibility(View.GONE);
                    layout2.setVisibility(View.VISIBLE);
                } else {
                    onBackPressed();
                }
            }
        });

        sendOtpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = emailForgotPswd.getText().toString();
                if (email.length() > 0 && validEmail(email)) {
                    sendOtpMethod(email.trim());
                } else {
                    emailForgotPswd.setError("Enter valid email address");
                }
            }
        });

        continueShoppingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otpTxt = otp.getText().toString().trim() + otp2.getText().toString().trim() + otp3.getText().toString().trim();

                if (otpTxt.length() == 6) {
                    verifyOtpMethod(otpTxt);
                } else {
                    Toast.makeText(ForgotPswdActivity.this, "Enter Valid OTP.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        otp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (otp.getText().toString().length() == 2) {
                    otp2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (otp2.getText().toString().length() == 2) {
                    otp3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pswd.getText().toString().trim().length() >= 8 && pswd.getText().toString().trim().length() <= 16) {
                    if (pswd.getText().toString().trim().equals(pswdCnfm.getText().toString().trim())) {
                        submitPassword(pswd.getText().toString().trim(), pswdCnfm.getText().toString().trim());
                    } else {
                        pswdCnfm.setError("Incorrect password");
                    }
                } else {
                    pswd.setError("Password should be between 8 to 16 characters.");
                }
            }
        });
        btnResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOtpMethod(preferences.getString("email", null));
            }
        });
    }

    private void submitPassword(String trim, String psword) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        String url = UrlAll.changePIN;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", preferences.getString("email", null));
            jsonObject.put("pin", trim);
            jsonObject.put("confirm_pin", psword);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        dialog.dismiss();
                        Log.d("response_forgot", response.toString());
                        try {
                            if (response.getString("message").equals("Forget Password Succesfull")) {
                                Toast.makeText(ForgotPswdActivity.this, "Password reset successfully.", Toast.LENGTH_SHORT).show();

                                /*    startActivity(new Intent(ForgotPswdActivity.this, SignInActivity.class));*/
                                onBackPressed();
                            } else {
                                Toast.makeText(ForgotPswdActivity.this, "Enter Correct Password", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                volleyErrMethod(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        int socketTimeout = 16000;//16 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        // Add the request to the RequestQueue.
        requestQueue.add(request);
    }

    private void sendOtpMethod(final String email) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        String url = UrlAll.forgotPswd;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email);
            jsonObject.put("is_shop", false);
            jsonObject.put("is_customer", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        dialog.dismiss();
                        try {
                            if (response.getString("message").equals("sucessfull done")) {
                                layout.setVisibility(View.GONE);
                                layout2.setVisibility(View.VISIBLE);

                                Log.d("response_forgot", response.toString());
                                editor.putString("email", email);
                                editor.commit();

                                String str = "An email with an OTP to reset your password has been send to ";
                                String str3 = ".Please check your email and enter the OTP.";

                                String str2 = str + "your registered email id <b>" + email + "</b>" + str3;

                                otpInstruction.setText(Html.fromHtml(str2));
                            } else {
                                Toast.makeText(ForgotPswdActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                volleyErrMethod(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        int socketTimeout = 16000;//16 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        // Add the request to the RequestQueue.
        requestQueue.add(request);
    }


    public void volleyErrMethod(VolleyError error) {

        Log.d("response_volleyErr", error.toString());

        if (error.networkResponse != null) {
            Log.d("response_volleyErr2", String.valueOf(error.networkResponse.statusCode));
        } else {
//			 Toast.makeText(context, "Please check your Internet Connection..!!", Toast.LENGTH_SHORT).show();
            Log.d("response_volleyErr7", "Please check your Internet Connection..!!");
        }

        NetworkResponse response = error.networkResponse;
        if (error instanceof ServerError && response != null) {
            try {
                String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));

                // Now you can use any deserializer to make sense of data
                JSONObject obj = new JSONObject(res);

                Log.d("response_volleyErr3", obj.toString());
            } catch (UnsupportedEncodingException e1) {
                // Couldn't properly decode data to string
                e1.printStackTrace();
            } catch (JSONException e2) {
                // returned data is not JSONObject?
                e2.printStackTrace();
            }
        } else {
//			 Toast.makeText(context, "Check Your Internet Connection and try again", Toast.LENGTH_SHORT).show();
        }
    }

    private void verifyOtpMethod(String otpTxt) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        String url = UrlAll.verifyOTP;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", preferences.getString("email", null));
            jsonObject.put("otp", otpTxt);
            jsonObject.put("is_shop", false);
            jsonObject.put("is_customer", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final RequestQueue requestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        dialog.dismiss();

                        try {
                            if (response.getString("message").equals("otp is correct")) {
                                layout2.setVisibility(View.GONE);
                                layout3.setVisibility(View.VISIBLE);
                            } else {
                                Toast.makeText(ForgotPswdActivity.this, "Enter valid OTP", Toast.LENGTH_SHORT).show();
                            }
                            layout.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.d("response_verified_otp", response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                volleyErrMethod(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        int socketTimeout = 16000;//16 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        // Add the request to the RequestQueue.
        requestQueue.add(request);
    }


    private boolean validEmail(String trim) {
        String email = trim;

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (email.matches(emailPattern) && email.length() > 0) {
            return true;
        }
        return false;
    }
}
