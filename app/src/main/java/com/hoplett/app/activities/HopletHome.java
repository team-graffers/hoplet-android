package com.hoplett.app.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SearchView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import com.android.volley.Response;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.gson.Gson;
import com.hoplett.app.BuildConfig;
import com.hoplett.app.R;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.PermissionManager;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.fragments.FragCategoryByProduct;
import com.hoplett.app.fragments.FragDepartment;
import com.hoplett.app.pojoClasses.leftPanel.viewProfile.ViewProfile;
import com.hoplett.app.pojoClasses.wishlist.ItemWishListPojo;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class HopletHome extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;

    public static AppBarLayout appBarLayout;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public static SearchView searchView;
    public static EditText searchEditText;
    public static Toolbar toolbar;

    FragmentManager manager;
    FragmentTransaction transaction;

    FragDepartment fragDepartment;
    FragCategoryByProduct fragCategory;

    public static TextView toolbarTitle;
    public static TextView favoriteCount;

    ImageButton backBtn, editProfileBtn;
    ImageView ordersImgBtn, needHelpImgBtn, rateUsImgBtn, shareImgBtn, aboutUsImgBtn, termsImgBtn, privacyImgBtn, logoutImgBtn;
    public static TextView ordersTxtBtn, needHelpTxtBtn, rateUsTxtBtn, shareTxtBtn, aboutUsTxtBtn, termsTxtBtn, privacyTxtBtn, logoutTxtBtn;

    public static CircleImageView profileImg;
    public static TextView nameUser;

    ImageView favImgVwBtn;
    DrawerLayout drawer;
    String token;

    private PermissionManager permissionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

			/*FragSignIn fragSignIn = new FragSignIn();
			fragSignIn.googleSignOutMethod();*/
        permissionManager = new PermissionManager() {
        };
        permissionManager.checkAndRequestPermissions(this);

        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // Check if enabled and if not send user to the GPS settings
        if (!enabled) {
			/*Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);*/
            Toast.makeText(this, "Enable Your GPS Location", Toast.LENGTH_SHORT).show();
        }else { initLocation();}

        setContentView(R.layout.activity_hoplet_home);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        appBarLayout = (AppBarLayout) findViewById(R.id.appbar_layout);

        preferences = getSharedPreferences(GlobalDataCls.PreferencesName, MODE_PRIVATE);
        editor = preferences.edit();

        toolbarTitle = (TextView) findViewById(R.id.toolbar_title_txt_vw);

//        toolbarTitle.setText("Men");

        favoriteCount = (TextView) findViewById(R.id.favorite_dot);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        backBtn = (ImageButton) navigationView.findViewById(R.id.back_btn_left_panel);
        editProfileBtn = (ImageButton) navigationView.findViewById(R.id.edit_profile_btn);

        profileImg = (CircleImageView) navigationView.findViewById(R.id.profile_img);
        nameUser = (TextView) navigationView.findViewById(R.id.name_user);

        /*if (preferences.getString(GlobalDataCls.profileName, null) != null) {
            nameUser.setText(preferences.getString(GlobalDataCls.profileName, null));
        }
        if (preferences.getString(GlobalDataCls.profileImg, null) != null) {
            Picasso.with(this).load(preferences.getString(GlobalDataCls.profileImg, null)).placeholder(R.drawable.user_default_img).error(R.drawable.user_default_img).into(profileImg);
        }*/

        ordersImgBtn = (ImageView) navigationView.findViewById(R.id.orders_img_left_panel);
        ordersTxtBtn = (TextView) navigationView.findViewById(R.id.orders_txt_left_panel);

        needHelpImgBtn = (ImageView) navigationView.findViewById(R.id.need_help_img_left_panel);
        needHelpTxtBtn = (TextView) navigationView.findViewById(R.id.need_help_txt_left_panel);

        rateUsImgBtn = (ImageView) navigationView.findViewById(R.id.rate_us_img_left_panel);
        rateUsTxtBtn = (TextView) navigationView.findViewById(R.id.rate_us_txt_left_panel);

        shareImgBtn = (ImageView) navigationView.findViewById(R.id.share_img_left_panel);
        shareTxtBtn = (TextView) navigationView.findViewById(R.id.share_txt_left_panel);

        aboutUsImgBtn = (ImageView) navigationView.findViewById(R.id.about_us_img_left_panel);
        aboutUsTxtBtn = (TextView) navigationView.findViewById(R.id.about_us_txt_left_panel);

        termsImgBtn = (ImageView) navigationView.findViewById(R.id.terms_img_left_panel);
        termsTxtBtn = (TextView) navigationView.findViewById(R.id.terms_txt_left_panel);

        privacyImgBtn = (ImageView) navigationView.findViewById(R.id.privacy_img_left_panel);
        privacyTxtBtn = (TextView) navigationView.findViewById(R.id.privacy_txt_left_panel);

        logoutImgBtn = (ImageView) navigationView.findViewById(R.id.logout_img_left_panel);
        logoutTxtBtn = (TextView) navigationView.findViewById(R.id.logout_txt_left_panel);
        TextView versionName = (TextView) navigationView.findViewById(R.id.version_name_of_app);

        PackageInfo pinfo = null;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            int versionNumber = pinfo.versionCode;
            versionName.setText("Version "+pinfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        favImgVwBtn = (ImageView) findViewById(R.id.fav_img_vw_btn);

        /**
         *  Create Fragment instance
         **/
        fragDepartment = new FragDepartment();
        fragCategory = new FragCategoryByProduct();

        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();

        String val = preferences.getString(GlobalDataCls.FragmentName, null);


        /**
         *  Back Button
         * **/
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
            }
        });


        /**
         *
         * Edit Profile Button
         * **/
        editProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (preferences.getString(GlobalDataCls.toKen, null) != null) {
                    Intent intent = new Intent(HopletHome.this, ProfileEditActivity.class);
                    startActivity(intent);
                } else {
                    editor.putString(GlobalDataCls.SignInActivityName, "ProfileEditActivity");
                    editor.commit();
                    Intent intent = new Intent(HopletHome.this, SignInActivity.class);
                    startActivity(intent);
                }
            }
        });

        /**
         * Navigation View On Click Listener
         *
         * **/
        ordersImgBtn.setOnClickListener(this);
        ordersTxtBtn.setOnClickListener(this);

        needHelpImgBtn.setOnClickListener(this);
        needHelpTxtBtn.setOnClickListener(this);

        rateUsImgBtn.setOnClickListener(this);
        rateUsTxtBtn.setOnClickListener(this);

        shareImgBtn.setOnClickListener(this);
        shareTxtBtn.setOnClickListener(this);

        aboutUsImgBtn.setOnClickListener(this);
        aboutUsTxtBtn.setOnClickListener(this);

        termsImgBtn.setOnClickListener(this);
        termsTxtBtn.setOnClickListener(this);

        privacyImgBtn.setOnClickListener(this);
        privacyTxtBtn.setOnClickListener(this);

        logoutImgBtn.setOnClickListener(this);
        logoutTxtBtn.setOnClickListener(this);

        /**
         *  Favourite list
         * **/
        favImgVwBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (preferences.getString(GlobalDataCls.toKen, null) != null) {
                    Intent intent = new Intent(HopletHome.this, WishListActivity.class);
                    startActivity(intent);
                } else {
                    startActivity(new Intent(HopletHome.this, SignInActivity.class));
                }
            }
        });

        /**
         *  Add Fragment to Activity
         *
         **/
		 /*if (preferences.getString(GlobalDataCls.FragmentName, null) != null && val.equals("FragSearchProduct")) {

			 editor.remove(GlobalDataCls.FragmentName);
			 editor.commit();

			 transaction.add(R.id.hoplet_home_layout, fragSearchProduct);
			 transaction.commit();
		 } else if (preferences.getString(GlobalDataCls.FragmentName, null) != null && val.equals(
			 "FragCategoryByProduct")) {

			 editor.remove(GlobalDataCls.FragmentName);
			 editor.commit();

			 transaction.add(R.id.hoplet_home_layout, fragCategory);
			 transaction.commit();
		 }else if (preferences.getString(GlobalDataCls.SignInFragName, null) != null && preferences.getString(GlobalDataCls.SignInFragName, null).equals("FragCategoryByProduct")) {

			 editor.remove(GlobalDataCls.SignInFragName);
			 editor.commit();

			 transaction.add(R.id.hoplet_home_layout, fragCategory);
			 transaction.commit();
		 } else if (preferences.getString(GlobalDataCls.FragmentName, null) != null && val.equals("ProfileEditActivity")) {

			 editor.remove(GlobalDataCls.FragmentName);
			 editor.commit();

			 transaction.add(R.id.hoplet_home_layout, fragDepartment);
			 transaction.commit();

			 if (drawer.isDrawerOpen(GravityCompat.START)) {
				 drawer.closeDrawer(GravityCompat.START);
			 } else {
				 drawer.openDrawer(GravityCompat.START);
			 }
		 } else {
			 transaction.add(R.id.hoplet_home_layout, fragDepartment);
			 transaction.commit();
		 }*/

        if (preferences.getString(GlobalDataCls.FragmentName, null) != null && preferences.getString(GlobalDataCls.FragmentName, null).equals("Category")) {
            transaction.add(R.id.hoplet_home_layout, fragCategory);
            transaction.commit();
        } else if (preferences.getString(GlobalDataCls.FragmentName, null) != null && preferences.getString(GlobalDataCls.FragmentName, null).equals("Department")) {
            transaction.add(R.id.hoplet_home_layout, fragDepartment);
            transaction.commit();
        } else {
            transaction.add(R.id.hoplet_home_layout, fragDepartment);
            transaction.commit();
        }


        /**
         *  Filter List Instance
         * **/
        GlobalDataCls.listColor = new ArrayList<>();
        GlobalDataCls.listBrand = new ArrayList<>();
        GlobalDataCls.listCategory = new ArrayList<>();
        GlobalDataCls.listSize = new ArrayList<>();

        GlobalDataCls.boolListColor = new ArrayList<>();
        GlobalDataCls.boolListBrand = new ArrayList<>();
        GlobalDataCls.boolListCategory = new ArrayList<>();
        GlobalDataCls.boolListSize = new ArrayList<>();

        GlobalDataCls.favoriteItemList = new ArrayList<>();
    }

    @Override
    protected void onStart() {
        super.onStart();

        /** Login Logout Text Change On Button **/
        token = preferences.getString(GlobalDataCls.toKen, null);

        if (token != null && token != "null" && token != "") {
            logoutTxtBtn.setText("Logout");
        } else {
            logoutTxtBtn.setText("Login");
        }
        Log.d("resume__onStart", "onstart_activity");
        if (preferences.getString(GlobalDataCls.toKen, null) != null) {
            profileData();
            initWishlist();
        }
    }

    private void profileData() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        String url = UrlAll.viewProfileUrl;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Token " + preferences.getString(GlobalDataCls.toKen, null));

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ViewProfile viewProfile = new Gson().fromJson(response.toString(),
                                ViewProfile.class);

                        if (viewProfile != null) {
                            if (viewProfile.getData().getUserName() != null) {
                                editor.putString(GlobalDataCls.profileName, viewProfile.getData().getUserName());
                                nameUser.setText(viewProfile.getData().getUserName());
                            }
                            if (viewProfile.getData().getEmail() != null) {
                                editor.putString(GlobalDataCls.profileImg, viewProfile.getData().getEmail());
                            }

                            editor.commit();

                            String imgPath = viewProfile.getData().getImage().toString();
                            if (imgPath != null && imgPath != "null" && imgPath.length() > 0 && imgPath != "") {
                                Picasso.with(HopletHome.this).load((String) viewProfile.getData().getImage()).placeholder(R.drawable.user_default_img).error(R.drawable.user_default_img).into(profileImg);
                            }
                        }
                        dialog.dismiss();
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(this, url, headers, dialog);
    }

    /**
     * Location
     **/
    public void initLocation() {

        fusedLocationProviderClient = new FusedLocationProviderClient(this);

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setFastestInterval(2000);
        locationRequest.setInterval(4000);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                GlobalDataCls.latitude = String.valueOf(locationResult.getLastLocation().getLatitude());
                GlobalDataCls.longitude = String.valueOf(locationResult.getLastLocation().getLongitude());

                //					Toast.makeText(getContext(), latitude+"\n"+longitude, Toast.LENGTH_SHORT).show();
            }
        }, Looper.getMainLooper());
    }

    /**
     * Back Button Press Action
     **/
    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (fragCategory.isResumed()) {
            startDepartmentFrag();
        } else if (fragDepartment.isResumed()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            System.exit(0);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        permissionManager.checkResult(requestCode, permissions, grantResults);
        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // Check if enabled and if not send user to the GPS settings
        if (!enabled) {
			/*Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);*/
            Toast.makeText(HopletHome.this, "Enable Your GPS Location", Toast.LENGTH_SHORT).show();
        }else { initLocation();}
    }


    /**
     * Search menu for brand and product
     **/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.hoplet_home, menu);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        int colorEdt = 0x000000;
        searchEditText.setTextColor(Color.parseColor("#000000"));
        searchEditText.setHintTextColor(Color.parseColor("#777777"));
        searchEditText.setHint("Search Product");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                searchView.setIconified(true);
                searchView.onActionViewCollapsed();

                if (fragCategory.isResumed()) {
                    editor.putString(GlobalDataCls.FragmentName, "Category");
                } else if (fragDepartment.isResumed()) {
                    editor.putString(GlobalDataCls.FragmentName, "Department");
                }

                editor.putString(GlobalDataCls.SearchText, s);
                editor.commit();
                searchEditText.setText("");
                startActivity(new Intent(HopletHome.this, SearchProductActivity.class));
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        /*searchView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
                    searchView.setIconified(true);
                    searchView.onActionViewCollapsed();
                    *//*searchView.setOnCloseListener(new SearchView.OnCloseListener() {
                        @Override
                        public boolean onClose() {
                            return false;
                        }
                    });*//*
                }
            }
        });*/
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
			/*if (id == R.id.action_settings) {
          return true;
      }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void startFragCategoryByProduct() {
        transaction = manager.beginTransaction();
        transaction.replace(R.id.hoplet_home_layout, fragCategory);
        transaction.commit();
        //    searchView.onActionViewCollapsed();
        //    searchView.setIconified(true);
    }

    public void startDepartmentFrag() {

        transaction = manager.beginTransaction();
        transaction.replace(R.id.hoplet_home_layout, fragDepartment);
        transaction.commit();
        //      searchView.onActionViewCollapsed();
        //      searchView.setIconified(true);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        if (v.getId() == R.id.orders_img_left_panel || v.getId() == R.id.orders_txt_left_panel) {

            LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
            boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

            // Check if enabled and if not send user to the GPS settings
            if (!enabled) {
			/*Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);*/
                Toast.makeText(HopletHome.this, "Enable Your GPS Location", Toast.LENGTH_SHORT).show();
            }else {
                drawer.closeDrawer(GravityCompat.START);

                if (preferences.getString(GlobalDataCls.toKen, null) != null) {
                    intent = new Intent(HopletHome.this, OrdersActivity.class);
                    startActivity(intent);
                } else {
                    editor.putString(GlobalDataCls.SignInActivityName, "OrdersActivity");
                    editor.commit();
                    intent = new Intent(HopletHome.this, SignInActivity.class);
                    startActivity(intent);
                }
            }
        } else if (v.getId() == R.id.need_help_img_left_panel || v.getId() == R.id.need_help_txt_left_panel) {

            /*editor.putString(GlobalDataCls.FragmentName, "NeedHelpFragment");
            editor.commit();

            intent = new Intent(HopletHome.this, LeftPanelActivity.class);
            startActivity(intent);*/
        } else if (v.getId() == R.id.rate_us_img_left_panel || v.getId() == R.id.rate_us_txt_left_panel) {

            /*editor.putString(GlobalDataCls.FragmentName, "RateUsFragment");
            editor.commit();

            intent = new Intent(HopletHome.this, LeftPanelActivity.class);
            startActivity(intent);*/
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + this.getPackageName())));
            } catch (android.content.ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + this.getPackageName())));
            }
        } else if (v.getId() == R.id.share_img_left_panel || v.getId() == R.id.share_txt_left_panel) {

            /*editor.putString(GlobalDataCls.FragmentName, "ShareFragment");
            editor.commit();

            intent = new Intent(HopletHome.this, LeftPanelActivity.class);
            startActivity(intent);*/

            try {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Hoplett");
                String shareMessage= "\nRegister on Hoplett and explore stores around you.\n\n";
                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                startActivity(Intent.createChooser(shareIntent, "choose one"));
            } catch(Exception e) {
                //e.toString();
            }

        } else if (v.getId() == R.id.about_us_img_left_panel || v.getId() == R.id.about_us_txt_left_panel) {

            editor.putString(GlobalDataCls.FragmentName, "AboutUsFragment");
            editor.commit();

            intent = new Intent(HopletHome.this, LeftPanelActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.terms_img_left_panel || v.getId() == R.id.terms_txt_left_panel) {

            editor.putString(GlobalDataCls.FragmentName, "TermsFragment");
            editor.commit();

            intent = new Intent(HopletHome.this, LeftPanelActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.privacy_img_left_panel || v.getId() == R.id.privacy_txt_left_panel) {

            editor.putString(GlobalDataCls.FragmentName, "PrivacyFragment");
            editor.commit();

            intent = new Intent(HopletHome.this, LeftPanelActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.logout_img_left_panel || v.getId() == R.id.logout_txt_left_panel) {

            token = preferences.getString(GlobalDataCls.toKen, null);
            if (token != null && token != "null" && token != "") {
                editor.remove(GlobalDataCls.toKen);
                editor.remove(GlobalDataCls.profileImg);
                editor.remove(GlobalDataCls.profileName);
                editor.clear();
                editor.commit();
                favoriteCount.setText("");
                GlobalDataCls.favoriteItemList.clear();

                profileImg.setImageResource(R.mipmap.ic_launcher);
                nameUser.setText("");
                logoutTxtBtn.setText("Login");
                GlobalDataCls.FavoriteCounter = 0;

                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }

                startDepartmentFrag();
            } else {
                startActivity(new Intent(HopletHome.this, SignInActivity.class));
                startDepartmentFrag();
            }
        }
    }


    public void initWishlist() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		/*dialog.setCancelable(false);
		dialog.show();*/

        String url = UrlAll.WishlistUrl;

        HashMap headers = new HashMap();
        headers.put("Authorization", "Token " + preferences.getString(GlobalDataCls.toKen, null));

        Log.d("token_token", preferences.getString(GlobalDataCls.toKen, null));
        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("res_wishlist", response.toString());

                        try {
                            if (response.get("data").toString() != "" && response.get("data").toString().length() > 0) {

                                ItemWishListPojo itemList = new Gson().fromJson(response.toString(), ItemWishListPojo.class);

                                if (itemList.getData() != null && itemList.getData().size() > 0 && !itemList.getData().isEmpty()) {

                                    favoriteCount.setText(String.valueOf(itemList.getData().size()));
                                    GlobalDataCls.favoriteItemList = new ArrayList<>();
                                    GlobalDataCls.FavoriteCounter = itemList.getData().size();

                                    for (int i = 0; i < itemList.getData().size(); i++) {
                                        GlobalDataCls.favoriteItemList.add(itemList.getData().get(i).getSkuId().toString());
                                    }
                                }
                            } else {
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(this, url, headers, dialog);
    }
}
