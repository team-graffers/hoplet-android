package com.hoplett.app.activities;

import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.hoplett.app.R;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.fragments.leftPanel.FragAboutUs;
import com.hoplett.app.fragments.leftPanel.FragNeedHelp;
import com.hoplett.app.fragments.leftPanel.FragPrivacyPolicy;
import com.hoplett.app.fragments.leftPanel.FragTermsConditions;

public class LeftPanelActivity extends AppCompatActivity {

	SharedPreferences preferences;
	SharedPreferences.Editor editor;

	FragmentManager manager;
	FragmentTransaction transaction;

	FragNeedHelp fragNeedHelp;
	FragPrivacyPolicy fragPrivacyPolicy;
	FragTermsConditions fragTermsConditions;
	FragAboutUs fragAboutUs;

	private Toolbar toolbar;

	 @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_left_panel);

		 /*toolbar = (Toolbar) findViewById(R.id.toolbar);
		 setSupportActionBar(toolbar);
		 toolbar.setNavigationIcon(R.drawable.back_icon);*/

		preferences = getSharedPreferences(GlobalDataCls.PreferencesName, MODE_PRIVATE);
		editor = preferences.edit();

		manager = getSupportFragmentManager();
		transaction = manager.beginTransaction();

		fragNeedHelp = new FragNeedHelp();
		fragPrivacyPolicy = new FragPrivacyPolicy();
		fragTermsConditions = new FragTermsConditions();
		fragAboutUs = new FragAboutUs();



		if (preferences.getString(GlobalDataCls.FragmentName, null) != null && preferences.getString(GlobalDataCls.FragmentName, null).equals("NeedHelpFragment")){
			transaction.add(R.id.left_panel_layout, fragNeedHelp);
			transaction.commit();
		}else if (preferences.getString(GlobalDataCls.FragmentName, null) != null && preferences.getString(GlobalDataCls.FragmentName, null).equals("RateUsFragment")){
			/*transaction.add(R.id.left_panel_layout, fragNeedHelp);
			transaction.commit();*/
		}else if (preferences.getString(GlobalDataCls.FragmentName, null) != null && preferences.getString(GlobalDataCls.FragmentName, null).equals("ShareFragment")){
			/*transaction.add(R.id.left_panel_layout, fragNeedHelp);
			transaction.commit();*/
		}else if (preferences.getString(GlobalDataCls.FragmentName, null) != null && preferences.getString(GlobalDataCls.FragmentName, null).equals("AboutUsFragment")){
			transaction.add(R.id.left_panel_layout, fragAboutUs);
			transaction.commit();
		}else if (preferences.getString(GlobalDataCls.FragmentName, null) != null && preferences.getString(GlobalDataCls.FragmentName, null).equals("TermsFragment")){
			transaction.add(R.id.left_panel_layout, fragTermsConditions);
			transaction.commit();
		}else if (preferences.getString(GlobalDataCls.FragmentName, null) != null && preferences.getString(GlobalDataCls.FragmentName, null).equals("PrivacyFragment")){
			transaction.add(R.id.left_panel_layout, fragPrivacyPolicy);
			transaction.commit();
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}
}
