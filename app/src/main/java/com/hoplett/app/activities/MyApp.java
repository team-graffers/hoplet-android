package com.hoplett.app.activities;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Sandy on 20-03-2019 at 10:29 AM.
 */
public class MyApp extends Application {

	 @Override
	 public void onCreate() {
			super.onCreate();

			// Add code to print out the key hash
			try {
				 PackageInfo info = getPackageManager().getPackageInfo(
						 "com.graffersid.hoplett",
						 PackageManager.GET_SIGNATURES);
				 for (Signature signature : info.signatures) {
						MessageDigest md = MessageDigest.getInstance("SHA");
						md.update(signature.toByteArray());
						Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
				 }
			} catch (PackageManager.NameNotFoundException e) {

			} catch (NoSuchAlgorithmException e) {

			}


		 FacebookSdk.sdkInitialize(getApplicationContext());
		 AppEventsLogger.activateApp(this);
	 }
}
