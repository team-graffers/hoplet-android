package com.hoplett.app.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Looper;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.fragments.leftPanel.FragPastOrders;
import com.hoplett.app.fragments.leftPanel.FragUpcomingOrders;
import com.hoplett.app.pojoClasses.wishlist.ItemWishListPojo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrdersActivity extends AppCompatActivity implements View.OnClickListener {

	SearchView searchView;
	private TabLayout tabLayout;
	private ViewPager viewPager;
	Toolbar toolbar;
	private EditText searchEditText;
	TextView favDotCounter;
	ImageView favImgBtn;
	ImageButton backBtn;

	SharedPreferences preferences;
	SharedPreferences.Editor editor;

	private FusedLocationProviderClient fusedLocationProviderClient;
	private LocationRequest locationRequest;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_orders);

		backBtn = (ImageButton) findViewById(R.id.back_btn_orders);
		favDotCounter = (TextView) findViewById(R.id.favorite_dot);
		favImgBtn = (ImageView) findViewById(R.id.fav_img_btn);

		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		viewPager = (ViewPager) findViewById(R.id.viewpager);
		setupViewPager(viewPager);

		tabLayout = (TabLayout) findViewById(R.id.tabs);
		tabLayout.setupWithViewPager(viewPager);

		preferences = getSharedPreferences(GlobalDataCls.PreferencesName, MODE_PRIVATE);
		editor = preferences.edit();

		backBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(OrdersActivity.this, HopletHome.class);
				startActivity(intent);
			}
		});

		init();
		initWishlist();

		favImgBtn.setOnClickListener(this);
		favDotCounter.setOnClickListener(this);

		tabLayout.getSelectedTabPosition();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.hoplet_home, menu);

		MenuItem menuItem = menu.findItem(R.id.action_search);

		searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

		searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
		int colorEdt = 0x000000;
		searchEditText.setTextColor(Color.parseColor("#000000"));

		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String s) {

				if (viewPager.getCurrentItem() == 0) {

					FragUpcomingOrders orders = new FragUpcomingOrders();
					orders.searchUpcomingOrdersMethod(s);
				} else {

					new FragPastOrders().searchMethod(s);
				}
				return false;
			}

			@Override
			public boolean onQueryTextChange(String s) {

				if (viewPager.getCurrentItem() == 0) {

					FragUpcomingOrders orders = new FragUpcomingOrders();
					orders.searchUpcomingOrdersMethod(s);
				} else {

					new FragPastOrders().searchMethod(s);
				}
				return false;
			}
		});

		return true;
	}

	ViewPagerAdapter adapter;

	private void setupViewPager(ViewPager viewPager) {
		adapter = new ViewPagerAdapter(getSupportFragmentManager());

		adapter.addFragment(new FragUpcomingOrders(), "Upcoming Order");
		adapter.addFragment(new FragPastOrders(), "Past Order");
		viewPager.setAdapter(adapter);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.fav_img_btn || v.getId() == R.id.favorite_dot){
			Intent intent = new Intent(this, WishListActivity.class);
			startActivity(intent);
		}
	}


	class ViewPagerAdapter extends FragmentPagerAdapter {

		private final List<Fragment> mFragmentList = new ArrayList<>();
		private final List<String> mFragmentTitleList = new ArrayList<>();

		public ViewPagerAdapter(FragmentManager manager) {
			super(manager);
		}

		@Override
		public Fragment getItem(int position) {
			return mFragmentList.get(position);
		}

		@Override
		public int getCount() {
			return mFragmentList.size();
		}

		public void addFragment(Fragment fragment, String title) {
			mFragmentList.add(fragment);
			mFragmentTitleList.add(title);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return Html.fromHtml(mFragmentTitleList.get(position));
		}
	}


	private void init() {

		fusedLocationProviderClient = new FusedLocationProviderClient(this);

		locationRequest = new LocationRequest();
		locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		locationRequest.setFastestInterval(2000);
		locationRequest.setInterval(4000);

		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			// TODO: Consider calling
			//    ActivityCompat#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for ActivityCompat#requestPermissions for more details.
			return;
		}
		fusedLocationProviderClient.requestLocationUpdates(locationRequest, new LocationCallback() {
			@Override
			public void onLocationResult(LocationResult locationResult) {
				super.onLocationResult(locationResult);

				GlobalDataCls.latitude = String.valueOf(locationResult.getLastLocation().getLatitude());
				GlobalDataCls.longitude = String.valueOf(locationResult.getLastLocation().getLongitude());
			}
		}, Looper.getMainLooper());
	}

	private void initWishlist() {

		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.dialog_category_layout);
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		/*dialog.setCancelable(false);
		dialog.show();*/

		String url = UrlAll.WishlistUrl;

		HashMap headers = new HashMap();
		headers.put("Authorization", "Token "+preferences.getString(GlobalDataCls.toKen, null));

		ApiCallMethods apiCallMethods = new ApiCallMethods(){
			@Override
			public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
				return new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {

						Log.d("res_wishlist", response.toString());

						try {
							if (response.get("data").toString() != "" && response.get("data").toString().length() > 0) {

								ItemWishListPojo itemList = new Gson().fromJson(response.toString(), ItemWishListPojo.class);

								if (itemList.getData() != null) {

									favDotCounter.setText(String.valueOf(itemList.getData().size()));
								}
							}else {
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				};
			}
		};
		apiCallMethods.volleyJSONObjectGET(this, url, headers, dialog);
	}
}
