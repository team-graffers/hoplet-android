package com.hoplett.app.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.leftPanel.viewProfile.ViewProfile;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileEditActivity extends AppCompatActivity {

    ImageButton backBtn, dropDownArrow;
    CardView dropDownArrowBtn;
    LinearLayout chngPswdLayout, dropDownLayout;
    Boolean boolChngArrow = true;

    CircleImageView profilePic;
    LinearLayout takeImg;
    EditText usersName, emailVw, contactNo;
    TextView securityPin, generateNewPin, fbId, changeFbId, gmailId,
            changeGmailId;
    EditText pswdCurrent, pswdNew, pswdReEnterNew;
    Button btnUpdateProfile;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);

 //       dropDownArrowBtn = (CardView) findViewById(R.id.chng_pswd_drop_down_btn);
        chngPswdLayout = (LinearLayout) findViewById(R.id.chng_pswd_layout);
        backBtn = (ImageButton) findViewById(R.id.back_btn_profile_edit);
        dropDownArrow = (ImageButton) findViewById(R.id.drop_down_arrow_btn);
 //       dropDownLayout = (LinearLayout) findViewById(R.id.drop_down_layout);

        profilePic = (CircleImageView) findViewById(R.id.img_vw);
        takeImg = (LinearLayout) findViewById(R.id.take_img);
        usersName = (EditText) findViewById(R.id.name_user);
        emailVw = (EditText) findViewById(R.id.email_vw);
        contactNo = (EditText) findViewById(R.id.contact_no);
        securityPin = (TextView) findViewById(R.id.security_pin);
        generateNewPin = (TextView) findViewById(R.id.generate_new_pin);
        fbId = (TextView) findViewById(R.id.fb_id);
        changeFbId = (TextView) findViewById(R.id.change_fb_id);
        gmailId = (TextView) findViewById(R.id.gmail_id);
        changeGmailId = (TextView) findViewById(R.id.change_gmail_id);
        pswdCurrent = (EditText) findViewById(R.id.current_pswd);
        pswdNew = (EditText) findViewById(R.id.new_pswd);
        pswdReEnterNew = (EditText) findViewById(R.id.re_enter_pswd);
        btnUpdateProfile = (Button) findViewById(R.id.update_profile_btn);

        preferences = getSharedPreferences(GlobalDataCls.PreferencesName, MODE_PRIVATE);
        editor = preferences.edit();

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);

        initialize();
    }

    int PICK_IMAGE_REQUEST = 111;
    Bitmap bitmap = null;
    private void initialize() {

        dropDownArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (boolChngArrow) {
                    boolChngArrow = false;
                    chngPswdLayout.setVisibility(View.VISIBLE);
                    dropDownArrow.setBackgroundResource(R.drawable.up_arrow);
                } else {
                    boolChngArrow = true;
                    chngPswdLayout.setVisibility(View.GONE);
                    dropDownArrow.setBackgroundResource(R.drawable.down_arrow);
                }
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences preferences = getSharedPreferences(GlobalDataCls.PreferencesName,
                        MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();

                editor.putString(GlobalDataCls.FragmentName, "ProfileEditActivity");
                editor.commit();

                Intent intent = new Intent(ProfileEditActivity.this, HopletHome.class);
                startActivity(intent);
            }
        });

        profileDetail();


        generateNewPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateNewPinMthod();
            }
        });

        takeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
            }
        });

        btnUpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfile();
            }
        });
    }

    private void updateProfile() {

        JSONObject jsonObject = new JSONObject();
        String pswd = null, email = null, name = null, phone = null, img = null;

        try {
            if (usersName.getText().toString().trim().length() >3){
                name = usersName.getText().toString().trim();
            }else {usersName.setError("Enter name.");}

            if (bitmap != null){

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] imageBytes = baos.toByteArray();
                final String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

                img = imageString;
            }else { img = "";}

            if (pswdNew.getText().toString().trim().length()>0){
                pswd = pswdNew.getText().toString().trim();
            }else {pswd = "";}

            if (emailVw.getText().toString().trim().length()>0){
                if (validEmail(emailVw.getText().toString().trim())) {
                    email = emailVw.getText().toString().trim();
                }else {emailVw.setError("Enter valid email address");}
            }else {email = "";}

            if (contactNo.getText().toString().trim().length() > 0){
                if (contactNo.getText().toString().trim().length() == 10){
                    phone = contactNo.getText().toString().trim();
                }else {contactNo.setError("Enter valid mobile number");}
            }else {phone = "";}

            if (name != null){
                if (pswd != null && pswd.equals(pswdReEnterNew.getText().toString())){
                    if (email != null){
                        if (phone != null){
                            if (img != null){
                                jsonObject.put("name", name);
                                jsonObject.put("profile", img);
                                jsonObject.put("password", pswd);
                                jsonObject.put("email", email);
                                jsonObject.put("phone", phone);
                                jsonObject.put("is_shop", false);
                                jsonObject.put("is_customer", true);

                                updateProfile22(jsonObject);
                            }else {}
                        }else {contactNo.setError("Enter valid mobile number");}
                    }else {emailVw.setError("Enter valid email address");}
                }else {pswdNew.setError("Enter valid password");}
            }else {usersName.setError("Enter name.");}
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateProfile22(JSONObject jsonObject) {

        dialog.show();

        HashMap headers = new HashMap();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Token "+preferences.getString(GlobalDataCls.toKen, null));

        String url = UrlAll.editProfile;

        ApiCallMethods apiCallMethods = new ApiCallMethods(){
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        Log.d("update_profile", response.toString());

                        profileDetail();
                        startActivity(new Intent(ProfileEditActivity.this, HopletHome.class));
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectPOST(this, url, jsonObject, headers, dialog);
    }

    private boolean validEmail(String trim) {
        String email = trim;

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (email.matches(emailPattern) && email.length() > 0) {
            return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();

            try {
                //getting image from gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);

                //Setting image to ImageView
                profilePic.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void generateNewPinMthod() {

        dialog.show();

        HashMap headers = new HashMap();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Token "+preferences.getString(GlobalDataCls.toKen, null));

        String url = UrlAll.generateNewPin;

        ApiCallMethods apiCallMethods = new ApiCallMethods(){
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        Log.d("generate_pin", response.toString());
                        try {
                            String pin = response.getString("data");
                            securityPin.setText(pin);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(this, url, headers, dialog);
    }

    @Override
    public void onBackPressed() {
		/*SharedPreferences preferences = getSharedPreferences(GlobalDataCls.PreferencesName,
						MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();

		editor.putString(GlobalDataCls.FragmentName, "ProfileEditActivity");
		editor.commit();

		Intent intent = new Intent(ProfileEditActivity.this, HopletHome.class);
		startActivity(intent);*/
        super.onBackPressed();
    }

    private void profileDetail() {

        dialog.show();

        String url = UrlAll.viewProfileUrl;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Token " + preferences.getString(GlobalDataCls.toKen, null));

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ViewProfile viewProfile = new Gson().fromJson(response.toString(),
                                ViewProfile.class);

                        if (viewProfile != null) {
                            if (viewProfile.getData().getUserName() != null) {
                                usersName.setText(viewProfile.getData().getUserName());
                            }
                            if (viewProfile.getData().getEmail() != null) {
                                emailVw.setText(viewProfile.getData().getEmail());
                            }
                            if (viewProfile.getData().getUserNo() != null) {
                                contactNo.setText(String.valueOf(viewProfile.getData().getUserNo()));
                            }

                            String imgPath = viewProfile.getData().getImage().toString();
                            if (imgPath != null && imgPath != "null" && imgPath.length()>0 && imgPath != "") {

                                Picasso.with(ProfileEditActivity.this).load(imgPath).placeholder(R.drawable.user_default_img).error(R.drawable.user_default_img).into(profilePic);
                            }

                            if (viewProfile.getData().getPin() != null) {
                                securityPin.setText(viewProfile.getData().getPin());
                            }
                        }
                        dialog.dismiss();
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(this, url, headers, dialog);

    }
}
