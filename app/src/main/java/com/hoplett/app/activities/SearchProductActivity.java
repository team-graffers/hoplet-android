package com.hoplett.app.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.android.volley.Response;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.adapters.AdaptrSearchProduct;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.byProductShopping.productList.ItemProductListByProduct;
import com.hoplett.app.pojoClasses.byProductShopping.productList.ProductListByProductItems;
import com.hoplett.app.pojoClasses.byProductShopping.productList.ResultList;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchProductActivity extends AppCompatActivity {

    RecyclerView recyclerVwSearchProduct;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;
    ImageButton backBtn;
    AdaptrSearchProduct adapter;
    ArrayList<Boolean> listBool;
    private List<ResultList> listProducts;
    private ProductListByProductItems productListByProductItems;
    HashMap headers;

    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_product);

        recyclerVwSearchProduct = (RecyclerView) findViewById(R.id.recycler_vw_search_product);

        preferences = getSharedPreferences(GlobalDataCls.PreferencesName, Context.MODE_PRIVATE);
        editor = preferences.edit();

        progressLoader = (ProgressBar) findViewById(R.id.progress_loader);

        backBtn = (ImageButton) findViewById(R.id.back_btn_search);

        context = SearchProductActivity.this;

        String url;
        headers = new HashMap();
        headers.put("Content-Type", "application/json");

        if (preferences.getString(GlobalDataCls.toKen, null) != null) {

            headers.put("Authorization", "Token " + preferences.getString(GlobalDataCls.toKen, null));

            if (preferences.getString(GlobalDataCls.SearchText, null) != null) {
                url = UrlAll.SearchProductList_Token + preferences.getString(GlobalDataCls.SearchText, null);
                //        Toast.makeText(context, url, Toast.LENGTH_SHORT).show();
            } else {
                url = UrlAll.SearchProductList_Token;
            }
        }else {
            if (preferences.getString(GlobalDataCls.SearchText, null) != null) {
                url = UrlAll.SearchProductList + preferences.getString(GlobalDataCls.SearchText, null);
                //        Toast.makeText(context, url, Toast.LENGTH_SHORT).show();
            } else {
                url = UrlAll.SearchProductList;
            }
        }

        listBool = new ArrayList<>();
        listProducts = new ArrayList<>();
        productListByProductItems = new ProductListByProductItems();

        adapter = new AdaptrSearchProduct(context, listProducts, listBool);
        layoutManager = new GridLayoutManager(SearchProductActivity.this, 2);
        recyclerVwSearchProduct.setHasFixedSize(true);
        recyclerVwSearchProduct.setLayoutManager(layoutManager);
        recyclerVwSearchProduct.setAdapter(adapter);

        prepareList(url);

        paginationMethod();

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(SearchProductActivity.this, HopletHome.class));
    }

    private void prepareList(String url) {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        String ssss = response.toString();

                        ItemProductListByProduct product = new Gson().fromJson(ssss, ItemProductListByProduct.class);

                        if (product != null) {

                            ResultList resultList;

                            if (product.getNext() != null && product.getNext().toString() != "null" && product.getNext().toString().length() > 0) {

                                nextPage = product.getNext().toString();
                            } else {
                                nextPage = null;
                            }

                            if (product != null && product.getResults() != null) {
                                for (int i = 0; i < product.getResults().size(); i++) {

                                    listBool.add(product.getResults().get(i).getFav());

                                    resultList = new ResultList();

                                    resultList.setBrand(product.getResults().get(i).getBrand());
                                    resultList.setProduct(product.getResults().get(i).getProduct());
                                    resultList.setProductId(product.getResults().get(i).getProductId());

                                    if (product.getResults().get(i).getImage() != null) {
                                        resultList.setImage(product.getResults().get(i).getImage());
                                    }else {resultList.setImage("");}
                                    resultList.setShopCount(0);
                                    resultList.setPrice(product.getResults().get(i).getPrice());
                                    resultList.setDiscPrice(product.getResults().get(i).getDiscPrice());
                                    resultList.setSkuId(product.getResults().get(i).getSkuId());

                                    listProducts.add(resultList);
                                }
                            }

                            productListByProductItems.setCount(product.getCount());
                            productListByProductItems.setNext(String.valueOf(product.getNext()));
                            productListByProductItems.setPrevious(product.getPrevious());
                            productListByProductItems.setResults(listProducts);
                            adapter.notifyDataSetChanged();
                        }
                        dialog.dismiss();
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(context, url, headers, dialog);
    }

    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;
    GridLayoutManager layoutManager;
    String nextPage, previousPage;
    ProgressBar progressLoader;

    private void paginationMethod() {

        recyclerVwSearchProduct.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {

                    isScrolling = false;
                    if (nextPage != null) {

                        progressLoader.setLayoutParams(new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.MATCH_PARENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM));

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressLoader.setVisibility(View.GONE);
                                prepareList(nextPage);
                            }
                        }, 300);
                    }
                }
            }
        });
    }
}
