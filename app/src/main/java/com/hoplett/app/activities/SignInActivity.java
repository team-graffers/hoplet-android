package com.hoplett.app.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.Gson;
import com.hoplett.app.R;

import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.login.fbLogin.FbLoginData;
import com.hoplett.app.pojoClasses.login.googleLogin.GoogleLoginData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SignInActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    Context context;

    String userNAme = "";
    String userImage = "";
    String userToken = "";

    final int RC_SIGN_IN = 101;

    private GoogleApiClient googleApiClient;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    TextView skipBtn, forgotPassBtn, signUpBtn;
    EditText emailEdtTxt, passwordEdtTxt;
    Button signInBtn;
    ImageButton fbSignInBtn, googleSignInBtn;
    LoginButton fbLoginBtn;

    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    TextView signInTxtBold;

    Dialog dialog;

    String mailIdResponded, whoIsRespond;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        context = SignInActivity.this;

        dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);

        signInTxtBold = (TextView) findViewById(R.id.sign_in_txt_bold);

        /*String ss = "<font font-weight=900>Sign In</font>";
        signInTxtBold.setText(Html.fromHtml(ss));*/

        initialize();
    }

    private void initialize() {

        preferences = getSharedPreferences(GlobalDataCls.PreferencesName,
                Context.MODE_PRIVATE);
        editor = preferences.edit();

        skipBtn = (TextView) findViewById(R.id.skip_signin_txt);
        forgotPassBtn = (TextView) findViewById(R.id.forgot_password_txt);
        signUpBtn = (TextView) findViewById(R.id.signup_txt);

        emailEdtTxt = (EditText) findViewById(R.id.email_signin_edt_txt);
        passwordEdtTxt = (EditText) findViewById(R.id.password_signin_edt_txt);

        signInBtn = (Button) findViewById(R.id.signin_btn);

        fbSignInBtn = (ImageButton) findViewById(R.id.facebook_login_btn);
        googleSignInBtn = (ImageButton) findViewById(R.id.google_login_btn);

        fbLoginBtn = (LoginButton) findViewById(R.id.login_button_fb);

        btnOnClickMethod();
    }

    private void btnOnClickMethod() {

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
            }
        });

        forgotPassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignInActivity.this, ForgotPswdActivity.class));
            }
        });

        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, HopletHome.class);
                startActivity(intent);
            }
        });

        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailEdtTxt.getText().toString();
                String pswd = passwordEdtTxt.getText().toString();
                if (email.length() > 0 && validEmail(email)) {
                    if (pswd.length() >= 8 && pswd.length() <= 16) {
                        signInCustomerMethod(email, pswd);
                    } else {
                        passwordEdtTxt.setError("incorrect password");
                    }
                } else {
                    emailEdtTxt.setError("Enter valid email address");
                }
            }
        });


        fbSignInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fbLoginBtn.performClick();
            }
        });

        fbLoginMethod();

        /**
         *  Google Sign In
         * **/
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();

        googleApiClient = new GoogleApiClient.Builder(context).enableAutoManage(this,
                SignInActivity.this).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();

        googleSignInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (preferences.getString(GlobalDataCls.toKen, null) != null) {

                    googleSignOutMethod();
                } else {

                    Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                    startActivityForResult(intent, RC_SIGN_IN);
                }
            }
        });
    }

    private void signInCustomerMethod(String email, String pswd) {
        dialog.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email);
            jsonObject.put("password", pswd);
            jsonObject.put("is_shop", false);
            jsonObject.put("is_customer", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("json_signInData", jsonObject.toString());

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("response_manual_login", response.toString());
                        dialog.dismiss();
                        try {
                            if (!response.getBoolean("status")){
                                Toast.makeText(SignInActivity.this, "incorrect username or password", Toast.LENGTH_SHORT).show();
                            }else {
                                String token = response.getString("data");
                                editor.putString(GlobalDataCls.toKen, token);
                                editor.commit();
                                onBackPressed();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
            }
        };
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        apiCallMethods.volleyJSONObjectPOST(context, UrlAll.loginUrl, jsonObject, headers, dialog);
    }

    private void googleSignOutMethod() {

        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {

                            Intent intent = new Intent(context, HopletHome.class);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                            editor.remove(GlobalDataCls.googleSignInInfo);
                            editor.remove(GlobalDataCls.userName);
                            editor.remove(GlobalDataCls.userImage);
                            editor.remove(GlobalDataCls.tokenId);
                            editor.commit();

                            startActivity(intent);
                        } else {
                            //  Toast.makeText(getApplicationContext(), "Session not close", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void fbLoginMethod() {

        callbackManager = CallbackManager.Factory.create();

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {

//					 Toast.makeText(getContext(), currentAccessToken.toString(), Toast.LENGTH_SHORT).show();

                if (currentAccessToken != null && currentAccessToken.getToken() != null) {
                    Log.d("response_f_accessToken", currentAccessToken.getToken());
                }
            }
        };


        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {

                //				Toast.makeText(getContext(), currentProfile.toString(), Toast.LENGTH_SHORT).show();
//					 displayMessage(currentProfile);
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();

        fbLoginBtn.setReadPermissions("email");

        fbLoginBtn.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();

                //           Toast.makeText(context, loginResult.toString(), Toast.LENGTH_LONG).show();
                //        Log.d("response_fbSuccs", loginResult.toString());

                //			 displayMessage(profile);
                getUserDetails(loginResult);
            }

            @Override
            public void onCancel() {
                Toast.makeText(context, "Cancel login request", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {

                //           Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                Log.d("response_fbErr", error.toString());
            }
        });
    }

    private void getUserDetails(final LoginResult loginResult) {

        GraphRequest data_request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(final JSONObject json_object, GraphResponse response) {

                //			JSONObject response, profile_pic_data, profile_pic_url;

                if (json_object != null && json_object.toString().length() > 0) {

                    Log.d("response_facebook_id", json_object.toString());

                    final FbLoginData fbLoginData = new Gson().fromJson(json_object.toString(), FbLoginData.class);

                    mailIdResponded = fbLoginData.getEmail();
                    whoIsRespond = "Google";

                    userImage = "";

                    userNAme = fbLoginData.getName();
                    userImage = fbLoginData.getPicture().getData().getUrl();
                    userToken = fbLoginData.getId();

                    final JSONObject jsonObject = new JSONObject();

                    try {

                        final String url = UrlAll.fbLoginUrl;

                        if (fbLoginData.getEmail() != null) {

                            jsonObject.put("is_shop", "False");
                            //						 jsonObject.put("facebook", json_object.toString());
                            if (fbLoginData.getPicture() != null) {
                                jsonObject.put("facebook", json_object);
                            } else {
                                JSONObject jsonObj = new JSONObject();
                                JSONObject data = new JSONObject();
                                JSONObject picture = new JSONObject();
                                JSONObject fbJson = new JSONObject();

                                try {

                                    data.put("height", fbLoginData.getPicture().getData().getHeight());
                                    data.put("is_silhouette", fbLoginData.getPicture().getData().getIsSilhouette());
                                    data.put("url", "");
                                    data.put("width", fbLoginData.getPicture().getData().getWidth());

                                    picture.put("data", data);

                                    fbJson.put("id", fbLoginData.getId());
                                    fbJson.put("name", fbLoginData.getName());
                                    fbJson.put("picture", picture);

                                    fbJson.put("email", fbLoginData.getEmail());

                                    jsonObject.put("facebook", fbJson);

                                } catch (JSONException e) {

                                }
                            }
                            LoginManager.getInstance().logOut();

                            Log.d("response_fb_json", jsonObject.toString());
                            signInMethod(jsonObject, url, json_object);
                        } else {
                            final Dialog dialogSignIn = new Dialog(context);
                            dialogSignIn.setContentView(R.layout.dialog_email_sign_in);

                            WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();
                            lWindowParams.copyFrom(dialogSignIn.getWindow().getAttributes());
                            lWindowParams.width = WindowManager.LayoutParams.FILL_PARENT;
                            lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;


                            final EditText editText = (EditText) dialogSignIn.findViewById(R.id.email_signin);
                            Button cancelBtn, okBtn;

                            cancelBtn = (Button) dialogSignIn.findViewById(R.id.cancel_dialog_btn);
                            okBtn = (Button) dialogSignIn.findViewById(R.id.email_cnfm_btn);

                            cancelBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    LoginManager.getInstance().logOut();
                                    dialogSignIn.dismiss();
                                }
                            });

                            okBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    JSONObject jsonObj = new JSONObject();
                                    JSONObject data = new JSONObject();
                                    JSONObject picture = new JSONObject();
                                    JSONObject fbJson = new JSONObject();

                                    try {

                                        data.put("height", fbLoginData.getPicture().getData().getHeight());
                                        data.put("is_silhouette", fbLoginData.getPicture().getData().getIsSilhouette());
                                        data.put("url", fbLoginData.getPicture().getData().getUrl());
                                        data.put("width", fbLoginData.getPicture().getData().getWidth());

                                        picture.put("data", data);

                                        fbJson.put("id", fbLoginData.getId());
                                        fbJson.put("name", fbLoginData.getName());
                                        fbJson.put("picture", picture);

                                        if (validEmail(editText.getText().toString().trim())) {
                                            fbJson.put("email", editText.getText().toString().trim());

                                            jsonObj.put("is_shop", "False");

                                            jsonObj.put("facebook", fbJson);

                                            Toast.makeText(context, editText.getText().toString().trim(), Toast.LENGTH_LONG).show();
                                            dialogSignIn.dismiss();
                                            LoginManager.getInstance().logOut();
                                            signInMethod(jsonObj, url, fbJson);
                                        } else {
                                            editText.setError("Enter valid Email address.");
                                        }

                                        Log.d("json_email", jsonObj.toString());
                                    } catch (JSONException e) {

                                    }
                                }
                            });

                            dialogSignIn.setCancelable(false);
                            dialogSignIn.show();
                            dialogSignIn.getWindow().setAttributes(lWindowParams);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Log.d("response_jsonResponse", jsonObject.toString());
                    Log.d("response_jsonResponse", loginResult.getAccessToken().toString());
                }
            }

        });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,email,picture.width(120).height(120)");

        data_request.setParameters(permission_param);
        data_request.executeAsync();
    }

    private boolean validEmail(String trim) {
        String email = trim;

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (email.matches(emailPattern) && email.length() > 0) {
            return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

//            Log.d("sign_in_google_result", String.valueOf(result.isSuccess())+"\n"+result.getSignInAccount().zac().toString());
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {

            GoogleSignInAccount account = result.getSignInAccount();

            Log.d("google_scs_res", account.toString());

            JSONObject jsonObject = new JSONObject();

            try {

                JSONObject object = new JSONObject(account.zac());
                final GoogleLoginData loginData = new Gson().fromJson(object.toString(), GoogleLoginData.class);

                final String url = UrlAll.googleLoginUrl;

                if (loginData.getEmail() != null && loginData.getEmail() != "null" && loginData.getEmail().length() > 0) {

                    jsonObject.put("is_shop", "False");
                    if (loginData.getPhotoUrl() != null) {

                        jsonObject.put("google", object);

                        mailIdResponded = loginData.getEmail();
                        whoIsRespond = "Facebook";

                        userImage = "";
                        userNAme = loginData.getDisplayName();
                        userImage = loginData.getPhotoUrl();
                        userToken = loginData.getId();
                    } else {
                        JSONObject google = new JSONObject();
                        JSONArray grantedScopes = new JSONArray();

                        try {
                            grantedScopes.put(loginData.getGrantedScopes().get(0));
                            grantedScopes.put(loginData.getGrantedScopes().get(1));
                            grantedScopes.put(loginData.getGrantedScopes().get(2));
                            grantedScopes.put(loginData.getGrantedScopes().get(3));
                            grantedScopes.put(loginData.getGrantedScopes().get(4));

                            google.put("id", loginData.getId());

                            google.put("displayName", loginData.getDisplayName());
                            google.put("givenName", loginData.getGivenName());
                            google.put("familyName", loginData.getFamilyName());
                            google.put("photoUrl", "");
                            google.put("expirationTime", loginData.getExpirationTime());
                            google.put("obfuscatedIdentifier", loginData.getObfuscatedIdentifier());
                            google.put("grantedScopes", grantedScopes);

                            google.put("email", loginData.getEmail());

                            jsonObject.put("google", google);
                        } catch (JSONException e) {

                        }
                    }

                    //          Log.d("response_google", account.toString());

                    Log.d("response_google2", result.getStatus().toString());

                    Log.d("response_google_SignIn", jsonObject.toString());


                    signInMethod(jsonObject, url, object);
                } else {
                    final Dialog dialogSignIn = new Dialog(context);
                    dialogSignIn.setContentView(R.layout.dialog_email_sign_in);

                    final EditText editText = (EditText) dialogSignIn.findViewById(R.id.email_signin);
                    Button cancelBtn, okBtn;

                    cancelBtn = (Button) dialogSignIn.findViewById(R.id.cancel_dialog_btn);
                    okBtn = (Button) dialogSignIn.findViewById(R.id.email_cnfm_btn);

                    cancelBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogSignIn.dismiss();
                        }
                    });
                    okBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            JSONObject jsonObj = new JSONObject();
                            JSONObject google = new JSONObject();
                            JSONArray grantedScopes = new JSONArray();

                            try {
                                grantedScopes.put(loginData.getGrantedScopes().get(0));
                                grantedScopes.put(loginData.getGrantedScopes().get(1));
                                grantedScopes.put(loginData.getGrantedScopes().get(2));
                                grantedScopes.put(loginData.getGrantedScopes().get(3));
                                grantedScopes.put(loginData.getGrantedScopes().get(4));

                                google.put("id", loginData.getId());

                                google.put("displayName", loginData.getDisplayName());
                                google.put("givenName", loginData.getGivenName());
                                google.put("familyName", loginData.getFamilyName());
                                google.put("photoUrl", loginData.getPhotoUrl());
                                google.put("expirationTime", loginData.getExpirationTime());
                                google.put("obfuscatedIdentifier", loginData.getObfuscatedIdentifier());
                                google.put("grantedScopes", grantedScopes);

                                if (validEmail(editText.getText().toString())) {

                                    dialogSignIn.dismiss();

                                    google.put("email", editText.getText().toString());

                                    jsonObj.put("is_shop", "False");
                                    jsonObj.put("google", google);

                                    signInMethod(jsonObj, url, google);
                                } else {
                                    editText.setError("Enter valid Email address.");
                                }
                            } catch (JSONException e) {

                            }

                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Sign in cancel : "+String.valueOf(result.isSuccess()), Toast.LENGTH_LONG).show();
            Log.d("google_scs_res", String.valueOf(result.isSuccess()));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (GlobalDataCls.backBool) {
            GlobalDataCls.backBool = false;
            onBackPressed();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void signInMethod(final JSONObject jsonObject, String url, final JSONObject object) {

        dialog.show();

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("response_loginResponse", response.toString());
                        try {

                            if (response.get("data").toString() != null && response.get("data").toString() != "null" && response.get("data").toString() != "" && response.get("data").toString().length() > 0) {

                                Log.d("response_responseData", response.get("data").toString());

                                editor.putString(GlobalDataCls.userName, userNAme);
                                editor.putString(GlobalDataCls.userImage, userImage);
                                editor.putString(GlobalDataCls.tokenId, userToken);

                                String token = response.get("data").toString();

                                editor.putString(GlobalDataCls.toKen, token);
                                editor.commit();

                                HopletHome.logoutTxtBtn.setText("Logout");

                                dialog.dismiss();
                                onBackPressed();
    //                            profileDataMethod(dialog);
                            } else {

                                dialog.dismiss();
                                String str =
                                        response.get("message").toString() + " By <b>" + mailIdResponded +
                                                "</b>. Do you want to merge this id with <b>" + whoIsRespond +
                                                "</b>.";
                                final Dialog dialog2 = new Dialog(context);
                                dialog2.setContentView(R.layout.dialog_merge_social_ids);

                                TextView textView = (TextView) dialog2.findViewById(R.id.txt_vw);
                                Button yesBtn = (Button) dialog2.findViewById(R.id.yes_btn);
                                Button noBtn = (Button) dialog2.findViewById(R.id.no_btn);

                                textView.setText(Html.fromHtml(str));

                                dialog2.setCancelable(false);
                                dialog2.show();

                                noBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog2.dismiss();
                                    }
                                });

                                yesBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        JSONObject param = new JSONObject();
                                        try {
                                            param.put("id", object.get("id").toString());
                                            param.put("email", object.get("email").toString());
                                            if (whoIsRespond.equals("Google")) {
                                                param.put("type", "facebook");
                                            } else if (whoIsRespond.equals("Facebook")) {
                                                param.put("type", "google");
                                            }
                                            if (param.toString() != null && param.toString().length() > 0) {

                                                mergeSocialIds(param, dialog2, dialog);
                                            } else {
                                                dialog.dismiss();
                                            }

                                            Log.d("response_paramMerge", param.toString());
                                        } catch (JSONException e) {

                                        }
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectPOST(context, url, jsonObject, headers, dialog);
    }

    /*private void profileDataMethod(final Dialog dialog) {

        String url = UrlAll.viewProfileUrl;
        //      url = "http://52.66.151.82:8001/viewprofile";

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Token " + preferences.getString(GlobalDataCls.toKen, null));

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        dialog.dismiss();

                        ViewProfile viewProfile = new Gson().fromJson(response.toString(),
                                ViewProfile.class);

                        if (viewProfile != null) {
                            if (viewProfile.getData().getUserName() != null) {
                                editor.putString(GlobalDataCls.profileName, viewProfile.getData().getUserName());
                                HopletHome.nameUser.setText(viewProfile.getData().getUserName());
                            }
                            if (viewProfile.getData().getEmail() != null) {
                                editor.putString(GlobalDataCls.profileImg, viewProfile.getData().getEmail());
                            }

                            editor.commit();

                            String imgPath = viewProfile.getData().getImage().toString();
                            if (imgPath != null && imgPath != "null" && imgPath.length() > 0 && imgPath != "") {
                                Picasso.with(context).load((String) viewProfile.getData().getImage()).placeholder(R.drawable.user_default_img).error(R.drawable.user_default_img).into(HopletHome.profileImg);
                            }
                        }
                        SignInActivity.this.dialog.dismiss();
                        onBackPressed();
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(this, url, headers, dialog);
    }*/

    private void mergeSocialIds(JSONObject param, final Dialog dialog2, final Dialog dialog) {

        String url = UrlAll.mergeSocial;

        Log.d("response_paramMerge", param.toString());

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        SignInActivity.this.dialog.dismiss();
                        dialog2.dismiss();
                        dialog.dismiss();

                        Log.d("response_socialMerge", response.toString());

                        String msgTxt = "";
                        if (whoIsRespond.equals("Google")) {
                            msgTxt = "Facebook";
                        } else if (whoIsRespond.equals("Facebook")) {
                            msgTxt = "Google";
                        }

                        String msgToast = "You can login with <b>" + msgTxt + "</b>";
                        Toast.makeText(SignInActivity.this, Html.fromHtml(msgToast), Toast.LENGTH_LONG).show();
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectPOST(context, url, param, headers, dialog);
    }
}
