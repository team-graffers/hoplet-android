package com.hoplett.app.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.leftPanel.viewProfile.ViewProfile;
import com.hoplett.app.pojoClasses.login.fbLogin.FbLoginData;
import com.hoplett.app.pojoClasses.login.googleLogin.GoogleLoginData;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    Context context;

    String userNAme = "";
    String userImage = "";
    String userToken = "";
    String imageString = null;

    final int RC_SIGN_IN = 101;

    private GoogleApiClient googleApiClient;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    Button signUpBtn, btnVerifyEmail, uploadImgBtn;
    EditText emailEdtTxt, passwordEdtTxt, nameEdtTxt, cnfmPswdEdtTxt, phoneEdtTxt;
    TextView signInBtn;
    ImageButton backBtn, fbSignInBtn, googleSignInBtn;
    LoginButton fbLoginBtn;

    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    Dialog dialog;
    String mailIdResponded, whoIsRespond;
    TextView termsCondition;
    ImageView uploadedImgVw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        context = SignUpActivity.this;

        dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);

        initialize();
    }

    private void initialize() {

        preferences = getSharedPreferences(GlobalDataCls.PreferencesName, Context.MODE_PRIVATE);
        editor = preferences.edit();

        backBtn = (ImageButton) findViewById(R.id.back_signup_txt);
        signInBtn = (TextView) findViewById(R.id.signin_txt);

        nameEdtTxt = (EditText) findViewById(R.id.name_signup_edt_txt);
        emailEdtTxt = (EditText) findViewById(R.id.email_signup_edt_txt);
        phoneEdtTxt  = (EditText) findViewById(R.id.phone_signup_edt_txt);
        passwordEdtTxt = (EditText) findViewById(R.id.password_signup_edt_txt);
        cnfmPswdEdtTxt = (EditText) findViewById(R.id.password_cnfm_signup_edt_txt);

        uploadImgBtn = (Button) findViewById(R.id.upload_img_btn);
        uploadedImgVw = (ImageView) findViewById(R.id.uploaded_img_vw);

        btnVerifyEmail = (Button) findViewById(R.id.verify_email_sign_up);

        termsCondition = (TextView) findViewById(R.id.terms_condition);

        signUpBtn = (Button) findViewById(R.id.signup_btn);

        fbSignInBtn = (ImageButton) findViewById(R.id.facebook_login_btn);
        googleSignInBtn = (ImageButton) findViewById(R.id.google_login_btn);

        fbLoginBtn = (LoginButton) findViewById(R.id.login_button_fb);

        String signIn = signInBtn.getText().toString().trim()+"<b><font color=#f4684e> SignIn.</font></b>";
        signInBtn.setText(Html.fromHtml(signIn));

        btnOnClickMethod();
    }

    private void btnOnClickMethod() {

        String terms = "By signing up you agree to our <font color=#1767F1><b><u>Terms & Conditions</u></b></font>";
        termsCondition.setText(Html.fromHtml(terms));
        termsCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://hoplett.com/terms-of-use/"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        uploadImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage();
            }
        });

        emailEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null){
                    if (validEmail(s.toString())){
                        btnVerifyEmail.setEnabled(true);
                        signUpBtn.setEnabled(false);
                        btnVerifyEmail.setBackgroundResource(R.drawable.verify_btn_back);
                        signUpBtn.setBackgroundResource(R.drawable.signin_btn_back_gray);
                    }else {
                        btnVerifyEmail.setEnabled(false);
                        signUpBtn.setEnabled(false);
                        btnVerifyEmail.setBackgroundResource(R.drawable.verify_btn_back_gray);
                        signUpBtn.setBackgroundResource(R.drawable.signin_btn_back_gray);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnVerifyEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyEmailForSignUp();
            }
        });

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String emailId, nameCust, phoneCust, pswd, pswdCnfm;

                emailId = emailEdtTxt.getText().toString();
                nameCust = nameEdtTxt.getText().toString();
                phoneCust = phoneEdtTxt.getText().toString();
                pswd = passwordEdtTxt.getText().toString();
                pswdCnfm = cnfmPswdEdtTxt.getText().toString();

                if (nameCust.length() > 3){
                    if (emailId.length() > 0 && validEmail(emailId)){
                        if (phoneCust.length() == 10){
                            if (pswd.length()>=8 && pswd.length() <= 16){
                                if (pswd.equals(pswdCnfm)){
                                    if (imageString != null){
                                    signUpMethod(nameCust, emailId, phoneCust, pswd);
                                    }else { Toast.makeText(context, "Upload an image", Toast.LENGTH_SHORT).show();}
                                }else { cnfmPswdEdtTxt.setError("Enter correct password"); }
                            }else { passwordEdtTxt.setError("Password should be at least 8 characters and maximum 16 characters."); }
                        }else { phoneEdtTxt.setError("Enter valid mobile number"); }
                    }else { emailEdtTxt.setError("Enter valid email address"); }
                }else { nameEdtTxt.setError("Enter valid name"); }
            }
        });


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, HopletHome.class);
                startActivity(intent);
            }
        });

        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        fbSignInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fbLoginBtn.performClick();
            }
        });

        fbLoginMethod();

        /**
         *  Google Sign In
         * **/
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();

        googleApiClient = new GoogleApiClient.Builder(context).enableAutoManage(this,
                SignUpActivity.this).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();

        googleSignInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (preferences.getString(GlobalDataCls.toKen, null) != null) {

                    googleSignOutMethod();
                } else {

                    Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                    startActivityForResult(intent, RC_SIGN_IN);
                }
            }
        });
    }

    int RESULT_LOAD_IMAGE = 101;
    private void uploadImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        //     Intent intent = new Intent();
        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), RESULT_LOAD_IMAGE);
    }

    private void verifyEmailForSignUp() {

        dialog.show();
        String url = UrlAll.verifyEmail;
        final HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", emailEdtTxt.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response_verify_email", response.toString());
                dialog.dismiss();
                try {
                    if (response.getString("message").equals("otp is send successfully")){
                        final Dialog dialogVerifyEmail = new Dialog(SignUpActivity.this);
                        dialogVerifyEmail.setContentView(R.layout.dialog_verify_otp_sign_up);
                        Button closeDialog = (Button) dialogVerifyEmail.findViewById(R.id.close_btn_sign_up_dialog);
                        Button submitOTP = (Button) dialogVerifyEmail.findViewById(R.id.otp_cnfm_btn);
                        Button resendOTP = (Button) dialogVerifyEmail.findViewById(R.id.resend_otp_btn);
                        final EditText otpEdtTxt = (EditText) dialogVerifyEmail.findViewById(R.id.verify_otp_sign_up);

                        WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();
                        lWindowParams.copyFrom(dialogVerifyEmail.getWindow().getAttributes());
                        lWindowParams.width = WindowManager.LayoutParams.FILL_PARENT; // this is where the magic happens
                        lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

                        closeDialog.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogVerifyEmail.dismiss();
                            }
                        });
                        submitOTP.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (otpEdtTxt.getText().toString().trim().length() == 6){
                                    verifyOtp(otpEdtTxt.getText().toString().trim(), dialogVerifyEmail);
                                }
                            }
                        });
                        resendOTP.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                resendOtpForVerifyEmail();
                            }
                        });
                        dialogVerifyEmail.setCancelable(false);
                        dialogVerifyEmail.show();
                        dialogVerifyEmail.getWindow().setAttributes(lWindowParams);
                    }else {
                        Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("response_verify_email", error.toString());
                dialog.dismiss();
                volleyErrMethod(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        int socketTimeout = 12000;//16 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void resendOtpForVerifyEmail() {
        dialog.show();
        String url = UrlAll.verifyEmail;
        final HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", emailEdtTxt.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response_verify_email", response.toString());
                dialog.dismiss();
                try {
                    if (response.getString("message").equals("otp is send successfully")){
                        Toast.makeText(context, "Otp send successfully", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("response_verify_email", error.toString());
                dialog.dismiss();
                volleyErrMethod(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        int socketTimeout = 12000;//16 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void verifyOtp(String otp, final Dialog dialogVerifyEmail) {
        dialog.show();
        String url = UrlAll.verifyOtpSignUp;
        final HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", emailEdtTxt.getText().toString().trim());
            jsonObject.put("otp", otp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                dialog.dismiss();
                try {
                    if (response.getString("message").equals("the otp is correct")){
                        dialogVerifyEmail.dismiss();
                        Toast.makeText(context, "OTP verified", Toast.LENGTH_SHORT).show();
                        signUpBtn.setEnabled(true);
                        signUpBtn.setBackgroundResource(R.drawable.signin_btn_back);
                    }else {
                        Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                volleyErrMethod(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        int socketTimeout = 12000;//16 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void signUpMethod(String nameCust, final String emailId, String phoneCust, final String pswd) {

        dialog.show();
        final JSONObject jsonObject = new JSONObject();
        final JSONObject object = new JSONObject();

        try {
            object.put("name", nameCust);
            object.put("phone", phoneCust);
            object.put("image", imageString);

            jsonObject.put("email", emailId);
            jsonObject.put("password", pswd);
            jsonObject.put("is_shop", false);
            jsonObject.put("details", object);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("json_signUp", jsonObject.toString());

        ApiCallMethods apiCallMethods = new ApiCallMethods(){
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("response_signUp", response.toString());
                        emailEdtTxt.setText("");
                        nameEdtTxt.setText("");
                        phoneEdtTxt.setText("");
                        passwordEdtTxt.setText("");
                        cnfmPswdEdtTxt.setText("");

                        try {
                            String data = response.getString("data");

                            if (data.equals("") && data.length() == 0){
                                Toast.makeText(SignUpActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();

                                JSONObject obj = new JSONObject();
                                try {
                                    obj.put("email", emailId);
                                    obj.put("password", pswd);
                                    obj.put("is_shop", false);
                                    obj.put("is_customer", true);
                                }catch (JSONException e){

                                }
                                String url = UrlAll.loginUrl;
                                signInManuallyMethod(obj, url, dialog);
                            }else {
                                dialog.dismiss();
                                Toast.makeText(SignUpActivity.this, "Email already exist!!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
            }
        };

        String url = UrlAll.signUpUrl;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        apiCallMethods.volleyJSONObjectPOST(context, url, jsonObject, headers, dialog);
    }

    private void signInManuallyMethod(JSONObject obj, String url, final Dialog dialog) {

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        ApiCallMethods apiCallMethods = new ApiCallMethods(){
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        /*dialog.dismiss();*/

                        try {

                            if (response.get("data").toString() != null && response.get("data").toString() != "null" && response.get("data").toString() != "" && response.get("data").toString().length() > 0) {

                                Log.d("response_responseData", response.get("data").toString());

                                editor.putString(GlobalDataCls.userName, userNAme);
                                editor.putString(GlobalDataCls.userImage, userImage);
                                editor.putString(GlobalDataCls.tokenId, userToken);

                                String token = response.get("data").toString();

                                editor.putString(GlobalDataCls.toKen, token);
                                editor.commit();

                                HopletHome.logoutTxtBtn.setText("Logout");

                                profileDataMethod(dialog);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectPOST(SignUpActivity.this, url, obj, headers, dialog);
    }

    private void googleSignOutMethod() {

        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {

                            Intent intent = new Intent(context, HopletHome.class);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                            editor.remove(GlobalDataCls.googleSignInInfo);
                            editor.remove(GlobalDataCls.userName);
                            editor.remove(GlobalDataCls.userImage);
                            editor.remove(GlobalDataCls.tokenId);
                            editor.commit();

                            startActivity(intent);
                        } else {
                            //  Toast.makeText(getApplicationContext(), "Session not close", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void fbLoginMethod() {

        callbackManager = CallbackManager.Factory.create();

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {

//					 Toast.makeText(getContext(), currentAccessToken.toString(), Toast.LENGTH_SHORT).show();

                if (currentAccessToken != null && currentAccessToken.getToken() != null) {
                    Log.d("response_accessToken", currentAccessToken.getToken());
                }
            }
        };


        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {

                //				Toast.makeText(getContext(), currentProfile.toString(), Toast.LENGTH_SHORT).show();
//					 displayMessage(currentProfile);
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();

        fbLoginBtn.setReadPermissions("email");

        fbLoginBtn.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();

                //           Toast.makeText(context, loginResult.toString(), Toast.LENGTH_LONG).show();
                //        Log.d("response_fbSuccs", loginResult.toString());

                //			 displayMessage(profile);
                getUserDetails(loginResult);
            }

            @Override
            public void onCancel() {
                Toast.makeText(context, "Cancel login request", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {

                //           Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                Log.d("response_fbErr", error.toString());
            }
        });
    }



    private void getUserDetails(final LoginResult loginResult) {

        GraphRequest data_request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(final JSONObject json_object, GraphResponse response) {

                //			JSONObject response, profile_pic_data, profile_pic_url;

                if (json_object != null && json_object.toString().length() > 0) {

                    Log.d("response_id_facebook", json_object.toString());

                    final FbLoginData fbLoginData = new Gson().fromJson(json_object.toString(), FbLoginData.class);

                    mailIdResponded = fbLoginData.getEmail();
                    whoIsRespond = "Google";

                    userImage = "";

                    userNAme = fbLoginData.getName();
                    userImage = fbLoginData.getPicture().getData().getUrl();
                    userToken = fbLoginData.getId();

                    final JSONObject jsonObject = new JSONObject();

                    try {

                        final String url = UrlAll.fbLoginUrl;

                        if (fbLoginData.getEmail() != null) {

                            jsonObject.put("is_shop", "False");
                            //						 jsonObject.put("facebook", json_object.toString());
                            jsonObject.put("facebook", json_object);
                            LoginManager.getInstance().logOut();

                            signInMethod(jsonObject, url, json_object);
                        } else {
                            final Dialog dialogSignIn = new Dialog(context);
                            dialogSignIn.setContentView(R.layout.dialog_email_sign_in);

                            final EditText editText = (EditText) dialogSignIn.findViewById(R.id.email_signin);
                            Button cancelBtn, okBtn;

                            cancelBtn = (Button) dialogSignIn.findViewById(R.id.cancel_dialog_btn);
                            okBtn = (Button) dialogSignIn.findViewById(R.id.email_cnfm_btn);

                            cancelBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogSignIn.dismiss();
                                }
                            });

                            okBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    JSONObject jsonObj = new JSONObject();
                                    JSONObject data = new JSONObject();
                                    JSONObject picture = new JSONObject();
                                    JSONObject fbJson = new JSONObject();

                                    try {

                                        data.put("height", fbLoginData.getPicture().getData().getHeight());
                                        data.put("is_silhouette", fbLoginData.getPicture().getData().getIsSilhouette());
                                        data.put("url", fbLoginData.getPicture().getData().getUrl());
                                        data.put("width", fbLoginData.getPicture().getData().getWidth());

                                        picture.put("data", data);

                                        fbJson.put("id", fbLoginData.getId());
                                        fbJson.put("name", fbLoginData.getName());
                                        fbJson.put("picture", picture);

                                        if (validEmail(editText.getText().toString().trim())) {
                                            fbJson.put("email", editText.getText().toString().trim());

                                            jsonObj.put("is_shop", "False");

                                            jsonObj.put("facebook", fbJson);

                                            dialogSignIn.dismiss();

                                            signInMethod(jsonObj, url, fbJson);
                                        } else {
                                            editText.setError("Enter valid Email address.");
                                        }

                                        Log.d("json_email", jsonObj.toString());
                                    } catch (JSONException e) {

                                    }
                                }
                            });

                            dialogSignIn.setCancelable(false);
                            dialogSignIn.show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Log.d("response_jsonResponse", jsonObject.toString());
                    Log.d("response_jsonResponse", loginResult.getAccessToken().toString());
                }
            }

        });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,email,picture.width(120).height(120)");

        data_request.setParameters(permission_param);
        data_request.executeAsync();
    }

    private boolean validEmail(String trim) {
        String email = trim;

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (email.matches(emailPattern) && email.length() > 0) {
            return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();

            InputStream imageStream = null;
            try {
                imageStream = getContentResolver().openInputStream(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
            //     encodeTobase64(yourSelectedImage);
            uploadedImgVw.setImageBitmap(yourSelectedImage);
            uploadedImgVw.setVisibility(View.VISIBLE);

            //encode image to base64 string
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            yourSelectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageBytes = baos.toByteArray();
            imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        }else if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {

            GoogleSignInAccount account = result.getSignInAccount();

            JSONObject jsonObject = new JSONObject();

            try {

                JSONObject object = new JSONObject(account.zac());
                final GoogleLoginData loginData = new Gson().fromJson(object.toString(), GoogleLoginData.class);

                final String url = UrlAll.googleLoginUrl;

                if (loginData.getEmail() != null && loginData.getEmail() != "null" && loginData.getEmail().length()>0) {

                    jsonObject.put("is_shop", "False");

                    jsonObject.put("google", object);

                    mailIdResponded = loginData.getEmail();
                    whoIsRespond = "Facebook";

                    userImage = "";
                    userNAme = loginData.getDisplayName();
                    userImage = loginData.getPhotoUrl();
                    userToken = loginData.getId();

                    //          Log.d("response_google", account.toString());

                    Log.d("response_google2", result.getStatus().toString());

                    Log.d("response_google_SignIn", jsonObject.toString());

                    signInMethod(jsonObject, url, object);
                }else {
                    final Dialog dialogSignIn = new Dialog(context);
                    dialogSignIn.setContentView(R.layout.dialog_email_sign_in);

                    final EditText editText = (EditText) dialogSignIn.findViewById(R.id.email_signin);
                    Button cancelBtn, okBtn;

                    cancelBtn = (Button) dialogSignIn.findViewById(R.id.cancel_dialog_btn);
                    okBtn = (Button) dialogSignIn.findViewById(R.id.email_cnfm_btn);

                    cancelBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogSignIn.dismiss();
                        }
                    });
                    okBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            JSONObject jsonObj = new JSONObject();
                            JSONObject google = new JSONObject();
                            JSONArray grantedScopes = new JSONArray();

                            try {
                                grantedScopes.put(loginData.getGrantedScopes().get(0));
                                grantedScopes.put(loginData.getGrantedScopes().get(1));
                                grantedScopes.put(loginData.getGrantedScopes().get(2));
                                grantedScopes.put(loginData.getGrantedScopes().get(3));
                                grantedScopes.put(loginData.getGrantedScopes().get(4));

                                google.put("id", loginData.getId());

                                google.put("displayName", loginData.getDisplayName());
                                google.put("givenName", loginData.getGivenName());
                                google.put("familyName", loginData.getFamilyName());
                                google.put("photoUrl", loginData.getPhotoUrl());
                                google.put("expirationTime", loginData.getExpirationTime());
                                google.put("obfuscatedIdentifier", loginData.getObfuscatedIdentifier());
                                google.put("grantedScopes", grantedScopes);

                                if (validEmail(editText.getText().toString())){

                                    dialogSignIn.dismiss();

                                    google.put("email", editText.getText().toString());

                                    jsonObj.put("is_shop", "False");
                                    jsonObj.put("google", google);

                                    signInMethod(jsonObj, url, google);
                                }else {
                                    editText.setError("Enter valid Email address.");
                                }
                            }catch (JSONException e){
                            }
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Sign in cancel", Toast.LENGTH_LONG).show();
        }
    }



    private void signInMethod(final JSONObject jsonObject, String url, final JSONObject object) {

        dialog.show();

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("response_loginResponse", response.toString());
                        try {

                            if (response.get("data").toString() != null && response.get("data").toString() != "null" && response.get("data").toString() != "" && response.get("data").toString().length() > 0) {

                                Log.d("response_responseData", response.get("data").toString());

                                editor.putString(GlobalDataCls.userName, userNAme);
                                editor.putString(GlobalDataCls.userImage, userImage);
                                editor.putString(GlobalDataCls.tokenId, userToken);

                                String token = response.get("data").toString();

                                editor.putString(GlobalDataCls.toKen, token);
                                editor.commit();

                                HopletHome.logoutTxtBtn.setText("Logout");

                                profileDataMethod(dialog);
                            } else {

                                String str =
                                        response.get("message").toString() + " By <b>" + mailIdResponded +
                                                "</b>. Do you want to merge this id with <b>" + whoIsRespond +
                                                "</b>.";
                                final Dialog dialog2 = new Dialog(context);
                                dialog2.setContentView(R.layout.dialog_merge_social_ids);

                                TextView textView = (TextView) dialog2.findViewById(R.id.txt_vw);
                                Button yesBtn = (Button) dialog2.findViewById(R.id.yes_btn);
                                Button noBtn = (Button) dialog2.findViewById(R.id.no_btn);

                                textView.setText(Html.fromHtml(str));

                                dialog2.setCancelable(false);
                                dialog2.show();

                                noBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog2.dismiss();
                                        dialog.dismiss();
                                    }
                                });

                                yesBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        JSONObject param = new JSONObject();
                                        try {
                                            param.put("id", object.get("id").toString());
                                            param.put("email", object.get("email").toString());
                                            if (whoIsRespond.equals("Google")) {
                                                param.put("type", "facebook");
                                            } else if (whoIsRespond.equals("Facebook")) {
                                                param.put("type", "google");
                                            }
                                            if (param.toString() != null && param.toString().length() > 0) {

                                                mergeSocialIds(param, dialog2, dialog);
                                            }

                                            Log.d("response_paramMerge", param.toString());
                                        } catch (JSONException e) {

                                        }
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectPOST(context, url, jsonObject, headers, dialog);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void profileDataMethod(final Dialog dialog) {

        String url = UrlAll.viewProfileUrl;
        //      url = "http://52.66.151.82:8001/viewprofile";

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Token " + preferences.getString(GlobalDataCls.toKen, null));

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        dialog.dismiss();

                        ViewProfile viewProfile = new Gson().fromJson(response.toString(),
                                ViewProfile.class);

                        if (viewProfile != null) {
                            if (viewProfile.getData().getUserName() != null) {
                                editor.putString(GlobalDataCls.profileName, viewProfile.getData().getUserName());
                                HopletHome.nameUser.setText(viewProfile.getData().getUserName());
                            }
                            if (viewProfile.getData().getEmail() != null) {
                                editor.putString(GlobalDataCls.profileImg, viewProfile.getData().getEmail());
                            }

                            editor.commit();

                            String imgPath = viewProfile.getData().getImage().toString();
                            if (imgPath != null && imgPath != "null" && imgPath.length() > 0 && imgPath != "") {
                                Picasso.with(context).load((String) viewProfile.getData().getImage()).placeholder(R.drawable.user_default_img).error(R.drawable.user_default_img).into(HopletHome.profileImg);
                            }
                        }
                        SignUpActivity.this.dialog.dismiss();
                        GlobalDataCls.backBool = true;
                        onBackPressed();
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(this, url, headers, dialog);
    }

    private void mergeSocialIds(JSONObject param, final Dialog dialog, final Dialog dialog2) {

        String url = UrlAll.mergeSocial;

        Log.d("response_paramMerge", param.toString());

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        SignUpActivity.this.dialog.dismiss();
                        dialog2.dismiss();
                        dialog.dismiss();

                        Log.d("response_socialMerge", response.toString());

                        String msgTxt = "";
                        if (whoIsRespond.equals("Google")) {
                            msgTxt = "Facebook";
                        } else if (whoIsRespond.equals("Facebook")) {
                            msgTxt = "Google";
                        }

                        String msgToast = "You can login with <b>" + msgTxt + "</b>";
                        Toast.makeText(SignUpActivity.this, Html.fromHtml(msgToast), Toast.LENGTH_LONG).show();
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectPOST(context, url, param, headers, dialog);

        /*RequestQueue queue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, param, )*/
    }

    public void volleyErrMethod(VolleyError error) {

        Log.d("response_volleyErr", error.toString());

        if (error.networkResponse != null) {
            Log.d("response_volleyErr", String.valueOf(error.networkResponse.statusCode));
        }else{
            Toast.makeText(this, "Please check your Internet Connection..!!", Toast.LENGTH_SHORT).show();
        }

        NetworkResponse response = error.networkResponse;
        if (error instanceof ServerError && response != null) {
            try {
                String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));

                // Now you can use any deserializer to make sense of data
                JSONObject obj = new JSONObject(res);

                Log.d("response_volleyErr", obj.toString());
            } catch (UnsupportedEncodingException e1) {
                // Couldn't properly decode data to string
                e1.printStackTrace();
            } catch (JSONException e2) {
                // returned data is not JSONObject?
                e2.printStackTrace();
            }
        }else {
            //          Toast.makeText(context, "Check Your Internet Connection and try again", Toast.LENGTH_SHORT).show();
        }
    }
}
