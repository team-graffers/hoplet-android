package com.hoplett.app.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.fragments.bystore.FragTodayByStore;
import com.hoplett.app.fragments.bystore.FragTomorrowByStore;
import com.hoplett.app.pojoClasses.leftPanel.visitDetail.VisitDetailData;
import com.hoplett.app.pojoClasses.wishlist.ItemWishListPojo;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class VisitDetailActivity extends AppCompatActivity {

	SharedPreferences preferences;
	SharedPreferences.Editor editor;

	ImageButton backBtn;
	TextView favDotCounter;
	ImageView favHeartBtn;

	ImageView storeImgVw, productImgVw;
	TextView storeName, storeAdd, storeDistnce, productName, brandName, sizeProduct, priceVw, dateVw;

	ImageView storeImgVw_Upcmng;
	TextView storeName_Upcmng, storeAdd_Upcmng, storeDistnce_Upcmng, dateVw_Upcmng;

	LinearLayout pastLay, upcmngLay, canclReplanVstLay;
	TextView cancelVst, replanVst;

	LinearLayout inactiveLay, reserveLay;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_visit_detail);

		init();

		if (GlobalDataCls.visitType != null){
			showHideLayoutMethod();
		}else {
			finish();
		}

		initWishlist();
	}

	private void showHideLayoutMethod() {

		if (GlobalDataCls.visitType.equals("Past")){

			pastLay.setVisibility(View.VISIBLE);
			upcmngLay.setVisibility(View.GONE);
			canclReplanVstLay.setVisibility(View.GONE);

			setDataOnLayout(storeImgVw, productImgVw, storeName, storeAdd, storeDistnce, productName, brandName, sizeProduct, priceVw, dateVw);
		}else if (GlobalDataCls.visitType.equals("Upcoming")){

			pastLay.setVisibility(View.GONE);
			upcmngLay.setVisibility(View.VISIBLE);
			canclReplanVstLay.setVisibility(View.VISIBLE);

			setDataOnLayout(storeImgVw_Upcmng, productImgVw, storeName_Upcmng, storeAdd_Upcmng, storeDistnce_Upcmng, productName, brandName, sizeProduct, priceVw, dateVw_Upcmng);
		}
	}

	private void init() {

		preferences = getSharedPreferences(GlobalDataCls.PreferencesName, MODE_PRIVATE);
		editor = preferences.edit();

		backBtn = (ImageButton) findViewById(R.id.back_btn_orders);
		favDotCounter = (TextView) findViewById(R.id.favorite_dot);
		favHeartBtn = (ImageView) findViewById(R.id.fav_heart);

		storeImgVw = (ImageView) findViewById(R.id.store_img_vw);
		productImgVw = (ImageView) findViewById(R.id.product_img_vw);

		storeName = (TextView) findViewById(R.id.store_name_vw);
		storeAdd = (TextView) findViewById(R.id.address_vw);
		storeDistnce = (TextView) findViewById(R.id.distance_vw);
		productName = (TextView) findViewById(R.id.product_name_vw);
		brandName = (TextView) findViewById(R.id.store_name_txt_vw);
		sizeProduct = (TextView) findViewById(R.id.size_vw);
		priceVw = (TextView) findViewById(R.id.price_vw);
		dateVw = (TextView) findViewById(R.id.date_vw);

		storeImgVw_Upcmng = (ImageView) findViewById(R.id.store_img_vw_u);
		storeName_Upcmng = (TextView) findViewById(R.id.store_name_vw_u);
		storeAdd_Upcmng = (TextView) findViewById(R.id.address_vw_u);
		storeDistnce_Upcmng = (TextView) findViewById(R.id.distance_vw_u);
		dateVw_Upcmng = (TextView) findViewById(R.id.date_vw_u);


		pastLay = (LinearLayout) findViewById(R.id.past_layout);
		upcmngLay = (LinearLayout) findViewById(R.id.upcmng_layout);

		canclReplanVstLay = (LinearLayout) findViewById(R.id.cancel_replan_layout);
		cancelVst = (TextView) findViewById(R.id.cancel_visit_vw);
		replanVst = (TextView) findViewById(R.id.replan_visit_vw);

		inactiveLay = (LinearLayout) findViewById(R.id.inactive_layout);
		reserveLay = (LinearLayout) findViewById(R.id.reserve_layout);

		cancelVst.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				final Dialog dialog = new Dialog(VisitDetailActivity.this);
				dialog.setContentView(R.layout.dialog_cancel_visit_layout);

				Button closeBtn = (Button) dialog.findViewById(R.id.close_dialog_btn);
				Button cnfmBtn = (Button) dialog.findViewById(R.id.cancel_visit_cnfm_btn);

				closeBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});

				cnfmBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						cancelVisitMethod(dialog);
					}
				});

				dialog.setCancelable(false);
				dialog.show();
			}
		});

		replanVst.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				replanVisitMethod();
			}
		});
		fragTodayByStore = new FragTodayByStore();
		fragTomorrowByStore = new FragTomorrowByStore();

		ft = getSupportFragmentManager().beginTransaction();
		ft.add(R.id.dialog_reserve_layout, fragTodayByStore);
		ft.commit();

		backBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(VisitDetailActivity.this, OrdersActivity.class));
			}
		});

		favHeartBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(VisitDetailActivity.this, WishListActivity.class));
			}
		});
		favDotCounter.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(VisitDetailActivity.this, WishListActivity.class));
			}
		});
	}

	@Override
	public void onBackPressed() {
//		super.onBackPressed();
		startActivity(new Intent(VisitDetailActivity.this, OrdersActivity.class));
	}

	private void setDataOnLayout(final ImageView imgVwStore, final ImageView productImgVw,
								 final TextView storeName, final TextView storeAdd,
								 final TextView storeDistnce, final TextView productName,
								 final TextView brandName, final TextView sizeProduct,
								 final TextView priceVw, final TextView dateVw) {

		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.dialog_category_layout);
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		dialog.setCancelable(false);
		dialog.show();

		String url = UrlAll.visitDetail+GlobalDataCls.proId+"&latt="+GlobalDataCls.latitude+"&long" +
			"="+GlobalDataCls.longitude;

		HashMap<String, String> headers = new HashMap<>();
		headers.put("Authorization", "Token "+preferences.getString(GlobalDataCls.toKen, null));

		ApiCallMethods apiCallMethods = new ApiCallMethods(){
			@Override
			public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
				return new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {

						VisitDetailData data = new Gson().fromJson(response.toString(), VisitDetailData.class);

						storeName.setText(data.getData().getShop().getShopName());
						storeAdd.setText(data.getData().getShop().getShopAddress());

						DecimalFormat decimalFormat = new DecimalFormat("###.##");
						String distance =
							decimalFormat.format(data.getData().getShop().getDistance());
						storeDistnce.setText(distance+" km away");

						productName.setText(data.getData().getProd().getProdName());
						brandName.setText(data.getData().getProd().getBrand());
						String s = "<b>"+data.getData().getProd().getSize()+"</b>";
						sizeProduct.setText("Size: "+ Html.fromHtml(s));

						String price = String.valueOf(data.getData().getProd().getPrice());
						priceVw.setText(price);


						String imgStr = data.getData().getProd().getProdImage();
						if (imgStr != null && imgStr != "" && imgStr != "null") {

							String img = imgStr.substring(0, imgStr.length() - 4) + "raw=1";
							Picasso.with(VisitDetailActivity.this).load(img).fit().centerCrop().placeholder(R.drawable.loading_default_image).error(R.drawable.default_img_back).into(productImgVw);
						}else {productImgVw.setImageResource(R.drawable.default_img_back);}

						String imgStore = data.getData().getShop().getImage();
						if (imgStore != null && imgStore != "" && imgStore != "null") {

							Picasso.with(VisitDetailActivity.this).load(imgStore).fit().centerCrop().placeholder(R.drawable.loading_default_image).error(R.drawable.default_img_back).into(imgVwStore);
						}else {imgVwStore.setImageResource(R.drawable.default_img_back);}

						String[] strDate = data.getData().getShop().getTimeFrom().split("T");

						Date date1 = null, date2 = null;
						try {
							date1=new SimpleDateFormat("yyyy-MM-dd").parse(strDate[0]);
							date2=new SimpleDateFormat("HH:mm:ss").parse(strDate[1]);
						} catch (ParseException e) {
							e.printStackTrace();
						}

						SimpleDateFormat dateFormat;
						dateFormat = new SimpleDateFormat("MMM");
						String visitMonth = dateFormat.format(date1);
						dateFormat = new SimpleDateFormat("haa");
						String visitTime = dateFormat.format(date2);

						Calendar c = Calendar.getInstance();
						c.setTime(date1);
						int day = c.get(Calendar.DAY_OF_MONTH);
						Log.d("day", String.valueOf(day)+"  "+visitMonth+"  "+visitTime);

						String visitDay;
						if (day == 1 || day == 21 || day == 31) {
							visitDay = String.valueOf(day) + "st " + visitMonth + " at " + visitTime;
						}else if (day == 2 || day == 22) {
							visitDay =
								String.valueOf(day) + "nd " + visitMonth + " at " + visitTime;
						}else if (day == 3 || day == 23) {
							visitDay =
								String.valueOf(day) + "rd " + visitMonth + " at " + visitTime;
						}else {
							visitDay =
								String.valueOf(day) + "th " + visitMonth + " at " + visitTime;
						}

						if (GlobalDataCls.visitType.equals("Upcoming")){
							Calendar calendar = Calendar.getInstance();
							SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
							Date d = calendar.getTime();
							String dd = dateFormat1.format(d);

							Date date = null;
							try {
								date = new SimpleDateFormat("yyyy-MM-dd").parse(dd);
							} catch (ParseException e) {
								e.printStackTrace();
							}

							if (date1.compareTo(date) > 0 && date1.compareTo(date) == 1){
								dateVw.setText("Tomorrow, "+visitDay);
							}else if (date1.compareTo(date) == 0){
								dateVw.setText("Today, "+visitDay);
							}
						}else {
							dateVw.setText(visitDay);
						}
						dialog.dismiss();
					}
				};
			}
		};
		apiCallMethods.volleyJSONObjectGET(this, url, headers, dialog);
	}


/**
 *  Re-plan Visit
 * **/
	FragTodayByStore fragTodayByStore;
	FragTomorrowByStore fragTomorrowByStore;
	Button doneBtn, cancelBtn;
	FragmentTransaction ft;

	LinearLayout todayLayout, tomorrowLayout;
	TextView openTime, closeTime, closeBtn;
	TextView todayTxtVw, todaySmallTxtVw, todayTxtLine, tomorrowTxtVw, tomorrowSmallTxtVw, tomorrowTxtLine;

	private void replanVisitMethod() {

		inactiveLay.setVisibility(View.VISIBLE);
		reserveLay.setVisibility(View.VISIBLE);
		backBtn.setEnabled(false);

		Calendar c = Calendar.getInstance();

		SimpleDateFormat dateFormatRD = new SimpleDateFormat("yyyy-M-dd");
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");

		Date toD = c.getTime();

		final String currentDateRD = dateFormatRD.format(toD);
		String currentDate = dateFormat.format(toD);

		String today = "<b><font size=20>"+currentDate+"</font></b>";

		editor.putString(GlobalDataCls.DateReserve, currentDateRD);
		editor.commit();

		c.add(Calendar.DATE, 1);
		Date nextD = c.getTime();

		String nextDay = dateFormat.format(nextD);
		final String nextDayRD = dateFormatRD.format(nextD);

		String tomorrow = "<b><font size=20>"+nextDay+"</font></b>";

		todayTxtVw = (TextView) findViewById(R.id.today_txt_vw);
		todaySmallTxtVw = (TextView) findViewById(R.id.today_small_txt_vw);
		todayTxtLine = (TextView) findViewById(R.id.today_txt_line);

		tomorrowTxtVw = (TextView) findViewById(R.id.tomorrow_txt_vw);
		tomorrowSmallTxtVw = (TextView) findViewById(R.id.tomorrow_small_txt_vw);
		tomorrowTxtLine = (TextView) findViewById(R.id.tomorrow_txt_line);

		doneBtn = (Button) findViewById(R.id.booking_done);
		cancelBtn = (Button) findViewById(R.id.booking_cancel);

		todayLayout = (LinearLayout) findViewById(R.id.today_layout);
		tomorrowLayout = (LinearLayout) findViewById(R.id.tomorrow_layout);

		openTime = (TextView) findViewById(R.id.open_time);
		closeTime = (TextView) findViewById(R.id.close_time);
		closeBtn = (TextView) findViewById(R.id.close_dialog);

		String openT = "<b>Opening time : <font color=#000000>10:00 AM</font</b>";
		openTime.setText(Html.fromHtml(openT));
		String closeT = "<b>Closing time : <font color=#000000>09:00 PM</font</b>";
		closeTime.setText(Html.fromHtml(closeT));

		todayTxtVw.setText(Html.fromHtml(today));
		tomorrowTxtVw.setText(Html.fromHtml(tomorrow));

		closeBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				backBtn.setEnabled(true);
				reserveLay.setVisibility(View.GONE);
				inactiveLay.setVisibility(View.GONE);
			}
		});

		cancelBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				backBtn.setEnabled(true);
				reserveLay.setVisibility(View.GONE);
				inactiveLay.setVisibility(View.GONE);
			}
		});

		doneBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				backBtn.setEnabled(true);
				replanVisitProductMethod();
			}
		});

		todayLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				editor.putString(GlobalDataCls.DateReserve, currentDateRD);
				editor.commit();

				todayTxtVw.setTextColor(Color.parseColor("#FF4949"));
				todaySmallTxtVw.setTextColor(Color.parseColor("#FF4949"));
				todayTxtLine.setVisibility(View.VISIBLE);


				tomorrowTxtVw.setTextColor(Color.parseColor("#888888"));
				tomorrowSmallTxtVw.setTextColor(Color.parseColor("#888888"));
				tomorrowTxtLine.setVisibility(View.INVISIBLE);

				ft = getSupportFragmentManager().beginTransaction();
				ft.replace(R.id.dialog_reserve_layout, fragTodayByStore);
				ft.commit();
			}
		});

		tomorrowLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				editor.putString(GlobalDataCls.DateReserve, nextDayRD);
				editor.commit();

				todayTxtVw.setTextColor(Color.parseColor("#888888"));
				todaySmallTxtVw.setTextColor(Color.parseColor("#888888"));
				todayTxtLine.setVisibility(View.INVISIBLE);


				tomorrowTxtVw.setTextColor(Color.parseColor("#FF4949"));
				tomorrowSmallTxtVw.setTextColor(Color.parseColor("#FF4949"));
				tomorrowTxtLine.setVisibility(View.VISIBLE);

				ft = getSupportFragmentManager().beginTransaction();
				ft.replace(R.id.dialog_reserve_layout, fragTomorrowByStore);
				ft.commit();
			}
		});
	}

	private void replanVisitProductMethod() {

		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.dialog_category_layout);
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		dialog.setCancelable(false);
		dialog.show();

		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.put("reserve_id", GlobalDataCls.proId);
			jsonObject.put("from",preferences.getString(GlobalDataCls.DateReserve, null)+"T"+ preferences.getString(GlobalDataCls.TimeReserveFrom, null));
			jsonObject.put("to", preferences.getString(GlobalDataCls.DateReserve, null)+ "T"+preferences.getString(GlobalDataCls.TimeReserveTo, null));
		} catch (JSONException e) {
			e.printStackTrace();
		}

		Log.d("replanVisitData", jsonObject.toString());

		String url = UrlAll.replanVisitDetail;
		HashMap<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("Authorization", "Token "+preferences.getString(GlobalDataCls.toKen, null));

		ApiCallMethods apiCallMethods = new ApiCallMethods(){
			@Override
			public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
				return new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						backBtn.setEnabled(true);
						reserveLay.setVisibility(View.GONE);
						inactiveLay.setVisibility(View.GONE);
						dialog.dismiss();
						showHideLayoutMethod();
					}
				};
			}
		};
		apiCallMethods.volleyJSONObjectPOST(this, url, jsonObject, headers, dialog);
	}

/**
 *  Cancel Visit
 * **/
	private void cancelVisitMethod(final Dialog dialogg) {

		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.dialog_category_layout);
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		dialog.setCancelable(false);
		dialog.show();

		String url = UrlAll.cancelVisitDetail;
		HashMap<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("Authorization", "Token "+preferences.getString(GlobalDataCls.toKen, null));

		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.put("status","cancelled");
			jsonObject.put("reserve_id", GlobalDataCls.proId);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		Log.d("cancelVisitData", jsonObject.toString());

		dialogg.dismiss();

		ApiCallMethods apiCallMethods = new ApiCallMethods(){
			@Override
			public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
				return new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						dialog.dismiss();
						startActivity(new Intent(VisitDetailActivity.this, OrdersActivity.class));
					}
				};
			}
		};
		apiCallMethods.volleyJSONObjectPOST(this, url, jsonObject, headers, dialog);
	}



	private void initWishlist() {

		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.dialog_category_layout);
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		/*dialog.setCancelable(false);
		dialog.show();*/

		String url = UrlAll.WishlistUrl;

		HashMap headers = new HashMap();
		headers.put("Authorization", "Token "+preferences.getString(GlobalDataCls.toKen, null));

		ApiCallMethods apiCallMethods = new ApiCallMethods(){
			@Override
			public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
				return new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {

						Log.d("res_wishlist", response.toString());

						try {
							if (response.get("data").toString() != "" && response.get("data").toString().length() > 0) {

								ItemWishListPojo itemList = new Gson().fromJson(response.toString(), ItemWishListPojo.class);

								if (itemList.getData() != null) {

									favDotCounter.setText(String.valueOf(itemList.getData().size()));
								}
							}else {
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				};
			}
		};
		apiCallMethods.volleyJSONObjectGET(this, url, headers, dialog);
	}
}
