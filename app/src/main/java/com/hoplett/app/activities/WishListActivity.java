package com.hoplett.app.activities;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.adapters.AdaptrWishList;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.wishlist.ItemWishListPojo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class WishListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    ImageButton backBtn;
    LinearLayout wishListLayout, noItemInWishList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);

        recyclerView = (RecyclerView) findViewById(R.id.recyclr_vw_wishlist);

        preferences = getSharedPreferences(GlobalDataCls.PreferencesName, MODE_PRIVATE);
        editor = preferences.edit();

        backBtn = (ImageButton) findViewById(R.id.back_btn);

        wishListLayout = (LinearLayout) findViewById(R.id.wishlist_layout);
        noItemInWishList = (LinearLayout) findViewById(R.id.no_item_in_wishlist_layout);

        noItemInWishList.setVisibility(View.GONE);
        wishListLayout.setVisibility(View.VISIBLE);

        init();


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
						/*Intent intent = new Intent(WishListActivity.this, HopletHome.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						intent.addCategory(Intent.CATEGORY_HOME);
						startActivity(intent);*/
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
			/*Intent intent = new Intent(WishListActivity.this, HopletHome.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.addCategory(Intent.CATEGORY_HOME);
			startActivity(intent);*/
        super.onBackPressed();
    }

    private void init() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        String url = UrlAll.WishlistUrl;

        HashMap headers = new HashMap();
        String token = preferences.getString(GlobalDataCls.toKen, null);
        headers.put("Authorization", "Token " + token);

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("response_fav_wishlist", response.toString());

                        try {
                            if (response.get("data").toString() != "" && response.get("data").toString().length() > 0) {

                                noItemInWishList.setVisibility(View.GONE);
                                wishListLayout.setVisibility(View.VISIBLE);

                                ItemWishListPojo itemList = new Gson().fromJson(response.toString(), ItemWishListPojo.class);

                                if (itemList.getData() != null) {

                                    /*HopletHome.favoriteCount.setText(String.valueOf(itemList.getData().size()));*/

                                    GlobalDataCls.FavoriteCounter = itemList.getData().size();

                                    if (itemList.getData().toString() != "" || itemList.getData().toString() != "null") {

                                        AdaptrWishList adapter = new AdaptrWishList(WishListActivity.this, itemList);

                                        recyclerView.setHasFixedSize(true);
                                        recyclerView.setLayoutManager(new GridLayoutManager(WishListActivity.this, 2));
                                        recyclerView.setAdapter(adapter);
                                    } else {
                                        Toast.makeText(WishListActivity.this, "No any item is available in your wishlist.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } else {
                                noItemInWishList.setVisibility(View.VISIBLE);
                                wishListLayout.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(this, url, headers, dialog);
    }
}
