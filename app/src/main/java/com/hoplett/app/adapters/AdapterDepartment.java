package com.hoplett.app.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.hoplett.app.R;
import com.hoplett.app.activities.HopletHome;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.pojoClasses.departmentPojo.Datum;
import com.hoplett.app.pojoClasses.departmentPojo.DepartmentClsPojo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LOCATION_SERVICE;

public class  AdapterDepartment extends RecyclerView.Adapter<AdapterDepartment.RecyclerViewHolder> implements /*View.OnClickListener,*/ Filterable {

    Context context;
    DepartmentClsPojo list;
    LayoutInflater inflater;
    private List<Datum> listFiltered;

    SharedPreferences preferences;
    private final SharedPreferences.Editor editor;

    public static boolean isClickable = true;

    public AdapterDepartment(Context context, DepartmentClsPojo list) {

        this.context = context;
        this.list = list;
        this.listFiltered = list.getData();
        inflater = LayoutInflater.from(context);
        preferences = context.getSharedPreferences(GlobalDataCls.PreferencesName,
                Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View view = inflater.inflate(R.layout.item_department_lay, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, final int position) {

        textView.setText(list.getData().get(position).getName());
        String imgStr = String.valueOf(list.getData().get(position).getImage());
        if (imgStr!= null && imgStr != "" && imgStr != "null"){

            /*String img = imgStr.substring(0, imgStr.length()-4)+"raw=1";*/
            Picasso.with(context).load(imgStr).placeholder(R.drawable.loading_default_image).error(R.drawable.default_img_back).into(itemBackLayout);
        }else {//itemBackLayout.setImageResource(R.drawable.default_img_back);
        itemBackLayout.setBackgroundResource(R.drawable.default_img_back);}


        textView.measure(0, 0);
        int width = textView.getMeasuredWidth();
        int height = textView.getMeasuredHeight();

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) txtVwBack.getLayoutParams();

        /*Log.d("view_height", String.valueOf(height));
        Log.d("view_height", String.valueOf(height-5));
        Log.d("view_height", String.valueOf(height-15));*/
        params.height = height-25;
        params.width = width;
        txtVwBack.setLayoutParams(params);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putString(GlobalDataCls.DeptName, list.getData().get(position).getName());
                editor.putString(GlobalDataCls.DepartmentId, list.getData().get(position).getId().toString());
                editor.commit();

                if (GlobalDataCls.departmentId == -1) {
                    GlobalDataCls.departmentId = list.getData().get(position).getId();
                } else {
                    if (GlobalDataCls.departmentId != list.getData().get(position).getId()) {
                        GlobalDataCls.departmentId = list.getData().get(position).getId();
                        GlobalDataCls.listColor.clear();
                        GlobalDataCls.listBrand.clear();
                        GlobalDataCls.listCategory.clear();
                        GlobalDataCls.listSize.clear();
                    }
                }

                LocationManager service = (LocationManager) context.getSystemService(LOCATION_SERVICE);
                boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

                // Check if enabled and if not send user to the GPS settings
                if (!enabled) {
			/*Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);*/
                    Toast.makeText(context, "Enable Your GPS", Toast.LENGTH_SHORT).show();
                }else {
                    HopletHome hopletHome = (HopletHome) context;
                    hopletHome.startFragCategoryByProduct();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.getData().size();
    }

    TextView textView, txtVwBack;
    public CardView layout;
    ImageView itemBackLayout;

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);

            textView = (TextView) itemView.findViewById(R.id.txt_vw_dept_item);
            txtVwBack = (TextView) itemView.findViewById(R.id.txt_vw_dept_item_back);
            layout = (CardView) itemView.findViewById(R.id.item_department_layout);
            itemBackLayout = (ImageView) itemView.findViewById(R.id.item_back_layout);
        }
    }

	/*@Override
	public void onClick(View v) {

		if (!isClickable){
			return;
		}
	}*/


    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    listFiltered = list.getData();
                } else {
                    List<Datum> filteredList = new ArrayList<>();
                    for (Datum row : list.getData()) {
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    listFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults results) {

                listFiltered = (ArrayList<Datum>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
