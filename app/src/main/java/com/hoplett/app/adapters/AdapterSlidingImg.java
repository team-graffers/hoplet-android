package com.hoplett.app.adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.hoplett.app.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

//
// Created by Sandy on 05-Jun-19 at 2:42 PM.
//
public class AdapterSlidingImg extends PagerAdapter {

    private ArrayList<String> IMAGES;
    private LayoutInflater inflater;
    private Context context;
    int pos;

    public AdapterSlidingImg(Context context, ArrayList<String> IMAGES, int pos) {
        this.IMAGES = IMAGES;
        this.context = context;
        this.pos = pos;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup view, int position) {

        View imageLayout = inflater.inflate(R.layout.item_img_slider_pro_detail, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);


  //      imageView.setImageResource(IMAGES.get(position));
        Picasso.with(context).load(IMAGES.get(position)).placeholder(R.drawable.loading_default_image).error(R.drawable.default_img_back).into(imageView);

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(@Nullable Parcelable state, @Nullable ClassLoader loader) {
    }

    @Nullable
    @Override
    public Parcelable saveState() {
        return null;
    }
}
