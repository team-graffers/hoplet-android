package com.hoplett.app.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.hoplett.app.R;
import com.hoplett.app.activities.ByProductActivity;
import com.hoplett.app.activities.HopletHome;
import com.hoplett.app.activities.SignInActivity;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.byProductShopping.productList.ResultList;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sandy on 15-03-2019 at 05:26 PM.
 */
public class AdaptrSearchProduct extends RecyclerView.Adapter<AdaptrSearchProduct.RecyclerViewHolder> {

    Context context;
    LayoutInflater inflater;

    ArrayList<Boolean> listBool;
    List<ResultList> list;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public AdaptrSearchProduct(Context context, List<ResultList> list, ArrayList<Boolean> listBool) {
        this.context = context;
        this.list = list;
        this.listBool = listBool;
        inflater = LayoutInflater.from(context);

        preferences = context.getSharedPreferences(GlobalDataCls.PreferencesName, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = inflater.inflate(R.layout.item_cat_by_product_layout, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerViewHolder holder, final int position) {

        String imgStr = list.get(position).getImage();
        if (imgStr!= null && imgStr != "" && imgStr != "null"){

            String img = imgStr.substring(0, imgStr.length()-4)+"raw=1";
            Picasso.with(context).load(img).fit().centerCrop().placeholder(R.drawable.loading_default_image).error(R.drawable.default_img_back).into(holder.imgProduct);
        }else {holder.imgProduct.setImageResource(R.drawable.default_img_back);}

        if (listBool.get(position)) {
            holder.imgFavorite.setBackgroundResource(R.drawable.heart_product_selected);
        }else {
            holder.imgFavorite.setBackgroundResource(R.drawable.heart_product_not_selected);
        }

        holder.nameBrand.setText(list.get(position).getBrand());
        holder.nameProduct.setText(list.get(position).getProduct());

        String price = String.valueOf(list.get(position).getPrice());

        String discPrice = list.get(position).getDiscPrice();

        if (discPrice.equals(price)){
            holder.imgPrice.setVisibility(View.GONE);
            holder.priceOld.setVisibility(View.GONE);
            holder.discount.setVisibility(View.GONE);
        }else {
            holder.imgPrice.setVisibility(View.VISIBLE);
            holder.priceOld.setVisibility(View.VISIBLE);
            holder.discount.setVisibility(View.VISIBLE);
        }

        price = "<del>" + price + "</del>";

        if (discPrice != null && discPrice != "null" && discPrice.length() > 0) {

            String dscntPercentage = String.valueOf(100 - Math.round((Integer.parseInt(list.get(position).getDiscPrice()) * 100) / list.get(position).getPrice()));

            String discnt = " (" + "<font color=#f4684e>" + dscntPercentage + "% Off</font>" + ")";

            holder.priceProduct.setText(list.get(position).getDiscPrice());
            holder.priceOld.setText(Html.fromHtml(price));
            holder.discount.setText(Html.fromHtml(discnt));
            holder.availableStores.setVisibility(View.GONE);
        }

        boolean b = true;
        holder.imgFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (preferences.getString(GlobalDataCls.toKen, null) != null){
                    if (!listBool.get(position)){
                        addToFavourite(holder.imgFavorite, list.get(position).getProductId().toString(), position, list.get(position).getSkuId().toString());
                    }else {
                        removeFromFavourite(holder.imgFavorite, list.get(position).getProductId().toString(), position, list.get(position).getSkuId().toString());
                    }
                }else {
                    context.startActivity(new Intent(context, SignInActivity.class));
                }
            }
        });

        holder.imgProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putString(GlobalDataCls.FragmentName, "FragProductDetail");
                editor.putString(GlobalDataCls.previousFragmentName, "FragSearchProduct");
                editor.putString(GlobalDataCls.ProductId, list.get(position).getProductId().toString());
                editor.putString(GlobalDataCls.PriceNew, list.get(position).getPrice().toString());
                editor.putString(GlobalDataCls.ProductImg, list.get(position).getImage());

                if (listBool.get(position)){
                    GlobalDataCls.proDetailBool = true;
                }else {
                    GlobalDataCls.proDetailBool = false;
                }
                editor.commit();
                GlobalDataCls.ItemPositionProductList = position;

                Intent intent = new Intent(context, ByProductActivity.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        ImageView imgProduct;
        ImageView imgFavorite, imgPrice;
        TextView nameBrand, nameProduct, priceProduct, availableStores, priceOld, discount;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);

            imgProduct = (ImageView) itemView.findViewById(R.id.item_by_product_img_product);
            imgFavorite = (ImageView) itemView.findViewById(R.id.item_by_product_img_favorite);
            imgPrice = (ImageView) itemView.findViewById(R.id.price_img);

            nameBrand = (TextView) itemView.findViewById(R.id.item_by_product_brand_name);
            nameProduct = (TextView) itemView.findViewById(R.id.item_by_product_name_product);
            priceProduct = (TextView) itemView.findViewById(R.id.item_by_product_price_product);
            availableStores = (TextView) itemView.findViewById(R.id.item_by_product_avail_stores);
            priceOld = (TextView) itemView.findViewById(R.id.item_by_product_price_product_old);
            discount = (TextView) itemView.findViewById(R.id.item_by_product_discount);
        }
    }


    /**
     *  Add item To Wish list
     * **/
    private void addToFavourite(final ImageView imgFavorite, String pro_id, final int position, String skuId) {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        String url = UrlAll.WishlistAddUrl+pro_id+"&sku_idd="+skuId;
        String token = preferences.getString(GlobalDataCls.toKen, null);

        HashMap headers = new HashMap();
        headers.put("Authorization", "Token " + token);

        ApiCallMethods apiCallMethods = new ApiCallMethods(){
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        dialog.dismiss();
                        imgFavorite.setBackgroundResource(R.drawable.heart_product_selected);
                        GlobalDataCls.FavoriteCounter++;
                        HopletHome.favoriteCount.setText(String.valueOf(GlobalDataCls.FavoriteCounter));

                        listBool.set(position, true);
                        try {
                            if (response.getInt("count")>0) {
                                HopletHome.favoriteCount.setText(String.valueOf(response.getInt("count")));
                            }else {HopletHome.favoriteCount.setText("");}
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        notifyDataSetChanged();
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(context, url, headers, dialog);
    }

    /**
     *  Remove item From Wish list
     * **/
    private void removeFromFavourite(final ImageView imgFavorite, String pro_id, final int position, String skuId) {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        String url = UrlAll.WishlistRemoveUrl+pro_id+"&sku_idd="+skuId;
        String token = preferences.getString(GlobalDataCls.toKen, null);

        HashMap headers = new HashMap();
        headers.put("Authorization", "Token " + token);

        ApiCallMethods apiCallMethods = new ApiCallMethods(){
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        dialog.dismiss();

                        imgFavorite.setBackgroundResource(R.drawable.heart_product_not_selected);

                        if (GlobalDataCls.FavoriteCounter != 0){

                            GlobalDataCls.FavoriteCounter--;
                            HopletHome.favoriteCount.setText(String.valueOf(GlobalDataCls.FavoriteCounter));

                            if (GlobalDataCls.FavoriteCounter == 0){
                                HopletHome.favoriteCount.setText(String.valueOf(""));
                            }
                        }else {
                            HopletHome.favoriteCount.setText(String.valueOf(""));
                        }
                        listBool.set(position, false);
                        try {
                            if (response.getInt("count")>0) {
                                HopletHome.favoriteCount.setText(String.valueOf(response.getInt("count")));
                            }else {HopletHome.favoriteCount.setText("");}
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        notifyDataSetChanged();
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(context, url, headers, dialog);
    }
}
