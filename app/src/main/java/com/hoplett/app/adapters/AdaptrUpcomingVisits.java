package com.hoplett.app.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hoplett.app.R;
import com.hoplett.app.get_set_classes.ItemUpcomingVisits;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdaptrUpcomingVisits extends RecyclerView.Adapter<AdaptrUpcomingVisits.RecyclerViewHolder> {

	Context context;
	ArrayList<ItemUpcomingVisits> list;
	LayoutInflater inflater;

	public AdaptrUpcomingVisits(Context context, ArrayList<ItemUpcomingVisits> list) {
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(context);
	}

	@NonNull
	@Override
	public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
		View view = inflater.inflate(R.layout.item_upcoming_visits_layout, parent, false);
		RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
		return viewHolder;
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {

		holder.storeName.setText(list.get(position).getStoreName());
		holder.address.setText(list.get(position).getAddress());
		holder.distance.setText(list.get(position).getDistance());
		holder.time.setText(list.get(position).getTimeDate());
		holder.status.setText(list.get(position).getStatus());

		String imgStr = String.valueOf(list.get(position).getImgProduct());
		if (imgStr!= null && imgStr != "" && imgStr != "null"){

			String img = imgStr.substring(0, imgStr.length()-4)+"raw=1";
			Picasso.with(context).load(img).fit().centerCrop().placeholder(R.drawable.loading_default_image).error(R.drawable.default_img_back).into(holder.imgProduct);
		}else {holder.imgProduct.setImageResource(R.drawable.default_img_back);}
	}

	@Override
	public int getItemCount() {
		return list.size();
	}

	public class RecyclerViewHolder extends RecyclerView.ViewHolder {

		TextView storeName, address, distance, time, status;
		ImageView imgProduct;

		public RecyclerViewHolder(@NonNull View itemView) {
			super(itemView);

			storeName = (TextView) itemView.findViewById(R.id.store_name_upcmng_vst);
			address = (TextView) itemView.findViewById(R.id.address_upcmng_vst);
			distance = (TextView) itemView.findViewById(R.id.distance_upcmng_vst);
			time = (TextView) itemView.findViewById(R.id.time_upcmng_vst);
			status = (TextView) itemView.findViewById(R.id.status_upcmng_vst);
			imgProduct = (ImageView) itemView.findViewById(R.id.img_product_upcmng_vst);
		}
	}
}
