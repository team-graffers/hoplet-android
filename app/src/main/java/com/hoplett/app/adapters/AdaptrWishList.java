package com.hoplett.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hoplett.app.R;
import com.hoplett.app.activities.ByProductActivity;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.pojoClasses.wishlist.ItemWishListPojo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Sandy on 18-03-2019 at 10:12 AM.
 */
public class AdaptrWishList extends RecyclerView.Adapter<AdaptrWishList.RecyclerViewHolder> {

    private final SharedPreferences preferences;
    private final SharedPreferences.Editor editor;
    Context context;
    ItemWishListPojo list;
    LayoutInflater inflater;

    public AdaptrWishList(Context context, ItemWishListPojo list) {
        this.context = context;
        this.list = list;

        preferences = context.getSharedPreferences(GlobalDataCls.PreferencesName, MODE_PRIVATE);
        editor = preferences.edit();

        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = inflater.inflate(R.layout.item_wish_list, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, final int position) {

        String imgStr = String.valueOf(list.getData().get(position).getImage());
        if (imgStr!= null && imgStr != "" && imgStr != "null"){

            String img = imgStr.substring(0, imgStr.length()-4)+"raw=1";
            Picasso.with(context).load(img).fit().centerCrop().placeholder(R.drawable.loading_default_image).error(R.drawable.default_img_back).into(holder.proImg);
        }else {holder.proImg.setImageResource(R.drawable.default_img_back);}

        Bitmap mbitmap = ((BitmapDrawable) holder.proImg.getDrawable()).getBitmap();

        if (mbitmap != null) {

            Bitmap imageRounded = Bitmap.createBitmap(mbitmap.getWidth(), mbitmap.getHeight(), mbitmap.getConfig());
            Canvas canvas = new Canvas(imageRounded);
            Paint mpaint = new Paint();
            mpaint.setAntiAlias(true);
            mpaint.setShader(new BitmapShader(mbitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
            canvas.drawRoundRect((new RectF(0, 0, mbitmap.getWidth(), mbitmap.getHeight())), 10, 10,
                    mpaint);// Round Image Corner 100 100 100 100
            holder.proImg.setImageBitmap(imageRounded);
        }

        holder.parentName.setText(list.getData().get(position).getBrand());
        holder.productName.setText(list.getData().get(position).getProductName());
        holder.pricePro.setText(list.getData().get(position).getPrice().toString());

        if (list.getData().get(position).getShopCount() > 1) {
            holder.storesTotal.setText("(" + list.getData().get(position).getShopCount().toString() + " stores)");
        } else {
            holder.storesTotal.setText("(" + list.getData().get(position).getShopCount().toString() + " store)");
        }

        holder.viewStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GlobalDataCls.sizeList = new ArrayList<>();
                GlobalDataCls.sizeSkuList = new ArrayList<>();
                for (int i=0; i<list.getData().get(position).getSize().size(); i++){
                    GlobalDataCls.sizeList.add(list.getData().get(position).getSize().get(i).getName());
                    GlobalDataCls.sizeSkuList.add(list.getData().get(position).getSize().get(i).getSkuId().toString());
                }
                GlobalDataCls.proDetailBool = true;
                editor.putString(GlobalDataCls.FragmentName, "FragProductDetail");
                editor.putString(GlobalDataCls.ProductId, String.valueOf(list.getData().get(position).getProductId()));
                editor.putString(GlobalDataCls.Sku_ID, list.getData().get(position).getSkuId().toString());
                editor.commit();
                context.startActivity(new Intent(context, ByProductActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.getData().size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        ImageView proImg, favImg;
        TextView parentName, productName, pricePro, storesTotal;
        Button viewStore;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);

            proImg = (ImageView) itemView.findViewById(R.id.pro_img_vw);
            favImg = (ImageView) itemView.findViewById(R.id.fav_img_vw);

            parentName = (TextView) itemView.findViewById(R.id.parent_name);
            productName = (TextView) itemView.findViewById(R.id.item_by_product_name_product);
            pricePro = (TextView) itemView.findViewById(R.id.price_item);
            storesTotal = (TextView) itemView.findViewById(R.id.total_stores);

            viewStore = (Button) itemView.findViewById(R.id.view_store_btn);
        }
    }
}
