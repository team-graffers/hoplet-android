package com.hoplett.app.adapters.byProduct;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.hoplett.app.R;
import com.hoplett.app.activities.ByProductActivity;
import com.hoplett.app.activities.HopletHome;
import com.hoplett.app.activities.SignInActivity;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.fragments.bystore.FragProductList;
import com.hoplett.app.pojoClasses.byProductShopping.productList.ResultList;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AdaptrCatByProduct extends RecyclerView.Adapter<AdaptrCatByProduct.RecyclerViewHolder> {

	Context context;
//	ProductListByProductItems list;
	ArrayList<Boolean> listBool;
	LayoutInflater inflater;
	SharedPreferences preferences;
	SharedPreferences.Editor editor;
	List<ResultList> list;

	public AdaptrCatByProduct(Context context, List<ResultList> list, ArrayList<Boolean> listBool) {
		this.context = context;
		this.list = list;
		this.listBool = listBool;
		inflater = LayoutInflater.from(context);

		preferences = context.getSharedPreferences(GlobalDataCls.PreferencesName, Context.MODE_PRIVATE);
		editor = preferences.edit();
	}

	@NonNull
	@Override
	public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

		View view = inflater.inflate(R.layout.item_cat_by_product_layout, parent, false);
		RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
		return viewHolder;
	}

	@Override
	public void onBindViewHolder(@NonNull final RecyclerViewHolder holder, final int position) {

		String imgStr = String.valueOf(list.get(position).getImage());
		if (imgStr!= null && imgStr != "" && imgStr != "null"){

			String img = imgStr.substring(0, imgStr.length()-4)+"raw=1";
			Picasso.with(context).load(img).fit().centerCrop().placeholder(R.drawable.loading_default_image).error(R.drawable.default_img_back).into(holder.imgProduct);
		}else {holder.imgProduct.setImageResource(R.drawable.default_img_back);}

		holder.nameBrand.setText(list.get(position).getBrand());
		holder.nameProduct.setText(list.get(position).getProduct());

		if (String.valueOf(list.get(position).getPrice()).equals(list.get(position).getDiscPrice())){

			holder.imgPriceSign.setVisibility(View.GONE);
			holder.priceOld.setVisibility(View.GONE);
			holder.discount.setVisibility(View.GONE);
		}else {

			holder.imgPriceSign.setVisibility(View.VISIBLE);
			holder.priceOld.setVisibility(View.VISIBLE);
			holder.discount.setVisibility(View.VISIBLE);
			String price = "<del>" + list.get(position).getPrice() + "</del>";

			String dscntPercentage = String.valueOf(100 - Math.round((Integer.parseInt(list.get(position).getDiscPrice()) * 100) / list.get(position).getPrice()));

			String discnt = " (" + "<font color=#f4684e>" + dscntPercentage + "% Off</font>" + ")";

			holder.priceOld.setText(Html.fromHtml(price));
			holder.discount.setText(Html.fromHtml(discnt));
		}
		holder.priceProduct.setText(list.get(position).getDiscPrice());
		holder.availableStores.setText("("+list.get(position).getShopCount().toString()+" Stores)");

		if (listBool.get(position)){
			holder.imgFavorite.setBackgroundResource(R.drawable.heart_product_selected);
		}else {holder.imgFavorite.setBackgroundResource(R.drawable.heart_product_not_selected);}

		boolean b = true;
		holder.imgFavorite.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				 if (preferences.getString(GlobalDataCls.toKen, null) != null){

					 if (!listBool.get(position)){
					 	addToFavourite(holder.imgFavorite, list.get(position).getProductId().toString(), position, list.get(position).getSkuId().toString());
					 }else {
					 	removeFromFavourite(holder.imgFavorite, list.get(position).getProductId().toString(), position, list.get(position).getSkuId().toString());
					 }
				 }else{

				 	editor.putString(GlobalDataCls.SignInActivityName, "HopletHome");
				 	editor.putString(GlobalDataCls.SignInFragName, "FragCategoryByProduct");
				 	editor.commit();
				 	Intent intent = new Intent(context, SignInActivity.class);
				 	context.startActivity(intent);
				 }
			}
		});

		holder.imgProduct.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				editor.putString(GlobalDataCls.FragmentName, "FragProductDetail");
				editor.putString(GlobalDataCls.ProductId, list.get(position).getProductId().toString());
				editor.putString(GlobalDataCls.PriceNew, list.get(position).getDiscPrice());
				editor.putString(GlobalDataCls.PriceOld, list.get(position).getPrice().toString());
				editor.putString(GlobalDataCls.ProductImg, list.get(position).getImage());
				editor.putString(GlobalDataCls.Sku_ID, list.get(position).getSkuId().toString());

				 Set<String> set = new HashSet<>();
				 set.add("true");
				 set.add("false");

				 editor.putStringSet("preferencesList", set);

				editor.commit();

				 /*Toast.makeText(context, String.valueOf(preferences.getStringSet("preferencesList", null).size()), Toast.LENGTH_SHORT).show();*/

				GlobalDataCls.ItemPositionProductList = position;

				if (listBool.get(position)){
					GlobalDataCls.proDetailBool = true;
				}else {
					GlobalDataCls.proDetailBool = false;
				}

				Intent intent = new Intent(context, ByProductActivity.class);
				context.startActivity(intent);
			}
		});
	}

	@Override
	public int getItemCount() {
		return list.size();
	}

	public class RecyclerViewHolder extends RecyclerView.ViewHolder {

		private final ImageView imgPriceSign;
		ImageView imgProduct;
		ImageButton imgFavorite;
		TextView nameBrand, nameProduct, priceProduct, availableStores, priceOld, discount;

		public RecyclerViewHolder(@NonNull View itemView) {
			super(itemView);

			imgProduct = (ImageView) itemView.findViewById(R.id.item_by_product_img_product);
			imgFavorite = (ImageButton) itemView.findViewById(R.id.item_by_product_img_favorite);

			nameBrand = (TextView) itemView.findViewById(R.id.item_by_product_brand_name);
			nameProduct = (TextView) itemView.findViewById(R.id.item_by_product_name_product);
			priceProduct = (TextView) itemView.findViewById(R.id.item_by_product_price_product);
			availableStores = (TextView) itemView.findViewById(R.id.item_by_product_avail_stores);
			priceOld = (TextView) itemView.findViewById(R.id.item_by_product_price_product_old);
			discount = (TextView) itemView.findViewById(R.id.item_by_product_discount);
			imgPriceSign = (ImageView) itemView.findViewById(R.id.price_img);
		}
	}

/**
 *  Add item To Wish list
 * **/
	private void addToFavourite(final ImageView imgFavorite, String pro_id, final int position, String skuId) {

		final Dialog dialog = new Dialog(context);
		dialog.setContentView(R.layout.dialog_category_layout);
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		dialog.setCancelable(false);
		dialog.show();

		String url = UrlAll.WishlistAddUrl+pro_id+"&sku_idd="+skuId;
		String token = preferences.getString(GlobalDataCls.toKen, null);

		HashMap headers = new HashMap();
		headers.put("Authorization", "Token " + token);

		ApiCallMethods apiCallMethods = new ApiCallMethods(){
			@Override
			public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
				return new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {

						Log.d("response_fav_add", response.toString());
						dialog.dismiss();
						imgFavorite.setBackgroundResource(R.drawable.heart_product_selected);
						GlobalDataCls.FavoriteCounter++;
						HopletHome.favoriteCount.setText(String.valueOf(GlobalDataCls.FavoriteCounter));

						listBool.set(position, true);
						notifyDataSetChanged();

						try {
							if (response.getInt("count")>0) {
								GlobalDataCls.FavoriteCounter = response.getInt("count");
								HopletHome.favoriteCount.setText(String.valueOf(GlobalDataCls.FavoriteCounter));
							}else {FragProductList.favoriteCount.setText("");}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				};
			}
		};
		apiCallMethods.volleyJSONObjectGET(context, url, headers, dialog);
	}

	/**
 *  Remove item From Wish list
 * **/
	private void removeFromFavourite(final ImageView imgFavorite, String pro_id, final int position, String skuId) {

		final Dialog dialog = new Dialog(context);
		dialog.setContentView(R.layout.dialog_category_layout);
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		dialog.setCancelable(false);
		dialog.show();

		String url = UrlAll.WishlistRemoveUrl+pro_id+"&sku_idd="+skuId;
		String token = preferences.getString(GlobalDataCls.toKen, null);

		HashMap headers = new HashMap();
		headers.put("Authorization", "Token " + token);

		ApiCallMethods apiCallMethods = new ApiCallMethods(){
			@Override
			public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
				return new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {

						Log.d("response_fav_remove", response.toString());

						dialog.dismiss();

						imgFavorite.setBackgroundResource(R.drawable.heart_product_not_selected);

						if (GlobalDataCls.FavoriteCounter != 0){

							GlobalDataCls.FavoriteCounter--;
							HopletHome.favoriteCount.setText(String.valueOf(GlobalDataCls.FavoriteCounter));

							if (GlobalDataCls.FavoriteCounter == 0){
								HopletHome.favoriteCount.setText(String.valueOf(""));
							}
						}else {
							HopletHome.favoriteCount.setText(String.valueOf(""));
						}
						listBool.set(position, false);
						notifyDataSetChanged();

						try {
							if (response.getInt("count")>0) {
								GlobalDataCls.FavoriteCounter = response.getInt("count");
								HopletHome.favoriteCount.setText(String.valueOf(GlobalDataCls.FavoriteCounter));
							}else {FragProductList.favoriteCount.setText("");}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				};
			}
		};
		apiCallMethods.volleyJSONObjectGET(context, url, headers, dialog);
	}


	Bitmap bitmap;

	public class getImageFromUrl extends AsyncTask<String, Void, Bitmap> {

		ImageView imageView;

		public getImageFromUrl(ImageView imageView){
			this.imageView = imageView;
		}

		@Override
		protected Bitmap doInBackground(String... strings) {

			String urlDisplay = strings[0];

			bitmap = null;

			try{
				InputStream srt = new java.net.URL(urlDisplay).openStream();
				bitmap = BitmapFactory.decodeStream(srt);
			}catch (Exception e){

			}
			return bitmap;
		}

		@Override
		protected void onPostExecute(Bitmap bitmap) {
			super.onPostExecute(bitmap);
			imageView.setImageBitmap(bitmap);
		}
	}
}
