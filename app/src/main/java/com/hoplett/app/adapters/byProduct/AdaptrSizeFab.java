package com.hoplett.app.adapters.byProduct;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.hoplett.app.R;
import com.hoplett.app.classes.GlobalDataCls;

import java.util.ArrayList;

/**
 * Created by Sandy on 29-03-2019 at 06:54 PM.
 */
public class AdaptrSizeFab extends RecyclerView.Adapter<AdaptrSizeFab.RecyclerVwHolder> {

	Context context;
	ArrayList<String> list;
	ArrayList<String> listSku;
	LayoutInflater inflater;

	SharedPreferences preferences;
	SharedPreferences.Editor editor;
	boolean bool;

	public AdaptrSizeFab(Context context, ArrayList<String> list, ArrayList<String> listSku, boolean bool) {
		this.context = context;
		this.list = list;
		this.listSku = listSku;
		this.bool  = bool;
		inflater = LayoutInflater.from(context);

		preferences = context.getSharedPreferences(GlobalDataCls.PreferencesName,
			Context.MODE_PRIVATE);
		editor = preferences.edit();
	}

	@NonNull
	@Override
	public RecyclerVwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
		View view = inflater.inflate(R.layout.item_product_size_layout, parent, false);
		RecyclerVwHolder vwHolder = new RecyclerVwHolder(view);
		return vwHolder;
	}

	private RadioButton lastCheckedRB = null;
	@Override
	public void onBindViewHolder(@NonNull final RecyclerVwHolder holder, final int position) {

		holder.radioButton.setText(list.get(position));

		String sku_id = preferences.getString(GlobalDataCls.Sku_ID, null);
		String sku_id_size = listSku.get(position);

		Log.d("sku_ids", "sku_id:"+sku_id+"    sku_size:"+sku_id_size);

		if (sku_id.equals(sku_id_size)){

			holder.radioButton.setChecked(true);
			lastCheckedRB = holder.radioButton;
			editor.putString(GlobalDataCls.SizeOfItem, holder.radioButton.getText().toString());
			editor.putString(GlobalDataCls.Sku_ID, listSku.get(position));
			editor.commit();
			bool = false;
		}

		holder.radioButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (lastCheckedRB != null){
					if (lastCheckedRB == (RadioButton) v){
					}else { lastCheckedRB.setChecked(false); }
				}
				lastCheckedRB = (RadioButton) v;
				editor.putString(GlobalDataCls.SizeOfItem, holder.radioButton.getText().toString());
				editor.putString(GlobalDataCls.Sku_ID, listSku.get(position));
				editor.commit();
			}
		});
	}

	@Override
	public int getItemCount() {
		return list.size();
	}

	public class RecyclerVwHolder extends RecyclerView.ViewHolder {

		RadioGroup radioGroup;
		RadioButton radioButton;

		public RecyclerVwHolder(@NonNull View itemView) {
			super(itemView);

//			radioGroup = (RadioGroup) itemView.findViewById(R.id.radio_grp);
			radioButton = (RadioButton) itemView.findViewById(R.id.radio_btn);
		}
	}
}
