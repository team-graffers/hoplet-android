package com.hoplett.app.adapters.byProduct;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hoplett.app.R;
import com.hoplett.app.activities.SignInActivity;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.fragments.byproduct.FragStoreList;
import com.hoplett.app.pojoClasses.byProductShopping.storeList.Datum;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

public class AdaptrStoreListByProduct extends RecyclerView.Adapter<AdaptrStoreListByProduct.RecyclerViewHolder> {

	Context context;
	List<Datum> list;
	LayoutInflater inflater;
	SharedPreferences preferences;
	SharedPreferences.Editor editor;

	public AdaptrStoreListByProduct(Context context, List<Datum> list) {
		this.context = context;
		this.list = list;

		preferences = context.getSharedPreferences(GlobalDataCls.PreferencesName,
			Context.MODE_PRIVATE);
		editor = preferences.edit();

		inflater = LayoutInflater.from(context);
	}

	@NonNull
	@Override
	public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
		View view = inflater.inflate(R.layout.item_store_list_by_product_layout, parent, false);
		RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
		return viewHolder;
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerViewHolder holder, final int position) {

		String imgStr = list.get(position).getImage();
		if (imgStr != null && imgStr != "" && imgStr != "null") {

			/*String img = imgStr.substring(0, imgStr.length() - 4) + "raw=1";*/
			Picasso.with(context).load(imgStr).fit().centerCrop().placeholder(R.drawable.loading_default_image).error(R.drawable.default_img_back).into(holder.imgBrand);
		} else {
			holder.imgBrand.setBackgroundResource(R.drawable.default_img_back);
		}

		holder.name.setText(list.get(position).getShopName());
		holder.address.setText(list.get(position).getAddress());

		holder.distance.setText(String.valueOf(new DecimalFormat("###.##").format(list.get(position).getDistance()))+" km away");

		holder.price.setText(list.get(position).getDiscPrice());

		String orgP = list.get(position).getOrgPrice().toString();
		String disP = list.get(position).getDiscPrice();

		if (orgP.equals(disP)){
			holder.oldRupee.setVisibility(View.GONE);
		}else {
			String oldP = "<del>"+list.get(position).getOrgPrice()+"</del>";
			holder.oldPrice.setText(Html.fromHtml(oldP));

			int dscntPrice = Integer.parseInt(list.get(position).getDiscPrice());
			int originalPrice = list.get(position).getOrgPrice();

			String dscntPercentage = String.valueOf(100 - Math.round((dscntPrice*100)/originalPrice));

			String discnt = "(<font color=#f4684e>"+dscntPercentage+"% Off</font>)";
			holder.discount.setText(Html.fromHtml(discnt));
		}

		holder.reserveBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (preferences.getString(GlobalDataCls.toKen, null) != null) {
					editor.putString(GlobalDataCls.Shop_ID, list.get(position).getShopId().toString());
					editor.commit();
					FragStoreList.reserveMethod();
				}else {

					editor.putString(GlobalDataCls.SignInActivityName, "ByProductActivity");
					editor.putString(GlobalDataCls.SignInFragName, "FragStoreList");
					editor.commit();
					Intent intent = new Intent(context, SignInActivity.class);
					context.startActivity(intent);
				}
			}
		});
	}

	@Override
	public int getItemCount() {
		return list.size();
	}

	public class RecyclerViewHolder extends RecyclerView.ViewHolder {

		ImageView imgBrand, oldRupee;
		TextView name, address, distance, price,oldPrice, discount;
		Button reserveBtn;

		public RecyclerViewHolder(@NonNull View itemView) {
			super(itemView);

			imgBrand = (ImageView) itemView.findViewById(R.id.item_img_brand_by_product);

			name = (TextView) itemView.findViewById(R.id.item_brand_name_by_product);
			address = (TextView) itemView.findViewById(R.id.item_address_by_product);
			distance = (TextView) itemView.findViewById(R.id.item_distance_by_product);
			price = (TextView) itemView.findViewById(R.id.item_by_product_price_product);
			oldPrice = (TextView) itemView.findViewById(R.id.item_by_product_price_product_old);
			discount = (TextView) itemView.findViewById(R.id.item_by_product_discount);
			reserveBtn = (Button) itemView.findViewById(R.id.item_reserve_btn_by_product);
			oldRupee = (ImageView) itemView.findViewById(R.id.old_rupee);
		}
	}
}
