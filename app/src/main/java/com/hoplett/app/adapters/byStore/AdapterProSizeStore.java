package com.hoplett.app.adapters.byStore;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.hoplett.app.R;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.fragments.bystore.FragProductDetailByStore;
import com.hoplett.app.pojoClasses.byProductShopping.productDetail.ProductDetail;

public class AdapterProSizeStore extends RecyclerView.Adapter<AdapterProSizeStore.RecyclerViewHolder> {

    Context context;
    ProductDetail list;
    LayoutInflater inflater;
    SharedPreferences preferences, preferencesConfrm;
    SharedPreferences.Editor editor, editorConfrm;

    public AdapterProSizeStore(Context context, ProductDetail list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);

        preferences = context.getSharedPreferences(GlobalDataCls.PreferencesName,
                Context.MODE_PRIVATE);
        editor = preferences.edit();

        preferencesConfrm = context.getSharedPreferences(GlobalDataCls.confrmPreferences,
                Context.MODE_PRIVATE);
        editorConfrm = preferencesConfrm.edit();
    }


    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = inflater.inflate(R.layout.item_product_size_layout, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    private RadioButton lastCheckedRB = null;
    @Override
    public void onBindViewHolder(@NonNull final RecyclerViewHolder holder, final int position) {

        holder.radioButton.setText(list.getData().getSize().get(position).getSize());

        Log.d("sku_id_by_store_de", preferences.getString(GlobalDataCls.Sku_ID, null));
        String sku = String.valueOf(list.getData().getSize().get(position).getSkuId());
        if (sku.equals(preferences.getString(GlobalDataCls.Sku_ID, null))){

            holder.radioButton.setChecked(true);
            lastCheckedRB = holder.radioButton;
            editor.putString(GlobalDataCls.SizeOfItem, holder.radioButton.getText().toString());
            editor.putString(GlobalDataCls.Sku_ID, list.getData().getSize().get(position).getSkuId().toString());
            editor.commit();

            editorConfrm.putString(GlobalDataCls.confrmSize, holder.radioButton.getText().toString());
            editorConfrm.putString(GlobalDataCls.Sku_ID, list.getData().getSize().get(position).getSkuId().toString());
            editorConfrm.commit();

            setPriceAndDscnt(position);
        }

        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (lastCheckedRB != null) {
                    if (lastCheckedRB == (RadioButton) v) {

                    } else {
                        lastCheckedRB.setChecked(false);
                    }
                }
                lastCheckedRB = (RadioButton) v;
                editor.putString(GlobalDataCls.SizeOfItem, holder.radioButton.getText().toString());
                editor.putString(GlobalDataCls.Sku_ID, list.getData().getSize().get(position).getSkuId().toString());
                editor.commit();

                editorConfrm.putString(GlobalDataCls.confrmSize, holder.radioButton.getText().toString());
                editorConfrm.putString(GlobalDataCls.Sku_ID, list.getData().getSize().get(position).getSkuId().toString());
                editorConfrm.commit();
                setPriceAndDscnt(position);
            }
        });
    }

    private void setPriceAndDscnt(int position) {

        FragProductDetailByStore.price.setText(list.getData().getSize().get(position).getInvPrice());

        int priceNew = Integer.parseInt(list.getData().getSize().get(position).getInvPrice());
        int priceOld = list.getData().getSize().get(position).getPrice();
        String discPrice = String.valueOf(100 - Math.round(priceNew * 100 / priceOld));

        if (discPrice.equals("0")){
            FragProductDetailByStore.discount.setVisibility(View.GONE);
            FragProductDetailByStore.oldPrice.setVisibility(View.GONE);
            FragProductDetailByStore.imgPriceOld.setVisibility(View.GONE);
        }else {
            String oldP = "<del>" + String.valueOf(list.getData().getSize().get(position).getPrice()) + "</del>";
            FragProductDetailByStore.oldPrice.setText(Html.fromHtml(oldP));
            String discnt = "(<font color=#F4684E>" + discPrice + "%Off</font>)";
            FragProductDetailByStore.discount.setText(Html.fromHtml(discnt));
            FragProductDetailByStore.discount.setVisibility(View.VISIBLE);
            FragProductDetailByStore.oldPrice.setVisibility(View.VISIBLE);
            FragProductDetailByStore.imgPriceOld.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return list.getData().getSize().size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        RadioGroup radioGroup;
        RadioButton radioButton;
        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);

            //          radioGroup = (RadioGroup) itemView.findViewById(R.id.radio_grp);
            radioButton = (RadioButton) itemView.findViewById(R.id.radio_btn);
        }
    }
}
