package com.hoplett.app.adapters.byStore;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hoplett.app.R;
import com.hoplett.app.activities.ByStoreActivity;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.pojoClasses.byStoreShopping.expVw.shopList.ShopListData;
import com.hoplett.app.pojoClasses.byStoreShopping.expVw.storeList.StoreListData;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

public class AdaptrCatByStore extends BaseExpandableListAdapter {

    private final SharedPreferences preferencesConfrm;
    private final SharedPreferences.Editor editorConfrm;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    Context context;
    List<StoreListData> listStoreName;
    HashMap<String, List<ShopListData>> listShopName;

    public AdaptrCatByStore(Context context, List<StoreListData> listStoreName, HashMap<String, List<ShopListData>> listShopName) {

        this.context = context;
        this.listShopName = listShopName;
        this.listStoreName = listStoreName;

        preferences = context.getSharedPreferences(GlobalDataCls.PreferencesName, Context.MODE_PRIVATE);
        editor = preferences.edit();

        preferencesConfrm = context.getSharedPreferences(GlobalDataCls.confrmPreferences, Context.MODE_PRIVATE);
        editorConfrm = preferencesConfrm.edit();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listShopName.get(listStoreName.get(groupPosition)).get(childPosition).getAddress();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.item_cat_store_name_by_store_layout, null);
        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.img_vw);
        TextView address = (TextView) convertView.findViewById(R.id.item_address_by_store);
        TextView distance = (TextView) convertView.findViewById(R.id.item_distance_by_store);
        TextView totalProducts = (TextView) convertView.findViewById(R.id.total_products);
        TextView dividerBottom = (TextView) convertView.findViewById(R.id.divider_bottom);

        LinearLayout storeLayout = (LinearLayout) convertView.findViewById(R.id.store_name_layout);

        String imgStr = String.valueOf(listShopName.get(listStoreName.get(groupPosition).getStoreId().toString()).get(childPosition).getImage());
        if (imgStr != null && imgStr != "" && imgStr != "null") {

            String img = imgStr.substring(0, imgStr.length() - 4) + "raw=1";
            Picasso.with(context).load(img).fit().centerCrop().placeholder(R.drawable.loading_default_image).error(R.drawable.default_img_back).into(imageView);
        } else {
            imageView.setImageResource(R.drawable.default_img_back);
        }

        Bitmap mbitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        Bitmap imageRounded = Bitmap.createBitmap(mbitmap.getWidth(), mbitmap.getHeight(), mbitmap.getConfig());
        Canvas canvas = new Canvas(imageRounded);
        Paint mpaint = new Paint();
        mpaint.setAntiAlias(true);
        mpaint.setShader(new BitmapShader(mbitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
        canvas.drawRoundRect((new RectF(0, 0, mbitmap.getWidth(), mbitmap.getHeight())), 40, 40,
                mpaint);// Round Image Corner 100 100 100 100
        imageView.setImageBitmap(imageRounded);

        address.setText(listShopName.get(listStoreName.get(groupPosition).getStoreId().toString()).get(childPosition).getAddress());

        DecimalFormat df = new DecimalFormat("####.##");
        distance.setText(String.valueOf(df.format(listShopName.get(listStoreName.get(groupPosition).getStoreId().toString()).get(childPosition).getDistance())) + " Km");

        totalProducts.setText(listShopName.get(listStoreName.get(groupPosition).getStoreId().toString()).get(childPosition).getProductCount().toString() + " products");

        editorConfrm.putString(GlobalDataCls.confrmAddress, listShopName.get(listStoreName.get(groupPosition).getStoreId().toString()).get(childPosition).getAddress());
        editorConfrm.putString(GlobalDataCls.confrmDistance, String.valueOf(df.format(listShopName.get(listStoreName.get(groupPosition).getStoreId().toString()).get(childPosition).getDistance())) + " Km");
        editorConfrm.commit();


        storeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (listShopName.get(listStoreName.get(groupPosition).getStoreId().toString()).get(childPosition).getProductCount() > 0) {

                    editor.putString(GlobalDataCls.FragmentName, "FragProductList");

                    String noOfProducts = listShopName.get(listStoreName.get(groupPosition).getStoreId().toString()).get(childPosition).getProductCount().toString();

                    String brandName = listStoreName.get(groupPosition).getName();

                    editor.putString(GlobalDataCls.BrandName, brandName);
                    editor.putString(GlobalDataCls.NoOfProducts, noOfProducts);

                    editor.putString(GlobalDataCls.Shop_ID, listShopName.get(listStoreName.get(groupPosition).getStoreId().toString()).get(childPosition).getShopId().toString());
             //       editor.putString(GlobalDataCls.Sku_ID, listShopName.get(listStoreName.get(groupPosition).getStoreId().toString()).get(childPosition).getSkuId());
                    editor.commit();

                    Intent intent = new Intent(context, ByStoreActivity.class);
                    context.startActivity(intent);
                }
            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        if (listShopName.get(listStoreName.get(groupPosition).getStoreId().toString()) != null) {

            int sSize = listShopName.get(listStoreName.get(groupPosition).getStoreId().toString()).size();

            if (sSize > 0)
                return sSize;
            return 0;
        }

        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listStoreName.get(groupPosition).getName();
    }

    @Override
    public int getGroupCount() {
        return listStoreName.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_cat_store_type_by_store_layout, null);
        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.img_vw);
        TextView brandName = (TextView) convertView.findViewById(R.id.brand_name);
        TextView totalStores = (TextView) convertView.findViewById(R.id.total_stores);
        TextView bottomDivider = (TextView) convertView.findViewById(R.id.divider_bottom_store_type);
        ImageView upDownArrow = (ImageView) convertView.findViewById(R.id.up_down_arrow);

        /*imageView.setBackgroundResource(listStoreName.get(groupPosition).getImg());*/
        brandName.setText(listStoreName.get(groupPosition).getName());

        if (listStoreName.get(groupPosition).getShopCount() > 1) {
            totalStores.setText(listStoreName.get(groupPosition).getShopCount().toString() + " Stores");
        } else {
            totalStores.setText(listStoreName.get(groupPosition).getShopCount().toString() + " Store");
        }

        if (isExpanded) {
            totalStores.setTextColor(0x99000000);
            bottomDivider.setVisibility(View.VISIBLE);
            upDownArrow.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
        } else {
            totalStores.setTextColor(Color.parseColor("#FF4949"));
            bottomDivider.setVisibility(View.GONE);
            upDownArrow.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
        }

        String imgStr = String.valueOf(listStoreName.get(groupPosition).getImage());
        if (imgStr != null && imgStr != "" && imgStr != "null") {

            String img = imgStr.substring(0, imgStr.length() - 4) + "raw=1";
            Picasso.with(context).load(img).fit().centerCrop().placeholder(R.drawable.loading_default_image).error(R.drawable.default_img_back).into(imageView);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
