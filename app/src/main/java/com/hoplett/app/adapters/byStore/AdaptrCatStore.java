package com.hoplett.app.adapters.byStore;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hoplett.app.R;
import com.hoplett.app.activities.ByStoreActivity;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.pojoClasses.storeList.StoreList;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

import static android.content.Context.LOCATION_SERVICE;

//
// Created by Sandy on 29-May-19 at 2:50 PM.
//
public class AdaptrCatStore extends RecyclerView.Adapter<AdaptrCatStore.RecyclerVwHolder> {

    private final SharedPreferences preferences;
    private final SharedPreferences.Editor editor;
    Context context;
    StoreList listProducts;
//    ArrayList<Boolean> listBool;
    LayoutInflater inflater;

    public AdaptrCatStore(Context context, StoreList listProducts/*, ArrayList<Boolean> listBool*/) {

        this.context = context;
        this.listProducts = listProducts;
        /*this.listBool = listBool;*/
        inflater = LayoutInflater.from(context);
        preferences = context.getSharedPreferences(GlobalDataCls.PreferencesName, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    @NonNull
    @Override
    public RecyclerVwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = inflater.inflate(R.layout.item_store_category, parent, false);
        return new RecyclerVwHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerVwHolder holder, final int position) {

        String name = listProducts.getData().get(position).getShopName();
        holder.brandName.setText(name);
        holder.address.setText(listProducts.getData().get(position).getAddress());

        holder.distance.setText(String.valueOf(new DecimalFormat("###.##").format(listProducts.getData().get(position).getDistance()))+" km away");
 //       holder.distance.setText(String.valueOf(listProducts.getData().get(position).getDistance()));

        holder.totalProducts.setText(String.valueOf(listProducts.getData().get(position).getProductCount())+" Products");

        if (listProducts.getData().get(position).getImage() != null) {
            Picasso.with(context).load(listProducts.getData().get(position).getImage()).placeholder(R.drawable.loading_default_image).error(R.drawable.default_img_back).fit().into(holder.imageView);
        }else {holder.imageView.setImageResource(R.drawable.loading_default_image);}


        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationManager service = (LocationManager) context.getSystemService(LOCATION_SERVICE);
                boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

                // Check if enabled and if not send user to the GPS settings
                if (!enabled) {
			/*Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);*/
                    Toast.makeText(context, "Enable Your GPS Location", Toast.LENGTH_SHORT).show();
                }else {
                    if (listProducts.getData().get(position).getProductCount() > 0) {
                        editor.putString(GlobalDataCls.FragmentName, "FragProductList");

                        String noOfProducts = String.valueOf(listProducts.getData().get(position).getProductCount());

                        String brandName = listProducts.getData().get(position).getShopName();

                        editor.putString(GlobalDataCls.BrandName, brandName);
                        editor.putString(GlobalDataCls.NoOfProducts, noOfProducts);

                        editor.putString(GlobalDataCls.Shop_ID, String.valueOf(listProducts.getData().get(position).getShopId()));
                        //       editor.putString(GlobalDataCls.Sku_ID, listShopName.get(listStoreName.get(groupPosition).getStoreId().toString()).get(childPosition).getSkuId());
                        editor.commit();

                        Intent intent = new Intent(context, ByStoreActivity.class);
                        context.startActivity(intent);
                    } else {
                        Toast.makeText(context, "Product not available for this shop.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listProducts.getData().size();
    }

    public class RecyclerVwHolder extends RecyclerView.ViewHolder {

        TextView brandName, address, distance, totalProducts;
        ImageView imageView;
        LinearLayout layout;
        public RecyclerVwHolder(@NonNull View itemView) {
            super(itemView);

            brandName = (TextView) itemView.findViewById(R.id.brand_name);
            address = (TextView) itemView.findViewById(R.id.item_address_by_store);
            distance = (TextView) itemView.findViewById(R.id.item_distance_by_store);
            totalProducts = (TextView) itemView.findViewById(R.id.total_products);
            imageView = (ImageView) itemView.findViewById(R.id.img_vw);
            layout = (LinearLayout) itemView.findViewById(R.id.by_store_layout);
        }
    }
}
