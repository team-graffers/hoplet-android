package com.hoplett.app.adapters.byStore;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.hoplett.app.R;
import com.hoplett.app.activities.ByStoreActivity;
import com.hoplett.app.activities.HopletHome;
import com.hoplett.app.activities.SignInActivity;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.fragments.bystore.FragProductList;
import com.hoplett.app.pojoClasses.byProductShopping.productList.ResultList;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AdaptrProductByStore extends RecyclerView.Adapter<AdaptrProductByStore.RecyclerViewHolder> {

    Context context;
    ArrayList<Boolean> listBool;
    LayoutInflater inflater;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    List<ResultList> listProducts;

    public AdaptrProductByStore(Context context, List<ResultList> listProducts, ArrayList<Boolean> listBool) {

        this.context = context;
        this.listProducts = listProducts;
        this.listBool = listBool;
        inflater = LayoutInflater.from(context);

        preferences = context.getSharedPreferences(GlobalDataCls.PreferencesName, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View view = inflater.inflate(R.layout.item_cat_by_product_layout, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerViewHolder holder, final int position) {

        if (listBool.get(position)){
            holder.imgFavorite.setBackgroundResource(R.drawable.heart_product_selected);
        }else {holder.imgFavorite.setBackgroundResource(R.drawable.heart_product_not_selected);}

        holder.imgProduct.setBackgroundResource(R.drawable.default_img_back);

        holder.nameBrand.setText(listProducts.get(position).getBrand());
        holder.nameProduct.setText(listProducts.get(position).getProduct());

        if (String.valueOf(listProducts.get(position).getPrice()).equals(listProducts.get(position).getDiscPrice())){

            holder.imgPriceSign.setVisibility(View.GONE);
            holder.priceOld.setVisibility(View.GONE);
            holder.discount.setVisibility(View.GONE);
        }else {

            holder.imgPriceSign.setVisibility(View.VISIBLE);
            holder.priceOld.setVisibility(View.VISIBLE);
            holder.discount.setVisibility(View.VISIBLE);
            String price = "<del>" + listProducts.get(position).getPrice() + "</del>";

            String discPri = listProducts.get(position).getDiscPrice();
            int dscntPrice = Integer.parseInt(discPri);
            int originalPrice = listProducts.get(position).getPrice();

            String dscntPercentage = String.valueOf(100 - Math.round((dscntPrice * 100) / originalPrice));

            String discnt = " (" + "<font color=#f4684e>" + dscntPercentage + "% Off</font>" + ")";

            holder.priceOld.setText(Html.fromHtml(price));
            holder.discount.setText(Html.fromHtml(discnt));
        }

        holder.priceProduct.setText(listProducts.get(position).getDiscPrice().toString());

        holder.availableStores.setVisibility(View.GONE);

        String imgUrl = listProducts.get(position).getImage();

        String imgStr = listProducts.get(position).getImage();
        if (imgStr!= null && imgStr != "" && imgStr != "null" && imgStr.length() > 0){

            String img = imgStr.substring(0, imgStr.length()-4)+"raw=1";
            Picasso.with(context).load(img).fit().centerCrop().placeholder(R.drawable.loading_default_image).error(R.drawable.default_img_back).into(holder.imgProduct);
        }else {holder.imgProduct.setImageResource(R.drawable.default_img_back);}
		/*if (listProducts.get(position).getImage() != null && listProducts.get(position).getImage() != "null" && listProducts.get(position).getImage().length() > 0){

	//		Picasso.with(context).load(imgUrl).centerCrop().into(holder.imgProduct);
            Picasso.with(context).load(imgUrl).fit().centerCrop().placeholder(R.drawable.default_img_back).error(R.drawable.default_img_back).into(holder.imgProduct);
		}*/

        holder.imgFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (preferences.getString(GlobalDataCls.toKen, null) != null) {

                    if (!listBool.get(position)) {
                        addToFavourite(holder.imgFavorite, listProducts.get(position).getProductId().toString(), position, listProducts.get(position).getSkuId());
                    } else {
                        removeFromFavourite(holder.imgFavorite, listProducts.get(position).getProductId().toString(), position, listProducts.get(position).getSkuId());
                    }
                } else {

                    editor.putString(GlobalDataCls.SignInActivityName, "HopletHome");
                    editor.putString(GlobalDataCls.SignInFragName, "FragCategoryByProduct");
                    editor.commit();
                    Intent intent = new Intent(context, SignInActivity.class);
                    context.startActivity(intent);
                }

                /*if (listBool.get(position)) {

                    GlobalDataCls.FavoriteCounter++;
                    HopletHome.favoriteCount.setText(String.valueOf(GlobalDataCls.FavoriteCounter));
                    holder.imgFavorite.setBackgroundResource(R.drawable.heart_product_selected);
                    listBool.set(position, false);
                } else if (GlobalDataCls.FavoriteCounter != 0) {

                    GlobalDataCls.FavoriteCounter--;
                    HopletHome.favoriteCount.setText(String.valueOf(GlobalDataCls.FavoriteCounter));

                    if (GlobalDataCls.FavoriteCounter == 0) {
                        HopletHome.favoriteCount.setText(String.valueOf(""));
                    }
                } else {
                    HopletHome.favoriteCount.setText(String.valueOf(""));
                }
                holder.imgFavorite.setBackgroundResource(R.drawable.heart_product_not_selected);*/
            }
        });

        holder.imgProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putString(GlobalDataCls.ProductId, listProducts.get(position).getProductId().toString());
                editor.putString(GlobalDataCls.PriceNew, listProducts.get(position).getPrice().toString());
                editor.putString(GlobalDataCls.PriceOld, String.valueOf(listProducts.get(position).getPrice() + 100));
                editor.putString(GlobalDataCls.Sku_ID, String.valueOf(listProducts.get(position).getSkuId()));
                editor.putString(GlobalDataCls.ProductImg, listProducts.get(position).getImage());
                editor.commit();

                Log.d("sku_id_by_store", preferences.getString(GlobalDataCls.Sku_ID, null));

                ByStoreActivity activity = (ByStoreActivity) context;
                activity.startProductDetailFragment();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listProducts.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        ImageView imgProduct;
        ImageView imgFavorite, imgPriceSign;
        TextView nameBrand, nameProduct, priceProduct, availableStores, priceOld, discount;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);

            imgProduct = (ImageView) itemView.findViewById(R.id.item_by_product_img_product);
            imgFavorite = (ImageView) itemView.findViewById(R.id.item_by_product_img_favorite);

            nameBrand = (TextView) itemView.findViewById(R.id.item_by_product_brand_name);
            nameProduct = (TextView) itemView.findViewById(R.id.item_by_product_name_product);
            priceProduct = (TextView) itemView.findViewById(R.id.item_by_product_price_product);
            availableStores = (TextView) itemView.findViewById(R.id.item_by_product_avail_stores);
            priceOld = (TextView) itemView.findViewById(R.id.item_by_product_price_product_old);
            discount = (TextView) itemView.findViewById(R.id.item_by_product_discount);
            imgPriceSign = (ImageView) itemView.findViewById(R.id.price_img);
        }
    }

    /**
     *  Add item To Wish list
     * **/
    private void addToFavourite(final ImageView imgFavorite, String pro_id, final int position, Integer skuId) {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        String url = UrlAll.WishlistAddUrl+pro_id+"&sku_idd="+skuId;
        HashMap headers = new HashMap();
        headers.put("Authorization", "Token " + preferences.getString(GlobalDataCls.toKen, null));

        ApiCallMethods apiCallMethods = new ApiCallMethods(){
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("response_fav_add", response.toString());

                        imgFavorite.setBackgroundResource(R.drawable.heart_product_selected);
                        GlobalDataCls.FavoriteCounter++;
                        HopletHome.favoriteCount.setText(String.valueOf(GlobalDataCls.FavoriteCounter));

                        listBool.set(position, true);
                        notifyDataSetChanged();
                        dialog.dismiss();

                        try {
                            if (response.getInt("count")>0) {
                                GlobalDataCls.FavoriteCounter = response.getInt("count");
                                FragProductList.favoriteCount.setText(String.valueOf(GlobalDataCls.FavoriteCounter));
                            }else {FragProductList.favoriteCount.setText("");}
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(context, url, headers, dialog);
    }

    /**
     *  Remove item From Wish list
     * **/
    private void removeFromFavourite(final ImageView imgFavorite, String pro_id, final int position, Integer skuId) {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        String url = UrlAll.WishlistRemoveUrl+pro_id+"&sku_idd="+skuId;
        HashMap headers = new HashMap();
        headers.put("Authorization", "Token " + preferences.getString(GlobalDataCls.toKen, null));

        ApiCallMethods apiCallMethods = new ApiCallMethods(){
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("response_fav_remove", response.toString());

                        imgFavorite.setBackgroundResource(R.drawable.heart_product_not_selected);

                        if (GlobalDataCls.FavoriteCounter != 0){

                            GlobalDataCls.FavoriteCounter--;
                            HopletHome.favoriteCount.setText(String.valueOf(GlobalDataCls.FavoriteCounter));

                            if (GlobalDataCls.FavoriteCounter == 0){
                                HopletHome.favoriteCount.setText(String.valueOf(""));
                            }
                        }else {
                            HopletHome.favoriteCount.setText(String.valueOf(""));
                        }
                        listBool.set(position, false);
                        notifyDataSetChanged();
                        dialog.dismiss();

                        try {
                            if (response.getInt("count")>0) {
                                GlobalDataCls.FavoriteCounter = response.getInt("count");
                                FragProductList.favoriteCount.setText(String.valueOf(GlobalDataCls.FavoriteCounter));
                            }else {FragProductList.favoriteCount.setText("");}
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(context, url, headers, dialog);
    }
}
