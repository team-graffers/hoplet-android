package com.hoplett.app.adapters.byStore;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.hoplett.app.R;
import com.hoplett.app.classes.GlobalDataCls;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AdaptrResrvToday extends RecyclerView.Adapter<AdaptrResrvToday.RecyclerViewHolder> {

    Context context;
    ArrayList<String> list;
    LayoutInflater inflater;
    ArrayList<Boolean> listBool;
    SharedPreferences preferences, preferencesCnfm;
    SharedPreferences.Editor editor, editorCnfm;

    public AdaptrResrvToday(Context context, ArrayList<String> list, ArrayList<Boolean> listBool) {
        this.context = context;
        this.list = list;
        this.listBool = listBool;
        inflater = LayoutInflater.from(context);

        preferences = context.getSharedPreferences(GlobalDataCls.PreferencesName, Context.MODE_PRIVATE);
        editor = preferences.edit();
        preferencesCnfm = context.getSharedPreferences(GlobalDataCls.confrmPreferences, Context.MODE_PRIVATE);
        editorCnfm = preferencesCnfm.edit();
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View view = inflater.inflate(R.layout.item_reserve_layout, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    private RadioButton lastCheckedRB = null;

    @Override
    public void onBindViewHolder(@NonNull final RecyclerViewHolder holder, final int position) {

        holder.radioButton.setText(list.get(position));

        if (listBool.get(position)) {
            holder.radioButton.setChecked(false);
        } else {
            holder.radioButton.setChecked(true);
            lastCheckedRB = holder.radioButton;
            timeFormatMethod(holder.radioButton.getText().toString());
        }

        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (lastCheckedRB != null) {
                    if (lastCheckedRB == (RadioButton) v) {
                    } else {
                        lastCheckedRB.setChecked(false);
                    }
                }
                lastCheckedRB = (RadioButton) v;

                timeFormatMethod(holder.radioButton.getText().toString());
            }
        });
    }

    private void timeFormatMethod(String s) {

        String[] time = s.split("-");

        SimpleDateFormat dateFormatOld = new SimpleDateFormat("hh:mm aa");
        Date timeFrom = null;
        Date timeTo = null;
        try {
            timeFrom = dateFormatOld.parse(time[0]);
            timeTo = dateFormatOld.parse(time[1]);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat dateFormatRT = new SimpleDateFormat("HH:mm:ss");
        String newTimeFrom = dateFormatRT.format(timeFrom);
        String newTimeTo = dateFormatRT.format(timeTo);

        SimpleDateFormat visitDateFormat = new SimpleDateFormat("hh aa");

        String visitTime = visitDateFormat.format(timeFrom);

//					Toast.makeText(context, newTimeFrom+"\n"+newTimeTo, Toast.LENGTH_SHORT).show();

        editor.putString(GlobalDataCls.confrmVisitingTime, visitTime);
        editor.putString(GlobalDataCls.TimeReserveFrom, newTimeFrom);
        editor.putString(GlobalDataCls.TimeReserveTo, newTimeTo);
        editor.commit();
        editorCnfm.putString(GlobalDataCls.confrmVisitingTime, visitTime);
        editorCnfm.putString(GlobalDataCls.TimeReserveFrom, newTimeFrom);
        editorCnfm.putString(GlobalDataCls.TimeReserveTo, newTimeTo);
        editorCnfm.commit();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        //			RadioGroup radioGroup;
        RadioButton radioButton;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);

            //		radioGroup = (RadioGroup) itemView.findViewById(R.id.radio_group);
            radioButton = (RadioButton) itemView.findViewById(R.id.radio_btn);
        }
    }
}
