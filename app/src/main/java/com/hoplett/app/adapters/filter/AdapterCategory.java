package com.hoplett.app.adapters.filter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hoplett.app.R;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.fragments.byproduct.FragFilter;
import com.hoplett.app.pojoClasses.filter.catName.CatItems;
import com.hoplett.app.pojoClasses.filter.catName.CatNames;
import com.hoplett.app.pojoClasses.filter.catName.CatSelectedItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sandy on 28-03-2019 at 05:04 PM.
 */
public class AdapterCategory extends BaseExpandableListAdapter {

	SharedPreferences preferences;
	SharedPreferences.Editor editor;

	Context context;
	ArrayList<CatNames> listCatNames;
	HashMap<String, List<CatItems>> listCatItems;
	ArrayList<CatSelectedItem> boolList;


	public AdapterCategory(Context context, ArrayList<CatNames> listCatNames, HashMap<String, List<CatItems>> listCatItems, ArrayList<CatSelectedItem> boolList) {

		this.context = context;
		this.listCatNames = listCatNames;
		this.listCatItems = listCatItems;
		this.boolList = boolList;

		preferences = context.getSharedPreferences(GlobalDataCls.PreferencesName, Context.MODE_PRIVATE);
		editor = preferences.edit();
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return listCatItems.get(listCatNames.get(groupPosition).getId()).get(childPosition).getName();
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

		if (convertView == null){

			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.item_filter_color_brand_size, null);
		}
		TextView nameTxtVw;
		final CheckBox checkBoxVw;
		LinearLayout colorLay, nameLay;

		nameLay = (LinearLayout) convertView.findViewById(R.id.txt_name_vw_lay);
		colorLay = (LinearLayout) convertView.findViewById(R.id.color_vw_layout);
		nameTxtVw = (TextView) convertView.findViewById(R.id.txt_name_vw);
		checkBoxVw = (CheckBox) convertView.findViewById(R.id.checkbox_vw);

		colorLay.setVisibility(View.GONE);
		LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 2.5f);
		nameLay.setLayoutParams(param);


		nameTxtVw.setText(listCatItems.get(listCatNames.get(groupPosition).getId()).get(childPosition).getName());

		checkBoxVw.setChecked(false);

		for (int i=0; i < boolList.size(); i++){
			if (boolList.get(i).getId().equals(listCatItems.get(listCatNames.get(groupPosition).getId()).get(childPosition).getId())){
				if (boolList.get(i).isBool()){
					checkBoxVw.setChecked(false);
				}else {
					checkBoxVw.setChecked(true);
				}
				break;
			}
		}


		checkBoxVw.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (checkBoxVw.isChecked()){

					for (int i=0; i < boolList.size(); i++){
						if (boolList.get(i).getId().equals(listCatItems.get(listCatNames.get(groupPosition).getId()).get(childPosition).getId())){

							CatSelectedItem item = new CatSelectedItem();
							item.setId(boolList.get(i).getId());
							item.setBool(false);

							boolList.set(i, item);

							GlobalDataCls.boolListCategory.set(i, item);
							GlobalDataCls.listCategory.add(String.valueOf(listCatItems.get(listCatNames.get(groupPosition).getId()).get(childPosition).getId()));
							break;
						}
					}
				}else {

					for (int i=0; i < boolList.size(); i++){
						if (boolList.get(i).getId().equals(listCatItems.get(listCatNames.get(groupPosition).getId()).get(childPosition).getId())){

							CatSelectedItem item = new CatSelectedItem();
							item.setId(boolList.get(i).getId());
							item.setBool(true);

							boolList.set(i, item);

							GlobalDataCls.boolListCategory.set(i, item);
							GlobalDataCls.listCategory.add(String.valueOf(listCatItems.get(listCatNames.get(groupPosition).getId()).get(childPosition).getId()));
							break;
						}
					}
					removeItemFromColorList(listCatItems.get(listCatNames.get(groupPosition).getId()).get(childPosition).getId());
				}

				for (int i=0; i<GlobalDataCls.listCategory.size(); i++){
					Log.d("categoryListItem", "Add : "+GlobalDataCls.listCategory.get(i));
				}

				GlobalDataCls.boolFilterCategory = true;
				new FragFilter().callAllApis(3);
			}
		});

		return convertView;
	}

	private void removeItemFromColorList(String id) {

		if (GlobalDataCls.listCategory.size() > 0){
			for (int i=0; i<GlobalDataCls.listCategory.size(); i++){
				if (id.equals(GlobalDataCls.listCategory.get(i))){

					GlobalDataCls.listCategory.remove(i);
					break;
				}
			}
		}
	}



	@Override
	public int getChildrenCount(int groupPosition) {
		return listCatItems.get(listCatNames.get(groupPosition).getId()).size();
	}

	@Override
	public int getGroupCount() {
		return listCatNames.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return listCatNames.get(groupPosition).getName();
	}



	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}





	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.item_category_name, null);
		}

		ImageView imageView = (ImageView) convertView.findViewById(R.id.up_down_arrow);
		TextView catName = (TextView) convertView.findViewById(R.id.cat_names);

		catName.setText(listCatNames.get(groupPosition).getName());
		catName.setTextSize(14);

//		imageView.requestLayout();
		int imgVwSize = 50;
		imageView.getLayoutParams().height = imgVwSize;
		imageView.getLayoutParams().width = imgVwSize;

		if (isExpanded){

			imageView.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
		}else {

			imageView.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
		}

		return convertView;
	}



	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}
