package com.hoplett.app.adapters.filter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hoplett.app.R;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.fragments.byproduct.FragFilter;
import com.hoplett.app.pojoClasses.filter.color.Datum;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandy on 28-03-2019 at 05:03 PM.
 */
public class AdaptrBrand extends RecyclerView.Adapter<AdaptrBrand.RecyclerVwHolder> {

	Context context;
	List<Datum> list;
	ArrayList<Boolean> boolList;
	LayoutInflater inflater;

	public AdaptrBrand(Context context, List<Datum> list, ArrayList<Boolean> boolList) {

		this.context = context;
		this.list = list;
		this.boolList = boolList;
		inflater = LayoutInflater.from(context);
	}

	@NonNull
	@Override
	public RecyclerVwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
		View view = inflater.inflate(R.layout.item_filter_color_brand_size, parent,false);
		RecyclerVwHolder vwHolder = new RecyclerVwHolder(view);
		return vwHolder;
	}

	@Override
	public void onBindViewHolder(@NonNull final RecyclerVwHolder holder, final int position) {

		holder.nameTxtVw.setText(list.get(position).getName());

		if (boolList.get(position)){
			holder.checkBoxVw.setChecked(false);
		}else {
			holder.checkBoxVw.setChecked(true);
		}

		holder.checkBoxVw.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (holder.checkBoxVw.isChecked()){

					boolList.set(position, false);
					GlobalDataCls.boolListBrand.set(position, false);
					GlobalDataCls.listBrand.add(String.valueOf(list.get(position).getId()));
					for (int i=0; i<GlobalDataCls.listBrand.size(); i++){
						Log.d("colorListItem", "Add : "+GlobalDataCls.listBrand.get(i));
					}
				}else {

					boolList.set(position, true);
					GlobalDataCls.boolListBrand.set(position, true);
					removeItemFromColorList(list.get(position).getId());
				}
				GlobalDataCls.boolFilterBrand = true;
				new FragFilter().callAllApis(2);
			}
		});
	}

	private void removeItemFromColorList(Integer id) {

		if (GlobalDataCls.listBrand.size() > 0){
			for (int i=0; i<GlobalDataCls.listBrand.size(); i++){
				if (String.valueOf(id).equals(GlobalDataCls.listBrand.get(i))){
					GlobalDataCls.listBrand.remove(i);
					break;
				}
			}

			for (int i=0; i<GlobalDataCls.listBrand.size(); i++){
				Log.d("colorListItem", "Remove : "+GlobalDataCls.listBrand.get(i));
			}
		}
	}

	@Override
	public int getItemCount() {
		return list.size();
	}

	public class RecyclerVwHolder extends RecyclerView.ViewHolder {

		TextView nameTxtVw;
		CheckBox checkBoxVw;
		LinearLayout colorLay, nameLay;

		public RecyclerVwHolder(@NonNull View itemView) {
			super(itemView);

			nameLay = (LinearLayout) itemView.findViewById(R.id.txt_name_vw_lay);
			colorLay = (LinearLayout) itemView.findViewById(R.id.color_vw_layout);
			nameTxtVw = (TextView) itemView.findViewById(R.id.txt_name_vw);
			checkBoxVw = (CheckBox) itemView.findViewById(R.id.checkbox_vw);

			colorLay.setVisibility(View.GONE);
			LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 2.5f);
			nameLay.setLayoutParams(param);

			/*int width = colorLay.getWidth();
			int height = colorLay.getHeight();*/
		}
	}
}
