package com.hoplett.app.adapters.filter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.hoplett.app.R;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.fragments.byproduct.FragFilter;
import com.hoplett.app.pojoClasses.filter.color.Datum;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandy on 28-03-2019 at 05:03 PM.
 */
public class AdaptrColor extends RecyclerView.Adapter<AdaptrColor.RecyclerVwHolder> {

	Context context;
	List<Datum> list;
	ArrayList<Boolean> boolList;
	LayoutInflater inflater;

	public AdaptrColor(Context context, List<Datum> list, ArrayList<Boolean> boolList) {
		this.context = context;
		this.list = list;
		this.boolList = boolList;
		inflater = LayoutInflater.from(context);
	}

	@NonNull
	@Override
	public RecyclerVwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

		View view = inflater.inflate(R.layout.item_filter_color_brand_size, parent,false);
		RecyclerVwHolder vwHolder = new RecyclerVwHolder(view);
		return vwHolder;
	}

	@Override
	public void onBindViewHolder(@NonNull final RecyclerVwHolder holder, final int position) {

		/*holder.colorTxtVw.setBackgroundColor(android.graphics.Color.parseColor(list.get(position).getName()));*/
		holder.nameTxtVw.setText(list.get(position).getName());

		if (list.get(position).getCode() != null) {
			holder.colorTxtVw.setBackgroundColor(android.graphics.Color.parseColor("#" + list.get(position).getCode()));
		}

		if (boolList.get(position)){
			holder.checkBoxVw.setChecked(false);
		}else {
			holder.checkBoxVw.setChecked(true);
		}

		/*holder.checkBoxVw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked){
					boolList.set(position, true);
					removeItemFromColorList(list.get(position).getId());
				}else {
					boolList.set(position, false);
					GlobalDataCls.listColor.add(String.valueOf(list.get(position).getId()));
					for (int i=0; i<GlobalDataCls.listColor.size(); i++){
						Log.d("colorListItem", GlobalDataCls.listColor.get(i));
					}
				}
			}
		});*/
		holder.checkBoxVw.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (holder.checkBoxVw.isChecked()){

					boolList.set(position, false);
					GlobalDataCls.boolListColor.set(position, false);
					GlobalDataCls.listColor.add(String.valueOf(list.get(position).getId()));
					for (int i=0; i<GlobalDataCls.listColor.size(); i++){
						Log.d("colorListItem", "Add : "+GlobalDataCls.listColor.get(i));
					}
				}else {

					boolList.set(position, true);
					GlobalDataCls.boolListColor.set(position, true);
					removeItemFromColorList(list.get(position).getId());
				}

				GlobalDataCls.boolFilterColor = true;
				new FragFilter().callAllApis(1);
			}
		});
	}

	private void removeItemFromColorList(Integer id) {

		if (GlobalDataCls.listColor.size() > 0){

			String allColor = "";
			String afterRemove = "";
			for (int i=0; i<GlobalDataCls.listColor.size(); i++){

				allColor = allColor+GlobalDataCls.listColor.get(i)+"  ";
				if (String.valueOf(id).equals(GlobalDataCls.listColor.get(i))){
					GlobalDataCls.listColor.remove(i);
				}
			}

			for (int i=0; i<GlobalDataCls.listColor.size(); i++){

				afterRemove = afterRemove+GlobalDataCls.listColor.get(i)+"  ";
			}

			Log.d("colorListItemRem", "All: "+allColor+"\nRemove : "+afterRemove);
		}
	}

	@Override
	public int getItemCount() {
		return list.size();
	}

	public class RecyclerVwHolder extends RecyclerView.ViewHolder {

		TextView colorTxtVw, nameTxtVw;
		CheckBox checkBoxVw;

		public RecyclerVwHolder(@NonNull View itemView) {
			super(itemView);

			colorTxtVw = (TextView) itemView.findViewById(R.id.color_vw_txt);
			nameTxtVw = (TextView) itemView.findViewById(R.id.txt_name_vw);
			checkBoxVw = (CheckBox) itemView.findViewById(R.id.checkbox_vw);
		}
	}
}
