package com.hoplett.app.adapters.leftPanel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hoplett.app.R;
import com.hoplett.app.activities.VisitDetailActivity;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.pojoClasses.leftPanel.upcomingVisits.ResultUpcomingPastVisit;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AdaptrPastOrders extends RecyclerView.Adapter<AdaptrPastOrders.RecyclerViewHolder> implements Filterable {

	Context context;
	List<ResultUpcomingPastVisit> list;
	 List<ResultUpcomingPastVisit> listFiltered;
	LayoutInflater inflater;

	public AdaptrPastOrders(Context context, List<ResultUpcomingPastVisit> list) {
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(context);
	}

	@NonNull
	@Override
	public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
		View view = inflater.inflate(R.layout.item_upcoming_orders_layout, parent, false);
		RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
		return viewHolder;
	}

	@SuppressLint("ResourceAsColor")
	@Override
	public void onBindViewHolder(@NonNull RecyclerViewHolder holder, final int position) {

		 holder.storeName.setText(list.get(position).getShopName());
		 holder.address.setText(list.get(position).getShopAddress());
		 DecimalFormat df = new DecimalFormat("###.##");
		 holder.distance.setText(String.valueOf(df.format(list.get(position).getDistance()))+" Km");

		String[] strDate = list.get(position).getDate().split("T");

		Date date1 = null, date2 = null;
		try {
			date1=new SimpleDateFormat("yyyy-MM-dd").parse(strDate[0]);
			date2=new SimpleDateFormat("HH:mm:ss").parse(strDate[1]);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		SimpleDateFormat dateFormat;
		dateFormat = new SimpleDateFormat("MMM");
		String visitMonth = dateFormat.format(date1);
		dateFormat = new SimpleDateFormat("haa");
		String visitTime = dateFormat.format(date2);

		Calendar c = Calendar.getInstance();
		c.setTime(date1);
		int day = c.get(Calendar.DAY_OF_MONTH);
		Log.d("day", String.valueOf(day)+"  "+visitMonth+"  "+visitTime);

		String visitDay;
		if (day == 1 || day == 21 || day == 31) {
			visitDay = String.valueOf(day) + "st " + visitMonth + " at " + visitTime;
		}else if (day == 2 || day == 22) {
			visitDay =
				String.valueOf(day) + "nd " + visitMonth + " at " + visitTime;
		}else if (day == 3 || day == 23) {
			visitDay =
				String.valueOf(day) + "rd " + visitMonth + " at " + visitTime;
		}else {
			visitDay =
				String.valueOf(day) + "th " + visitMonth + " at " + visitTime;
		}

		holder.time.setText(visitDay);
		holder.time.setTextColor(android.graphics.Color.parseColor("#333333"));
		holder.calImg.setImageResource(R.drawable.calendar_black);

		String myString = list.get(position).getStatus();
		String upperString = myString.substring(0,1).toUpperCase() + myString.substring(1);
		holder.status.setText(upperString);

		String imgStr = list.get(position).getImage();
		if (imgStr != null && imgStr != "" && imgStr != "null") {

			String img = imgStr.substring(0, imgStr.length() - 4) + "raw=1";
			Picasso.with(context).load(img).fit().centerCrop().placeholder(R.drawable.loading_default_image).error(R.drawable.default_img_back).into(holder.imgProduct);
		}else {holder.imgProduct.setImageResource(R.drawable.default_img_back);}

		holder.upcmngLay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, VisitDetailActivity.class);
				GlobalDataCls.proId = list.get(position).getResId().toString();
				GlobalDataCls.visitType = "Past";
				context.startActivity(intent);
			}
		});
	}

	@Override
	public int getItemCount() {
		return list.size();
	}

	 @Override
	 public Filter getFilter() {
			return new Filter() {
				 @Override
				 protected FilterResults performFiltering(CharSequence charSequence) {

						String charString = charSequence.toString();

						if (charString.isEmpty()){
							 listFiltered = list;
						}else {
							 List<ResultUpcomingPastVisit> filteredList = new ArrayList<>();
							 for (ResultUpcomingPastVisit row : list){
									if (row.getShopName().toLowerCase().contains(charString.toLowerCase())){
										 filteredList.add(row);
									}
							 }
							 listFiltered = filteredList;
						}

						FilterResults filterResults = new FilterResults();
						filterResults.values = listFiltered;
						return filterResults;
				 }

				 @Override
				 protected void publishResults(CharSequence constraint, FilterResults results) {
						listFiltered = (ArrayList<ResultUpcomingPastVisit>) results.values;
						notifyDataSetChanged();
				 }
			};
	 }

	 public class RecyclerViewHolder extends RecyclerView.ViewHolder {

		TextView storeName, address, distance, time, status;
		ImageView imgProduct, calImg;
		 LinearLayout upcmngLay;

		public RecyclerViewHolder(@NonNull View itemView) {
			super(itemView);

			storeName = (TextView) itemView.findViewById(R.id.store_name_upcmng_order);
			address = (TextView) itemView.findViewById(R.id.address_upcmng_order);
			distance = (TextView) itemView.findViewById(R.id.distance_upcmng_order);
			time = (TextView) itemView.findViewById(R.id.time_upcmng_order);
			status = (TextView) itemView.findViewById(R.id.status_upcmng_order);
			imgProduct = (ImageView) itemView.findViewById(R.id.img_product_upcmng_order);
			calImg = (ImageView) itemView.findViewById(R.id.calendar_img);

			upcmngLay = (LinearLayout) itemView.findViewById(R.id.upcmng_layout);
		}
	}
}
