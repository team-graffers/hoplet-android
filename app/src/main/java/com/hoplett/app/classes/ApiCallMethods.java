package com.hoplett.app.classes;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Graffersid on 12-03-2019 at 12:47 PM.
 */
public class ApiCallMethods {

    Context context;
    Dialog dialog;

    public void volleyStringPOST(Context context, String url, JSONObject jsonObject,
                                 final HashMap headers, Dialog dialog) {

        this.context = context;
        this.dialog = dialog;

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        final String requestBody = jsonObject.toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, stringPostSucsListener(), stringPostErrListener()) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        requestQueue.add(stringRequest);
    }

    public Response.Listener<String> stringPostSucsListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        };
    }

    private Response.ErrorListener stringPostErrListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                volleyErrMethod(error);
            }
        };
    }

    public void volleyJSONObjectPOST(Context context, String url, JSONObject jsonObject, final HashMap headers, Dialog dialog) {

        this.context = context;
        this.dialog = dialog;

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url,
                jsonObject, jsonObjectPostReqSuccListener(), stringPostErrListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Log.d("headers", headers.toString());
                return headers;
            }
        };


        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest.setRetryPolicy(policy);
        // Add the request to the RequestQueue.
        requestQueue.add(jsonObjectRequest);
    }

    public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        };
    }


    public void volleyStringGET(Context context, String url, String headersStr, final HashMap headers, final Dialog dialog) {

        this.context = context;
        this.dialog = dialog;

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return headers;
            }
        };

        requestQueue.add(stringRequest);
    }

    public void volleyJSONObjectGET(Context context, String url, final HashMap headers, Dialog dialog) {

        this.context = context;
        this.dialog = dialog;

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                jsonObjectPostReqSuccListener(), stringPostErrListener()) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return headers;
            }
        };

        int socketTimeout = 16000;//16 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        // Add the request to the RequestQueue.
        requestQueue.add(request);
    }


    public void volleyErrMethod(VolleyError error) {

        Log.d("response_volleyErr", error.toString());

        if (error.networkResponse != null) {
            Log.d("response_volleyErr2", String.valueOf(error.networkResponse.statusCode));

            try {
                String res = new String(error.networkResponse.data, HttpHeaderParser.parseCharset(error.networkResponse.headers, "utf-8"));

                // Now you can use any deserializer to make sense of data
                JSONObject obj = new JSONObject(res);

                Log.d("response_volleyErr21", obj.toString());
            } catch (UnsupportedEncodingException e1) {
                // Couldn't properly decode data to string
                e1.printStackTrace();
            } catch (JSONException e2) {
                // returned data is not JSONObject?
                e2.printStackTrace();
            }
        } else {
//			 Toast.makeText(context, "Please check your Internet Connection..!!", Toast.LENGTH_SHORT).show();
            Log.d("response_volleyErr7", "Please check your Internet Connection..!!");
        }

        NetworkResponse response = error.networkResponse;
        if (error instanceof ServerError && response != null) {
            try {
                String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));

                // Now you can use any deserializer to make sense of data
                JSONObject obj = new JSONObject(res);

                Log.d("response_volleyErr3", obj.toString());
            } catch (UnsupportedEncodingException e1) {
                // Couldn't properly decode data to string
                e1.printStackTrace();
            } catch (JSONException e2) {
                // returned data is not JSONObject?
                e2.printStackTrace();
            }
        } else {
//			 Toast.makeText(context, "Check Your Internet Connection and try again", Toast.LENGTH_SHORT).show();
        }
    }


    public void stringRawPost(Context context, String url, final String deptId,
                              final HashMap headers, Dialog dialog, final String filterName,
                              final List<String> list, final List<String> list2, final List<String> list3) {

        this.context = context;
        this.dialog = dialog;

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, stringPostSucsListener(), stringPostErrListener()) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String requestBody = "";

                if (filterName.equals("color")) {
                    requestBody = "{\"department\":" + deptId + ",\"brands\":[" + rawStringMethod(list) +
                            "],\"categories\":[" + rawStringMethod(list2) + "],\"sizes\":[" + rawStringMethod(list3) +
                            "]}";
                } else if (filterName.equals("brand")) {
                    requestBody = "{\"department\":" + deptId + ",\"colors\":[" + rawStringMethod(list) +
                            "],\"categories\":[" + rawStringMethod(list2) + "],\"sizes\":[" + rawStringMethod(list3) +
                            "]}";
                } else if (filterName.equals("category")) {
                    requestBody = "{\"department\":" + deptId + ",\"colors\":[" + rawStringMethod(list) +
                            "],\"brands\":[" + rawStringMethod(list2) + "],\"sizes\":[" + rawStringMethod(list3) +
                            "]}";
                } else if (filterName.equals("size")) {
                    requestBody = "{\"department\":" + deptId + ",\"colors\":[" + rawStringMethod(list) +
                            "],\"brands\":[" + rawStringMethod(list2) + "],\"categories\":[" + rawStringMethod(list3) +
                            "]}";
                }

                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return requestBody.getBytes();
                }
            }

            /*@Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }*/
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        requestQueue.add(stringRequest);
    }

    private String rawStringMethod(List<String> list) {
        String str = "";
        boolean bool = true;
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                if (bool) {
                    bool = false;
                    str = str + list.get(i);
                } else {
                    str = str + "," + list.get(i);
                }
            }
        }

        return str;
    }

    public void jsonRawDataPost(Context context, String url, final String deptId,
                                final HashMap headers, Dialog dialog, final String filterName,
                                final List<String> list, final List<String> list2, final List<String> list3) {


        String requestBody = "";

        if (filterName.equals("color")) {
            requestBody = "{\"department\":" + deptId + ",\"brands\":[" + rawStringMethod(list) +
                    "],\"categories\":[" + rawStringMethod(list2) + "],\"sizes\":[" + rawStringMethod(list3) +
                    "]}";
        } else if (filterName.equals("brand")) {
            requestBody = "{\"department\":" + deptId + ",\"colors\":[" + rawStringMethod(list) +
                    "],\"categories\":[" + rawStringMethod(list2) + "],\"sizes\":[" + rawStringMethod(list3) +
                    "]}";
        } else if (filterName.equals("category")) {
            requestBody = "{\"department\":" + deptId + ",\"colors\":[" + rawStringMethod(list) +
                    "],\"brands\":[" + rawStringMethod(list2) + "],\"sizes\":[" + rawStringMethod(list3) +
                    "]}";
        } else if (filterName.equals("size")) {
            requestBody = "{\"department\":" + deptId + ",\"colors\":[" + rawStringMethod(list) +
                    "],\"brands\":[" + rawStringMethod(list2) + "],\"categories\":[" + rawStringMethod(list3) +
                    "]}";
        }

        Log.d("filterRawData", requestBody);

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(requestBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.context = context;
        this.dialog = dialog;

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url,
                jsonObject, jsonObjectPostReqSuccListener(), stringPostErrListener()) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Log.d("headers", headers.toString());
                return headers;
            }
        };


        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest.setRetryPolicy(policy);
        // Add the request to the RequestQueue.
        requestQueue.add(jsonObjectRequest);
    }
}


