package com.hoplett.app.classes;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.Response;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.pojoClasses.wishlist.ItemWishListPojo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

//
// Created by Sandy on 13-May-19 at 5:34 PM.
//
public class FavoriteItemsCls {

    private final SharedPreferences preferences;
    private final SharedPreferences.Editor editor;
    Context context;
    public FavoriteItemsCls(Context context){

        this.context = context;

        preferences = context.getSharedPreferences(GlobalDataCls.PreferencesName,
                Context.MODE_PRIVATE);
        editor = preferences.edit();

        favoriteListItemMethod();
    }

    private void favoriteListItemMethod() {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        String url = UrlAll.WishlistUrl;
        HashMap headers = new HashMap();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Token " + preferences.getString(GlobalDataCls.toKen, null));

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            if (response.get("data").toString() != "" && response.get("data").toString().length() > 0) {

                                ItemWishListPojo itemWishList = new Gson().fromJson(response.toString(), ItemWishListPojo.class);

                                if (itemWishList.getData() != null && itemWishList.getData().size() > 0 && !itemWishList.getData().isEmpty()) {

                                    GlobalDataCls.FavoriteCounter = itemWishList.getData().size();

                                    GlobalDataCls.favoriteItemList.clear();

                                    for (int i=0; i<itemWishList.getData().size(); i++) {
                                        GlobalDataCls.favoriteItemList.add(String.valueOf(itemWishList.getData().get(i).getSkuId()));
                                    }
                                }
                            }
                        } catch (JSONException e) {

                        }
                        dialog.dismiss();
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(context, url, headers, dialog);
    }
}
