package com.hoplett.app.classes;

import com.hoplett.app.pojoClasses.filter.catName.CatSelectedItem;
import org.json.JSONObject;

import java.util.ArrayList;

public class GlobalDataCls {

    /**
     *
     *   Profile Data22 Information
     * **/
    public static final String profileName = "nameProfile";
    public static final String profileImg = "imgProfile";


    public static boolean backBool = false;
    public static boolean proDetailBool = false;

    public static String urlProduct;


    public static final String PreferencesName = "Hoplet";

    public static final String SignInTokenId = "signInTokenId";
    public static final String SignInActivityName = "signInActivityName";
    public static final String SignInFragName = "signInFragName";

    public static final String toKen = "loginToken";
    public static final String toKenDummy = null;

    public static final String googleSignInInfo = "signInInfo";

    /**
     * Sign in and Sign Out
     **/
    public static final String userName = "userName";
    public static final String userImage = "userImage";
    public static final String tokenId = "tokenId";

    public static String latitude = null;
    public static String longitude = null;

    public static final String DeptName = "Dept_Name";
    public static final String FragmentName = "fragment_Name";
    public static final String previousFragmentName = "fragment_Name_Previous";

    public static final String DepartmentId = "Department_Id";
    public static final String ProductId = "Product_Id";

    public static final String PriceNew = "Price_New";
    public static final String PriceOld = "Price_Old";

    public static final String ProductImg = "Product_Img";

    public static final String DateReserve = "Reserve_Date";
    public static final String TimeReserveFrom = "Reserve_Time_From";
    public static final String TimeReserveTo = "Reserve_Time_To";

    public static final String SizeOfItem = "Size_Of_Item";

    public static final String Sku_ID = "Sku_ID";

    public static final String Shop_ID = "Shop_ID";
    public static final String reserveID = "reserveID";

    public static final String BrandName = "BrandName";
    public static final String NoOfProducts = "NoOfProducts";
    public static final String ByStore = "ByStore";

    public static int FavoriteCounter = 0;
    public static int ItemPositionProductList;

    public static final String SearchFragName = "Last_Fragment";
    public static final String SearchActivityName = "Last_Activity";
    public static final String SearchText = "Search_Text";

    public static final String confrmPreferences = "preferencesConfrm";
    public static final String confrmVisitingTime = "visitingTime";
    public static final String confrmProName = "productNameConfrm";
    public static final String confrmBrandName = "brandNameConfrm";
    public static final String confrmSize = "sizeConfrm";
    public static final String confrmPrice = "priceConfrm";
    public static final String confrmAddress = "addressConfrm";
    public static final String confrmDistance = "distanceConfrm";
    public static final String confrmProImg = "productImgConfrm";
    public static final String confrmStoreImg = "storeImgConfrm";



    public static String proId;
    public static String visitType;

    public static ArrayList<Integer> listProductByProduct;
    public static ArrayList<Integer> listProductByProduct22;
    public static ArrayList<Integer> listProductByStore;

    public static ArrayList<String> favoriteItemList;

    /**
     * Filter List
     **/
    public static ArrayList<String> listColor;
    public static ArrayList<Boolean> boolListColor;

    public static ArrayList<String> listBrand;
    public static ArrayList<Boolean> boolListBrand;

    public static ArrayList<String> listCategory;
    public static ArrayList<CatSelectedItem> boolListCategory;

    public static ArrayList<String> listSize;
    public static ArrayList<Boolean> boolListSize;

    /**
     * Department Id
     **/
    public static int departmentId = -1;

    public static String productName = "", brandName = "";
    public static ArrayList<String> sizeList;
    public static ArrayList<String> sizeSkuList;


    /**
     *  Set Price Range
     * **/
    public static int minValue = 0, maxValue = 0;
    public static boolean boolFilterColor = false;
    public static boolean boolFilterBrand = false;
    public static boolean boolFilterSize = false;
    public static boolean boolFilterCategory = false;
    public static boolean boolApplyFilter = false;
    public static JSONObject jsonObject = null;


    /**
     *   boolean variables for calling fragment
     * **/

}
