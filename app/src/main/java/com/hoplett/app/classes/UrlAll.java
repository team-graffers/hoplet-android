package com.hoplett.app.classes;

/**
 * Created by Sandy on 12-03-2019 at 02:29 PM.
 */
public class UrlAll {

    public static final String baseUrl = "http://52.66.151.82:8000/";

    /**
     * Login SignUp url's
     **/
    public static final String fbLoginUrl = baseUrl + "facebook_login";
    public static final String googleLoginUrl = baseUrl + "google_login";
    public static final String signUpUrl = baseUrl + "signup";
    public static final String loginUrl = baseUrl + "login";
    public static final String forgotPswd = baseUrl + "forgetpassword";
    public static final String verifyOTP = baseUrl + "checkotp";
    public static final String changePIN = baseUrl + "changepin";
    /**
     * Department list url
     **/
    public static final String DepartmentListUrl = baseUrl + "departments";

    // by Product
    public static final String ProductListUrl = baseUrl + "product_list?department=";
    public static final String ProductListUrl_Token = baseUrl + "product_list_token?department=";
    public static final String ProductDetailUrl = baseUrl + "product_detail?p_id=";
    public static final String productShop = baseUrl + "product_shop?sku_id=";

    // by Store
    public static final String ProductRelatedToShopUrl = baseUrl + "test_token?shop=";
    public static final String ProductRelatedToShopUrl2 = baseUrl + "test?shop=";
    public static final String BookingVisitUrl = baseUrl + "bookvisit";
    public static final String StoreList = baseUrl + "liststores";

    //Other
    public static final String SearchProductList = baseUrl + "search?search=";
    public static final String SearchProductList_Token = baseUrl + "Search_token?search=";
    public static final String UpcomingVisits = baseUrl + "upcoming_visits?latt=";
    public static final String PastVisits = baseUrl + "past_visits?latt=";
    public static final String ShopList = baseUrl + "LA?latt=";
    /**
     * View Profile and Update Profile
     **/
    public static final String viewProfileUrl = baseUrl + "viewprofile";
    public static final String generateNewPin = baseUrl+"Generatepin";
 //   public static final String editProfile = baseUrl+"editprofile";
    public static final String editProfile = baseUrl+"changeprofilephoto";
    /**
     * Merge Social Id's
     **/
    public static final String mergeSocial = baseUrl + "mergesocial";
    /**
     * Visit Detail
     **/
    public static final String visitDetail = baseUrl + "visitdetail?reserved=";
    /**
     * Re-plan Visit Detail
     **/
    public static final String replanVisitDetail = baseUrl + "replanvisit";
    /**
     * Cancel Visit
     **/
    public static final String cancelVisitDetail = baseUrl + "updatestatus";

    /**
     * Favourite list, add, remove
     **/
    public static final String WishlistUrl = baseUrl + "favouritelist";
    public static final String WishlistAddUrl = baseUrl + "favouriteadd?p_id=";
    public static final String WishlistRemoveUrl = baseUrl + "favouriteremove?p_id=";

    /**
     * Filter Api's
     **/
    public static final String colorFilter = baseUrl + "colorapi";
    public static final String categoryFilterName = baseUrl + "categories?department=";
    public static final String categoryFilterItems = baseUrl + "categoryapi";
    public static final String brandFilter = baseUrl + "brandapi";
    public static final String sizeFilter = baseUrl + "sizeapi";
    public static final String filterProduct = baseUrl+"filtered_product_list";
    public static final String filterProduct_Token = baseUrl+"FilteredProductList_token";
    public static final String priceMinMax = baseUrl+"MaxAndMinPrice";

    /**
     *  Sort by Api's
     * **/
    public static final String whatsNew = baseUrl+"whatsnew?department=";
    public static final String priceLowHighDiscount = baseUrl+"product_list?department=";

    /**
     *  Verify Email nad Pin on the time of user creation
     * **/
    public static String verifyEmail = baseUrl+"GenerateSignUpPin";
    public static String verifyOtpSignUp = baseUrl+"PinCheckSignup";

    /**
     *  Confirm Visit Detail
     * **/
    public static final String confirmVisitDetail = baseUrl+"ConfirmVisit";
}
