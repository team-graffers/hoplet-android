package com.hoplett.app.fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.activities.ByProductActivity;
import com.hoplett.app.adapters.byProduct.AdaptrCatByProduct;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.byProductShopping.productList.ItemProductListByProduct;
import com.hoplett.app.pojoClasses.byProductShopping.productList.ProductListByProductItems;
import com.hoplett.app.pojoClasses.byProductShopping.productList.ResultList;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.LOCATION_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragByProduct extends Fragment {

    View view;
    RecyclerView recyclerView;
    AdaptrCatByProduct adaptrCatByProduct;

    Dialog dialog;
    Context context;

    private boolean isScrolling = false, isPaginate = false;
    private int currentItems, totalItems, scrollOutItems;
    GridLayoutManager layoutManager;
    String nextPage, previousPage;

    SwipeRefreshLayout swipeRefreshLayout;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    ArrayList<Boolean> listBool;
    List<ResultList> listProducts;
    ProductListByProductItems productListByProductItems;
    Button sortByBtn, filterBtn;
    String url;
    HashMap headers;
    private LinearLayout layoutNoProducts;

    public FragByProduct() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_by_product, container, false);

        context = getActivity();
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout_swipe);
        layoutNoProducts = (LinearLayout) view.findViewById(R.id.no_product_available);
        layoutNoProducts.setVisibility(View.GONE);

        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);

        preferences = getActivity().getSharedPreferences(GlobalDataCls.PreferencesName,
                Context.MODE_PRIVATE);
        editor = preferences.edit();

        sortByBtn = (Button) view.findViewById(R.id.sort_by_btn);
        filterBtn = (Button) view.findViewById(R.id.filter_btn);

        sortByBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortByMethod();
            }
        });

        filterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterMethod();
            }
        });

    //    init();

        return view;
    }

    private void init() {

        headers = new HashMap();
        if (preferences.getString(GlobalDataCls.toKen, null) != null){

            headers.put("Content-Type", "application/json");
            String token = preferences.getString(GlobalDataCls.toKen, null);
            headers.put("Authorization", "Token " + token);
            if (GlobalDataCls.boolApplyFilter){
                url = UrlAll.filterProduct_Token;
            }else {url = UrlAll.ProductListUrl_Token + preferences.getString(GlobalDataCls.DepartmentId, null);}
        }else {
            if (GlobalDataCls.boolApplyFilter){
                url = UrlAll.filterProduct;
            }else {url = UrlAll.ProductListUrl + preferences.getString(GlobalDataCls.DepartmentId, null);}
            headers.put("Content-Type", "application/json");
        }

        initializeRecyclrVw();

        LocationManager service = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // Check if enabled and if not send user to the GPS settings
        if (!enabled) {
			/*Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);*/
            Toast.makeText(context, "Enable Your GPS Location", Toast.LENGTH_SHORT).show();
        }else {
            byProductShopping(url);

            paginationMethod();

            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    init();
                }
            });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        init();
    }

    private void initializeRecyclrVw() {

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_vw_by_product);

        listBool = new ArrayList<>();
        listProducts = new ArrayList<>();
        adaptrCatByProduct = new AdaptrCatByProduct(getContext(), listProducts, listBool);

        layoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adaptrCatByProduct);
    }

    private void paginationMethod() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems + 2 == totalItems)) {

                    isScrolling = false;
                    if (nextPage != null) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isPaginate = true;
                                byProductShopping(nextPage);
                            }
                        }, 100);
                    }
                }
            }
        });
    }

    private void sortByMethod() {
        
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_sort_by_layout);

        Button btnWhatsNew = (Button) dialog.findViewById(R.id.whats_new_btn);
        Button btnPriceHighToLow = (Button) dialog.findViewById(R.id.price_high_to_low_btn);
        Button btnPriceLowToHigh = (Button) dialog.findViewById(R.id.price_low_to_high_btn);
        Button btnDiscount = (Button) dialog.findViewById(R.id.discount_btn);

        TextView cancelDialog = (TextView) dialog.findViewById(R.id.cancle_dialog);
        cancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnWhatsNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                initializeRecyclrVw();
                dialog.dismiss();

                headers = new HashMap();
                if (preferences.getString(GlobalDataCls.toKen, null) != null){
                    if (GlobalDataCls.boolApplyFilter){
                        url = UrlAll.filterProduct_Token+ "?filter=whatsnew";
                    }else {
                        url = UrlAll.ProductListUrl_Token + preferences.getString(GlobalDataCls.DepartmentId, null) + "&filter=whatsnew";
                    }
                    headers.put("Content-Type", "application/json");
                    String token = preferences.getString(GlobalDataCls.toKen, null);
                    headers.put("Authorization", "Token " + token);
                }else {
                    if (GlobalDataCls.boolApplyFilter){
                        url = UrlAll.filterProduct+ "?filter=whatsnew";
                    }else {
                        url = UrlAll.ProductListUrl + preferences.getString(GlobalDataCls.DepartmentId, null) + "&filter=whatsnew";
                    }
                    headers.put("Content-Type", "application/json");
                }
                /*url = UrlAll.whatsNew+preferences.getString(GlobalDataCls.DepartmentId, null);*/
                byProductShopping(url);
            }
        });
        btnPriceLowToHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                initializeRecyclrVw();
                dialog.dismiss();
                headers = new HashMap();
                if (preferences.getString(GlobalDataCls.toKen, null) != null){

                    if (GlobalDataCls.boolApplyFilter){
                        url = UrlAll.filterProduct_Token+ "?filter=lowhigh";
                    }else {
                        url = UrlAll.ProductListUrl_Token + preferences.getString(GlobalDataCls.DepartmentId, null) + "&filter=lowhigh";
                    }
                    headers.put("Content-Type", "application/json");
                    String token = preferences.getString(GlobalDataCls.toKen, null);
                    headers.put("Authorization", "Token " + token);
                }else {

                    if (GlobalDataCls.boolApplyFilter){
                        url = UrlAll.filterProduct+ "?filter=lowhigh";
                    }else {
                        url = UrlAll.ProductListUrl + preferences.getString(GlobalDataCls.DepartmentId, null) + "&filter=lowhigh";
                    }
                    headers.put("Content-Type", "application/json");
                }

                Log.d("sort_filter_url", url);
                /*url = UrlAll.priceLowHighDiscount+preferences.getString(GlobalDataCls.DepartmentId, null)+"&filter=--";*/
                byProductShopping(url);
            }
        });
        btnPriceHighToLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                initializeRecyclrVw();
                dialog.dismiss();
                headers = new HashMap();
                if (preferences.getString(GlobalDataCls.toKen, null) != null){

                    if (GlobalDataCls.boolApplyFilter){
                        url = UrlAll.filterProduct_Token+ "?filter=highlow";
                    }else {
                        url = UrlAll.ProductListUrl_Token + preferences.getString(GlobalDataCls.DepartmentId, null) + "&filter=highlow";
                    }
                    headers.put("Content-Type", "application/json");
                    String token = preferences.getString(GlobalDataCls.toKen, null);
                    headers.put("Authorization", "Token " + token);
                }else {

                    if (GlobalDataCls.boolApplyFilter){
                        url = UrlAll.filterProduct+ "?filter=highlow";
                    }else {
                        url = UrlAll.ProductListUrl + preferences.getString(GlobalDataCls.DepartmentId, null) + "&filter=highlow";
                    }
                    headers.put("Content-Type", "application/json");
                }
                /*url = UrlAll.priceLowHighDiscount+preferences.getString(GlobalDataCls.DepartmentId, null)+"&filter=-";*/
                byProductShopping(url);
            }
        });
        btnDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                initializeRecyclrVw();
                dialog.dismiss();
                headers = new HashMap();
                if (preferences.getString(GlobalDataCls.toKen, null) != null){

                    if (GlobalDataCls.boolApplyFilter){
                        url = UrlAll.filterProduct_Token+ "?filter=discount";
                    }else {
                        url = UrlAll.ProductListUrl_Token + preferences.getString(GlobalDataCls.DepartmentId, null) + "&filter=discount";
                    }
                    headers.put("Content-Type", "application/json");
                    String token = preferences.getString(GlobalDataCls.toKen, null);
                    headers.put("Authorization", "Token " + token);
                }else {

                    if (GlobalDataCls.boolApplyFilter){
                        url = UrlAll.filterProduct+ "?filter=discount";
                    }else {
                        url = UrlAll.ProductListUrl + preferences.getString(GlobalDataCls.DepartmentId, null) + "&filter=discount";
                    }
                    headers.put("Content-Type", "application/json");
                }
                /*url = UrlAll.priceLowHighDiscount+preferences.getString(GlobalDataCls.DepartmentId, null)+"&filter=discount";*/
                byProductShopping(url);
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


    /**
     * Filter Method
     **/
    private void filterMethod() {

        editor.putString(GlobalDataCls.FragmentName, "FragFilter");
        editor.commit();
        Intent intent = new Intent(getContext(), ByProductActivity.class);
        getActivity().startActivity(intent);
    }

    private void byProductShopping(String url) {

        if (!swipeRefreshLayout.isRefreshing()) {
            if (isPaginate){
                isPaginate = false;
            }else {dialog.show();}
        }

        GlobalDataCls.listProductByProduct = new ArrayList<>();

        int method;
        if (GlobalDataCls.boolApplyFilter){
            method = Request.Method.POST;
            try {
                GlobalDataCls.jsonObject.put("shop_id", 0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            method = Request.Method.GET;
            GlobalDataCls.jsonObject = null;
        }

        Log.d("method_get_post", ""+method);

        RequestQueue queue = Volley.newRequestQueue(getContext());
        JsonObjectRequest request = new JsonObjectRequest(method, url, GlobalDataCls.jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("response_product", response.toString());
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }

                try {
                    if (response.getInt("count") == 0){
                        swipeRefreshLayout.setVisibility(View.GONE);
                        layoutNoProducts.setVisibility(View.VISIBLE);
                    }else  if (response.getInt("count") > 0){
                        swipeRefreshLayout.setVisibility(View.VISIBLE);
                        layoutNoProducts.setVisibility(View.GONE);

                        String ssss = response.toString();

                        ItemProductListByProduct listByProduct = new Gson().fromJson(ssss, ItemProductListByProduct.class);

                        ResultList resultList;
                        for (int i = 0; i < listByProduct.getResults().size(); i++) {

                            listBool.add(listByProduct.getResults().get(i).getFav());

                            resultList = new ResultList();

                            resultList.setBrand(listByProduct.getResults().get(i).getBrand());
                            resultList.setProduct(listByProduct.getResults().get(i).getProduct());
                            resultList.setProductId(listByProduct.getResults().get(i).getProductId());
                            resultList.setImage(listByProduct.getResults().get(i).getImage());
                            resultList.setShopCount(listByProduct.getResults().get(i).getShopCount());
                            resultList.setPrice(listByProduct.getResults().get(i).getPrice());
                            resultList.setDiscPrice(listByProduct.getResults().get(i).getDiscPrice());
                            resultList.setSkuId(listByProduct.getResults().get(i).getSkuId());

                            listProducts.add(resultList);
                            /*int lll = GlobalDataCls.favoriteItemList.size();

                            if (preferences.getString(GlobalDataCls.toKen, null) != null) {

                                if (lll > 0) {
                                    for (int j = 0; j < GlobalDataCls.favoriteItemList.size(); j++) {

                                        String liID = GlobalDataCls.favoriteItemList.get(j);
                                        String skuID = listByProduct.getResults().get(i).getSkuId().toString();
                                        //                   Log.d("id_list_sku", "liID : "+liID+"    skuID : "+skuID);
                                        if (liID.equals(skuID)) {
                                            listBool.set(listBool.size() - 1, true);
                                            break;
                                        }
                                    }
                                }
                            }*/
                            adaptrCatByProduct.notifyDataSetChanged();
                        }

                        if (listByProduct.getNext() != null && listByProduct.getNext() != "null" && listByProduct.getNext().length() > 0) {

                            nextPage = listByProduct.getNext();
                            Log.d("next_page", nextPage);
                        } else {
                            nextPage = null;
                        }
                    } else {
                        Toast.makeText(context, "Product not available.", Toast.LENGTH_SHORT).show();
                    }

                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();

                Log.d("response_product", error.toString());

                new ApiCallMethods().volleyErrMethod(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return headers;
            }
        };

        queue.add(request);
    }
}
