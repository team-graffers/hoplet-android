package com.hoplett.app.fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.adapters.byStore.AdaptrCatByStore;
import com.hoplett.app.adapters.byStore.AdaptrCatStore;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.CurrentLocation;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.byProductShopping.productList.ProductListByProductItems;
import com.hoplett.app.pojoClasses.storeList.ListResultByStore;

import com.hoplett.app.pojoClasses.storeList.StoreList;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.LOCATION_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragByStore extends Fragment {

    View view;
    RecyclerView recyclerView;
    ExpandableListView expListVwByStore;
    AdaptrCatByStore adaptrCatByStore;
    Dialog dialog;
    Context context;

    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;
    GridLayoutManager layoutManager;
    String nextPage = null, previousPage;

    SwipeRefreshLayout swipeRefreshLayout;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    ArrayList<Boolean> listBool;
    List<ListResultByStore> listProducts;
    ProductListByProductItems productListByProductItems;
    private AdaptrCatStore adapter;
    private LinearLayout layoutNoProducts;

    public FragByStore() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_frag_by_store, container, false);

        context = getActivity();
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout_swipe);
        layoutNoProducts = (LinearLayout) view.findViewById(R.id.no_product_available);
        layoutNoProducts.setVisibility(View.GONE);

        GlobalDataCls.boolApplyFilter = false;
        GlobalDataCls.jsonObject = null;

        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);

        preferences = getActivity().getSharedPreferences(GlobalDataCls.PreferencesName,
                Context.MODE_PRIVATE);
        editor = preferences.edit();

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_vw_by_store);

        init();
        return view;
    }

    private void init() {

        listBool = new ArrayList<>();
        listProducts = new ArrayList<>();
        //      adapter = new AdaptrCatStore(getContext(), listProducts, listBool);

        LocationManager service = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // Check if enabled and if not send user to the GPS settings
        if (!enabled) {
			/*Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);*/
            Toast.makeText(context, "Enable Your GPS Location", Toast.LENGTH_SHORT).show();
        }else {
            storeShopping();

            //     paginationMethod();
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                init();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void storeShopping() {

        if (GlobalDataCls.latitude != null){
            String url;
            if (nextPage != null){
                url = nextPage;
            }else {
                url = UrlAll.ShopList + GlobalDataCls.latitude + "&long=" + GlobalDataCls.longitude+"&department="+preferences.getString(GlobalDataCls.DepartmentId, null);
            }
            byStoreShopping(url);
        }else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    new CurrentLocation(context).lattLong();
                    storeShopping();
                }
            }, 1500);
        }
    }


    private void paginationMethod() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems-5)) {

                    isScrolling = false;
                    String ss = nextPage;
                    if (ss != null) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                storeShopping();
                            }
                        }, 10);
                    }
                }
            }
        });
    }

    private void byStoreShopping(String url) {

        if (!swipeRefreshLayout.isRefreshing()) {
            dialog.show();
        }

        GlobalDataCls.listProductByProduct = new ArrayList<>();

        final HashMap headers = new HashMap();
        headers.put("Content-Type", "application/json");

        RequestQueue queue = Volley.newRequestQueue(getContext());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }

                Log.d("response_store_list", response.toString());

                try {
                    if (response.getJSONArray("data").length() == 0){
                        swipeRefreshLayout.setVisibility(View.GONE);
                        layoutNoProducts.setVisibility(View.VISIBLE);
                    }else if (response.getInt("count") > 0) {

                        swipeRefreshLayout.setVisibility(View.VISIBLE);
                        layoutNoProducts.setVisibility(View.GONE);
                        
                        String ssss = response.toString();

                        StoreList listByProduct = new Gson().fromJson(ssss, StoreList.class);

                        adapter = new AdaptrCatStore(getActivity(), listByProduct);

                        layoutManager = new GridLayoutManager(getContext(), 1);
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(adapter);

                            /*ListResultByStore resultList;
                            for (int i = 0; i < listByProduct.getData().size(); i++) {

                                listBool.add(false);

                                resultList = new ListResultByStore();

                                resultList.setAddress(listByProduct.getData().get(i).getAddress());
                                resultList.setDistance(listByProduct.getData().get(i).getDistance());
                                resultList.setName(listByProduct.getData().get(i).getName());
                                resultList.setImage(listByProduct.getData().get(i).getImage());
                                resultList.setProductCount(listByProduct.getData().get(i).getProductCount());
                                resultList.setShopId(listByProduct.getData().get(i).getShopId());

                                String name = listByProduct.getData().get(i).getShopName();
                                resultList.setShopName(name);
                                resultList.setStoreId(listByProduct.getData().get(i).getStoreId());

                                listProducts.add(resultList);
                            }

                            if (listByProduct.getNext() != null && listByProduct.getNext() != "null" && String.valueOf(listByProduct.getNext()).length()> 0) {

                                nextPage = String.valueOf(listByProduct.getNext());
                            } else {
                                nextPage = null;
                            }*/

                            /*productListByProductItems = new ProductListByProductItems();

                            productListByProductItems.setCount(listByProduct.getCount());
                            productListByProductItems.setNext(String.valueOf(listByProduct.getNext()));
                            productListByProductItems.setPrevious(listByProduct.getPrevious());
                            productListByProductItems.setResults(listProducts);*/

                    //        adapter.notifyDataSetChanged();
                        }
                    /*} else {
                        Toast.makeText(context, "Product not available.", Toast.LENGTH_SHORT).show();
                    }*/

                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();

                Log.d("response_api", error.toString());

                new ApiCallMethods().volleyErrMethod(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return headers;
            }
        };

        queue.add(request);
    }
}
