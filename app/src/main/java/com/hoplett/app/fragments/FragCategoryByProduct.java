package com.hoplett.app.fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import android.support.annotation.RequiresApi;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;

import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.activities.HopletHome;
import com.hoplett.app.adapters.byProduct.AdaptrProductBySort;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.FavoriteItemsCls;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.byStoreShopping.expVw.shopList.ItemShopName;
import com.hoplett.app.pojoClasses.byStoreShopping.expVw.shopList.ShopListData;
import com.hoplett.app.pojoClasses.byStoreShopping.expVw.storeList.ItemStoreName;
import com.hoplett.app.pojoClasses.byStoreShopping.expVw.storeList.StoreListData;
import com.hoplett.app.pojoClasses.wishlist.ItemWishListPojo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragCategoryByProduct extends Fragment {

    LocationManager locationManager;
    boolean gpsStatus = false;

    Button byProduct, byStore;
    TextView byProductTxt, byStoreTxt;

    LinearLayout deptLayout;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    AdaptrProductBySort adaptrProductBySort;

    View view;

    FragByProduct fragByProduct;
    FragByStore fragByStore;

    private Dialog dialog;
    private FragmentManager manager;
    private FragmentTransaction transaction;

    public FragCategoryByProduct() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        HopletHome.appBarLayout.setVisibility(View.VISIBLE);

        view = inflater.inflate(R.layout.fragment_category_by_product, container, false);

        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);

        // Set collapsing tool bar title.
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) view.findViewById(R.id.collapsing_toolbar_layout);
        //     collapsingToolbarLayout.setTitle("Collapsing Tool Bar");

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        /*gpsStatus = locationManager.isLocationEnabled();*/

        preferences = getActivity().getSharedPreferences(GlobalDataCls.PreferencesName,
                Context.MODE_PRIVATE);
        editor = preferences.edit();

        HopletHome.toolbarTitle.setText(preferences.getString(GlobalDataCls.DeptName, null));

        if (preferences.getString(GlobalDataCls.toKen, null) != null){
            initWishlist();
        }

        byProduct = (Button) view.findViewById(R.id.by_product_category);
        byStore = (Button) view.findViewById(R.id.by_store_category);

        byProductTxt = (TextView) view.findViewById(R.id.by_product_txt);
        byStoreTxt = (TextView) view.findViewById(R.id.by_store_txt);

        deptLayout = (LinearLayout) view.findViewById(R.id.progress_dept_lay);


        fragByProduct = new FragByProduct();
        fragByStore = new FragByStore();
        manager = getChildFragmentManager();
        transaction = manager.beginTransaction();

        transaction.add(R.id.frame_layout_by_pro_store, fragByProduct);
        transaction.commit();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (preferences.getString(GlobalDataCls.toKen, null) != null) {
            new FavoriteItemsCls(getActivity());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("resume_frag", "resume_frag");
        layoutClickAndRefresh();
    }

    private void layoutClickAndRefresh() {

        if (preferences.getString(GlobalDataCls.ByStore, null) != null && preferences.getString(GlobalDataCls.ByStore, null).equals("ByStore")) {

            editor.remove(GlobalDataCls.ByStore);
            editor.commit();
            byProductTxt.setText("Store");
            byStoreTxt.setText("Product");
            byStore.setTextColor(getResources().getColor(R.color.white));
            byProduct.setTextColor(getResources().getColor(R.color.black));
            byStore.setBackgroundResource(R.drawable.button_back);
            byProduct.setBackgroundResource(R.drawable.by_store_btn_back);

            layoutClickAndRefresh();
            startFragByStore();
        }

        byProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                byProductTxt.setText("Product");
                byStoreTxt.setText("Store");
                byStore.setTextColor(getResources().getColor(R.color.black));
                byProduct.setTextColor(getResources().getColor(R.color.white));
                byStore.setBackgroundResource(R.drawable.by_store_btn_back);
                byProduct.setBackgroundResource(R.drawable.button_back);
                layoutClickAndRefresh();

                GlobalDataCls.urlProduct = UrlAll.ProductListUrl + preferences.getString(GlobalDataCls.DepartmentId, null);
                startFragByProduct();
            }
        });

        byStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                byProductTxt.setText("Store");
                byStoreTxt.setText("Product");
                byStore.setTextColor(getResources().getColor(R.color.white));
                byProduct.setTextColor(getResources().getColor(R.color.black));
                byStore.setBackgroundResource(R.drawable.button_back);
                byProduct.setBackgroundResource(R.drawable.by_store_btn_back);

                layoutClickAndRefresh();
                startFragByStore();
            }
        });

        deptLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HopletHome activity = (HopletHome) getActivity();
                activity.startDepartmentFrag();
            }
        });
    }

    private void startFragByProduct() {

        transaction = manager.beginTransaction();
        transaction.replace(R.id.frame_layout_by_pro_store, fragByProduct);
        transaction.commit();
    }

    private void startFragSearch() {
        /*transaction = manager.beginTransaction();

        transaction.replace(R.id.frame_layout_by_pro_store, fragByProduct);
        transaction.commit();*/
    }

    private void startFragByStore() {
        transaction = manager.beginTransaction();

        transaction.replace(R.id.frame_layout_by_pro_store, fragByStore);
        transaction.commit();
    }


    ItemStoreName itemStoreName;
    List<StoreListData> storeListData;
    StoreListData item;

    ItemShopName itemShopName;
    HashMap<String, List<ShopListData>> listShopName;

    /**
     * Store Name List
     **/
    private void prepareExpListVwData(final String latt, final String longi) {

        if (dialog.isShowing()) {

        } else {
            //		dialog.show();
        }
        String url = UrlAll.StoreList;
        HashMap headers = new HashMap();
        headers.put("Content-Type", "application/json");

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        String ssss = response.toString();

                        try {
                            if (response.getString("data") != "" && response.getString("data").length() > 0) {

                                itemStoreName = new Gson().fromJson(ssss, ItemStoreName.class);

                                if (itemStoreName.getData() != null && itemStoreName.getData().size() > 0) {

                                    for (int i = 0; i < itemStoreName.getData().size(); i++) {

                                        item = new StoreListData();
                                        item.setImage(itemStoreName.getData().get(i).getImage());
                                        item.setName(itemStoreName.getData().get(i).getName());
                                        item.setStoreId(itemStoreName.getData().get(i).getStoreId());
                                        item.setShopCount(itemStoreName.getData().get(i).getShopCount());

                                        storeListData.add(item);
                                    }
                                    int ss = itemStoreName.getData().size();

             //                       prepareShopNameList(storeListData, ss, latt, longi);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(getContext(), url, headers, dialog);
    }

    /**
     * Shop Name List according to Store
     **/
    private void prepareShopNameList(final List<StoreListData> data, int sizeOfList, String latt, String longi) {

        dialog.show();

        int i;
        for (i = 0; i < data.size(); i++) {

            String url = UrlAll.ShopList + latt + "&long=" + longi + "&store=" + data.get(i).getStoreId();
            HashMap headers = new HashMap();
            headers.put("Content-Type", "application/json");

            final int finalI = i;
            final int finalI1 = i;
            ApiCallMethods apiCallMethods = new ApiCallMethods() {
                @Override
                public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                    return new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            /*if (swipeRefreshLayout.isRefreshing()) {
                                swipeRefreshLayout.setRefreshing(false);
                            }
                            if (swipeRefreshLayout22.isRefreshing()) {
                                swipeRefreshLayout22.setRefreshing(false);
                            }
                            if (swipeRefreshLayout33.isRefreshing()) {
                                swipeRefreshLayout33.setRefreshing(false);
                            }*/

                            String ssss = response.toString();

                            Log.d("response_shopList", ssss);

                            try {
                                if (response.getString("data") != "" && response.getString("data").length() > 0) {

                                    itemShopName = new Gson().fromJson(ssss, ItemShopName.class);

                                    if (itemShopName.getData() != null && itemShopName.getData().size() > 0) {

                                        List<ShopListData> shopListData = new ArrayList<>();
                                        ShopListData listData;

                                        for (int ss = 0; ss < itemShopName.getData().size(); ss++) {

                                            listData = new ShopListData();

                                            listData.setShopName(itemShopName.getData().get(ss).getShopName());
                                            listData.setShopId(itemShopName.getData().get(ss).getShopId());
                                            listData.setAddress(itemShopName.getData().get(ss).getAddress());
                                            listData.setDistance(itemShopName.getData().get(ss).getDistance());
                                            listData.setProductCount(itemShopName.getData().get(ss).getProductCount());
                                            listData.setImage(itemShopName.getData().get(ss).getImage());

                                            shopListData.add(listData);
                                        }

                                        String hashKeyH = data.get(finalI).getStoreId().toString();
                                        listShopName.put(hashKeyH, shopListData);
                                        //              adaptrCatByStore.notifyDataSetChanged();
                                    }
                                }
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                }
            };
            apiCallMethods.volleyJSONObjectGET(getContext(), url, headers, dialog);
        }
    }



    public void initWishlist() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		/*dialog.setCancelable(false);
		dialog.show();*/

        String url = UrlAll.WishlistUrl;

        HashMap headers = new HashMap();
        headers.put("Authorization", "Token " + preferences.getString(GlobalDataCls.toKen, null));

        Log.d("token_token", preferences.getString(GlobalDataCls.toKen, null));
        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("res_wishlist", response.toString());

                        try {
                            if (response.get("data").toString() != "" && response.get("data").toString().length() > 0) {

                                ItemWishListPojo itemList = new Gson().fromJson(response.toString(), ItemWishListPojo.class);

                                if (itemList.getData() != null && itemList.getData().size() > 0 && !itemList.getData().isEmpty()) {

                                    HopletHome.favoriteCount.setText(String.valueOf(itemList.getData().size()));
                                    GlobalDataCls.favoriteItemList = new ArrayList<>();
                                    GlobalDataCls.FavoriteCounter = itemList.getData().size();

                                    for (int i = 0; i < itemList.getData().size(); i++) {
                                        GlobalDataCls.favoriteItemList.add(itemList.getData().get(i).getSkuId().toString());
                                    }
                                }
                            } else {
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(getActivity(), url, headers, dialog);
    }
}
