package com.hoplett.app.fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.activities.HopletHome;
import com.hoplett.app.activities.SignInActivity;
import com.hoplett.app.adapters.AdapterDepartment;
import com.hoplett.app.adapters.leftPanel.AdaptrUpcomingOrders;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.departmentPojo.DepartmentClsPojo;
import com.hoplett.app.pojoClasses.leftPanel.upcomingVisits.ItemUpcomingPastVisit;
import com.hoplett.app.pojoClasses.leftPanel.upcomingVisits.ItemUpcomingVisit;
import com.hoplett.app.pojoClasses.leftPanel.upcomingVisits.ResultUpcomingPastVisit;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragDepartment extends Fragment {

    RecyclerView recyclerView, recyclrVwUpcmngVst;
    LinearLayout upcmngVisitLayout;
    RelativeLayout inactiveLayout, swipeLayout;
    LinearLayout layoutDrawerBack;
    TextView upcmngVstTxtVw, upcmngTxtVwNotAvlbl;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    SwipeRefreshLayout swipeRefreshLayout;
    boolean boolRefreshLayout;

    private Animation animShow, animHide;
    View viewShowHide;
    SlidingDrawer slidingDrawer;
    private GridLayoutManager layoutManager;
    AdapterDepartment adapter;
    AdaptrUpcomingOrders adapterUpcmng;

    public FragDepartment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_department, container, false);

        HopletHome.toolbarTitle.setText("Department");
        boolRefreshLayout = false;

        preferences = getActivity().getSharedPreferences(GlobalDataCls.PreferencesName,
                Context.MODE_PRIVATE);
        editor = preferences.edit();

        GlobalDataCls.boolApplyFilter = false;
        GlobalDataCls.jsonObject = null;

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.top_layout_refresh);
        progressLoader = (ProgressBar) view.findViewById(R.id.progress_loader);

        recyclerView = (RecyclerView) view.findViewById(R.id.department_recycle_vw);
        recyclrVwUpcmngVst = (RecyclerView) view.findViewById(R.id.recycler_vw_upcmng_vst);
        upcmngVisitLayout = (LinearLayout) view.findViewById(R.id.bottom_upcoming_visit_layout);
        upcmngVstTxtVw = (TextView) view.findViewById(R.id.upcmng_vst_txt_vw);
        upcmngTxtVwNotAvlbl = (TextView) view.findViewById(R.id.upcmng_txt_vw_not_avlbl);
        inactiveLayout = (RelativeLayout) view.findViewById(R.id.inactive_layout);
        swipeLayout = (RelativeLayout) view.findViewById(R.id.swipe_layout);
        layoutDrawerBack = (LinearLayout) view.findViewById(R.id.back_layout_sliding_drawer);

        slidingDrawer = (SlidingDrawer) view.findViewById(R.id.sliding_drawer_layout);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                boolRefreshLayout = true;
                onStart();
            }
        });

        upcmngTxtVwNotAvlbl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingDrawer.close();
                editor.putString(GlobalDataCls.SignInActivityName, "HopletHome");
                editor.commit();
                Intent intent = new Intent(getContext(), SignInActivity.class);
                getActivity().startActivity(intent);
            }
        });

        slidingDrawerLayoutMethod();
        prepareData();

        return view;
    }

    private void slidingDrawerLayoutMethod() {

        slidingDrawer.setOnDrawerOpenListener(new SlidingDrawer.OnDrawerOpenListener() {
            @Override
            public void onDrawerOpened() {

                if (preferences.getString(GlobalDataCls.toKen, null) != null) {
                    upcmngTxtVwNotAvlbl.setVisibility(View.GONE);
                    recyclrVwUpcmngVst.setVisibility(View.VISIBLE);
                    upcomingList();
                } else {
                    recyclrVwUpcmngVst.setVisibility(View.GONE);
                    String str = "You are not logged in. <font color = " +
                            "#FF3C35><b><u>Click here for login!</u></b></font>";
                    upcmngTxtVwNotAvlbl.setText(Html.fromHtml(str));
                    upcmngTxtVwNotAvlbl.setVisibility(View.VISIBLE);
                }
            }
        });

        slidingDrawer.setOnDrawerCloseListener(new SlidingDrawer.OnDrawerCloseListener() {
            @Override
            public void onDrawerClosed() {
            }
        });

        slidingDrawer.setOnDrawerScrollListener(new SlidingDrawer.OnDrawerScrollListener() {
            @Override
            public void onScrollStarted() {
                ((LinearLayout) layoutDrawerBack).setGravity(Gravity.BOTTOM);
                slidingDrawer.setBackgroundColor(Color.TRANSPARENT);
            }

            @Override
            public void onScrollEnded() {
            }
        });
    }

    private void upcomingList() {

        if (GlobalDataCls.latitude != null) {
            list = new ArrayList<>();
            itemUpcomingPastVisit = new ItemUpcomingPastVisit();

            layoutManager = new GridLayoutManager(getContext(), 1);
            adapterUpcmng = new AdaptrUpcomingOrders(getContext(), list);
            recyclrVwUpcmngVst.setHasFixedSize(true);
            recyclrVwUpcmngVst.setLayoutManager(layoutManager);
            recyclrVwUpcmngVst.setAdapter(adapterUpcmng);
            String url = UrlAll.UpcomingVisits + GlobalDataCls.latitude + "&long=" + GlobalDataCls.longitude;
            upcomingVisitMethod(url);
            paginationMethod();
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (GlobalDataCls.latitude != null) {
                        list = new ArrayList<>();
                        itemUpcomingPastVisit = new ItemUpcomingPastVisit();
                        layoutManager = new GridLayoutManager(getContext(), 1);
                        adapterUpcmng = new AdaptrUpcomingOrders(getContext(), list);
                        recyclrVwUpcmngVst.setHasFixedSize(true);
                        recyclrVwUpcmngVst.setLayoutManager(layoutManager);
                        recyclrVwUpcmngVst.setAdapter(adapterUpcmng);
                        String url = UrlAll.UpcomingVisits + GlobalDataCls.latitude + "&long=" + GlobalDataCls.longitude;
                        upcomingVisitMethod(url);
                        paginationMethod();
                    } else {
                        upcomingList();
                    }
                }
            }, 1000);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        if (boolRefreshLayout) {
            boolRefreshLayout = false;
            prepareData();
        }
    }

    /*private void initAnimation() {
        animShow = AnimationUtils.loadAnimation(getContext(), R.anim.out_anim);
        animHide = AnimationUtils.loadAnimation(getContext(), R.anim.in_anim);
    }*/

    private void prepareData() {

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        String url = new UrlAll().DepartmentListUrl;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
                /*if (swipeRefreshLayout.isActivated()){
                    swipeRefreshLayout.setActivated(false);
                }*/

                Gson gson = new Gson();

                try {
                    if (response.getString("data") != null && response.getString("data") != "null" && response.getString("data") != "" && response.getString("data").length() > 0) {
                        DepartmentClsPojo departmentClsPojo = gson.fromJson(response.toString(),
                                DepartmentClsPojo.class);
                        if (departmentClsPojo.getData() != null) {
                            prepareList(departmentClsPojo);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("response", response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("responseErr", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        requestQueue.add(request);
    }


    private void prepareList(DepartmentClsPojo departmentClsPojo) {

        adapter = new AdapterDepartment(getContext(), departmentClsPojo);

        recyclerView.setHasFixedSize(true);

        GridLayoutManager manager = new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false);

        manager.setSpanSizeLookup(
                new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        // 2 column size for first row
                        return ((position + 1) % 3 == 0 ? 2 : 1);
                    }
                });
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);

        /*if (swipeRefreshLayout.isActivated()){
            swipeRefreshLayout.setActivated(false);
        }*/

        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;
    ProgressBar progressLoader;
    String nextPage, previousPage;

    private void paginationMethod() {

        recyclrVwUpcmngVst.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {

                    isScrolling = false;
                    if (nextPage != null) {

                        progressLoader.setLayoutParams(new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.MATCH_PARENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM));

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressLoader.setVisibility(View.GONE);
                                upcomingVisitMethod(nextPage);
                            }
                        }, 300);
                    }
                }
            }
        });
    }

    List<ResultUpcomingPastVisit> list;
    private ItemUpcomingPastVisit itemUpcomingPastVisit;

    private void upcomingVisitMethod(String url) {

        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		 /*dialog.setCancelable(false);
		 dialog.show();*/

        HashMap headers = new HashMap();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Token " + preferences.getString(GlobalDataCls.toKen, null));

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("response_upcmng", response.toString());

                        ItemUpcomingVisit upcomingVisit = new Gson().fromJson(response.toString(), ItemUpcomingVisit.class);

                        if (upcomingVisit.getResults() != null && upcomingVisit.getResults().size() > 0 && !upcomingVisit.getResults().isEmpty() && upcomingVisit.getResults().toString() != "") {

                            if (upcomingVisit.getCount() != null || upcomingVisit.getCount().toString() != "" || upcomingVisit.getCount().toString() != "null") {

                                if (upcomingVisit.getCount() > 1) {
                                    upcmngVstTxtVw.setText("UPCOMING VISITS (" + upcomingVisit.getCount().toString() + ")");
                                } else if (upcomingVisit.getCount() == 1) {
                                    upcmngVstTxtVw.setText("UPCOMING VISIT (" + upcomingVisit.getCount().toString() + ")");
                                }
                            }

                            if (upcomingVisit.getResults() != null && upcomingVisit.getResults().size() > 0) {

                                ResultUpcomingPastVisit itemVisit;

                                for (int i = 0; i < upcomingVisit.getResults().size(); i++) {

                                    itemVisit = new ResultUpcomingPastVisit();
                                    itemVisit.setResId(upcomingVisit.getResults().get(i).getResId());
                                    itemVisit.setShopId(upcomingVisit.getResults().get(i).getShopId());
                                    itemVisit.setShopName(upcomingVisit.getResults().get(i).getShopName());
                                    itemVisit.setShopAddress(upcomingVisit.getResults().get(i).getShopAddress());
                                    itemVisit.setDistance(upcomingVisit.getResults().get(i).getDistance());
                                    itemVisit.setDate(upcomingVisit.getResults().get(i).getDate());
                                    itemVisit.setStatus(upcomingVisit.getResults().get(i).getStatus());
                                    itemVisit.setImage(upcomingVisit.getResults().get(i).getImage());
                                    itemVisit.setProductId(upcomingVisit.getResults().get(i).getProductId());

                                    list.add(itemVisit);
                                }

                                itemUpcomingPastVisit.setCount(upcomingVisit.getCount());
                                itemUpcomingPastVisit.setNext(upcomingVisit.getNext());
                                itemUpcomingPastVisit.setPrevious(upcomingVisit.getPrevious());
                                itemUpcomingPastVisit.setResults(list);

                                adapterUpcmng.notifyDataSetChanged();
                            }
                        } else {
                            upcmngTxtVwNotAvlbl.setText("Visit not available.");
                            upcmngTxtVwNotAvlbl.setVisibility(View.VISIBLE);
                            upcmngVisitLayout.setVisibility(View.GONE);
                        }
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(getContext(), url, headers, dialog);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("mainActivityFF", "onAttach");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("mainActivityFF", "onDetach");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("mainActivityFF", "onDestroy");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("mainActivityFF", "onStop");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("mainActivityFF", "onPause");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("mainActivityFF", "onResume");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("mainActivityFF", "onCreate");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("mainActivityFF", "onActivityCreated");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("mainActivityFF", "onDestroyView");
    }


    /*swipeLayout.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            @Override
            public void onSwipeTop() {
                super.onSwipeTop();

                inactiveLayout.setVisibility(View.VISIBLE);
                if (preferences.getString(GlobalDataCls.toKen, null) != null) {

                    upcmngVisitLayout.setVisibility(View.VISIBLE);
                    upcomingVisitMethod(GlobalDataCls.latitude, GlobalDataCls.longitude);

                    upcmngVisitLayout.startAnimation( animShow );

                    viewShowHide = upcmngVisitLayout;
                } else {
                    String str = "You are not logged in. <font color = " +
                            "#FF3C35><b><u>Click here for login!</u></b></font>";
                    upcmngTxtVwNotAvlbl.setText(Html.fromHtml(str));
                    upcmngTxtVwNotAvlbl.setVisibility(View.VISIBLE);

                    upcmngTxtVwNotAvlbl.startAnimation( animShow );
                    viewShowHide = upcmngTxtVwNotAvlbl;
                }
            }

            @Override
            public void onSwipeBottom() {
                super.onSwipeBottom();
                upcmngVisitLayout.setVisibility(View.GONE);
                inactiveLayout.setVisibility(View.GONE);
                upcmngTxtVwNotAvlbl.setVisibility(View.GONE);

                viewShowHide.startAnimation( animHide );
            }
        });*/

        /*upcmngVstTxtVw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (upcmngVisitLayout.getVisibility() == View.VISIBLE || upcmngTxtVwNotAvlbl.getVisibility() == View.VISIBLE) {
                    upcmngVisitLayout.setVisibility(View.GONE);
                    inactiveLayout.setVisibility(View.GONE);
                    upcmngTxtVwNotAvlbl.setVisibility(View.GONE);
                } else {
                    inactiveLayout.setVisibility(View.VISIBLE);
                    if (preferences.getString(GlobalDataCls.toKen, null) != null) {
                        upcmngVisitLayout.setVisibility(View.VISIBLE);
                        upcomingVisitMethod(GlobalDataCls.latitude, GlobalDataCls.longitude);
                    } else {
                        String str = "You are not logged in. <font color = " +
                                "#FF3C35><b><u>Click here for login!</u></b></font>";
                        upcmngTxtVwNotAvlbl.setText(Html.fromHtml(str));
                        upcmngTxtVwNotAvlbl.setVisibility(View.VISIBLE);
                    }
                }
            }
        });*/
}
