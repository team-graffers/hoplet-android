package com.hoplett.app.fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.adapters.byProduct.AdaptrCatByProduct;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.byProductShopping.productList.ItemProductListByProduct;
import com.hoplett.app.pojoClasses.byProductShopping.productList.ProductListByProductItems;
import com.hoplett.app.pojoClasses.byProductShopping.productList.ResultList;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragSearch extends Fragment {

    View view;
    RecyclerView recyclerView;
    AdaptrCatByProduct adaptrCatByProduct;
    Dialog dialog;
    Context context;

    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;
    GridLayoutManager layoutManager;
    String nextPage, previousPage;

    SwipeRefreshLayout swipeRefreshLayout;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    ArrayList<Boolean> listBool;
    List<ResultList> listProducts;
    ProductListByProductItems productListByProductItems;

    public FragSearch() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_search, container, false);

        context = getActivity();
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout_swipe);

        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);

        preferences = getActivity().getSharedPreferences(GlobalDataCls.PreferencesName,
                Context.MODE_PRIVATE);
        editor = preferences.edit();

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_vw_by_product);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        listBool = new ArrayList<>();
        listProducts = new ArrayList<>();
        adaptrCatByProduct = new AdaptrCatByProduct(getContext(), listProducts, listBool);

        layoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adaptrCatByProduct);

        paginationMethod();

        String url = UrlAll.ProductListUrl + preferences.getString(GlobalDataCls.DepartmentId, null);

        byProductShopping(url);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onStart();
            }
        });
    }


    private void paginationMethod() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems-5)) {

                    isScrolling = false;
                    if (nextPage != null) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                byProductShopping(nextPage);
                            }
                        }, 10);
                    }
                }
            }
        });
    }

    private void byProductShopping(String url) {

        if (!swipeRefreshLayout.isRefreshing()) {
            dialog.show();
        }

        GlobalDataCls.listProductByProduct = new ArrayList<>();

        final HashMap headers = new HashMap();
        headers.put("Content-Type", "application/json");

        RequestQueue queue = Volley.newRequestQueue(getContext());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }


                try {
                    if (response.getString("results") != null && response.getString("results").length() > 0) {
                        String ssss = response.toString();

                        ItemProductListByProduct listByProduct = new Gson().fromJson(ssss, ItemProductListByProduct.class);

                        ItemProductListByProduct listByProduct2 = listByProduct;

                        if (listByProduct.getResults() != null) {

                            ResultList resultList;
                            for (int i = 0; i < listByProduct.getResults().size(); i++) {

                                listBool.add(false);

                                resultList = new ResultList();

                                resultList.setBrand(listByProduct.getResults().get(i).getBrand());
                                resultList.setProduct(listByProduct.getResults().get(i).getProduct());
                                resultList.setProductId(listByProduct.getResults().get(i).getProductId());
                                resultList.setImage(listByProduct.getResults().get(i).getImage());
                                resultList.setShopCount(listByProduct.getResults().get(i).getShopCount());
                                resultList.setPrice(listByProduct.getResults().get(i).getPrice());
                                resultList.setDiscPrice(listByProduct.getResults().get(i).getDiscPrice());
                                resultList.setSkuId(listByProduct.getResults().get(i).getSkuId());

                                listProducts.add(resultList);
                                int lll = GlobalDataCls.favoriteItemList.size();

                                if (preferences.getString(GlobalDataCls.toKen, null) != null) {

                                    if (lll > 0) {
                                        for (int j = 0; j < GlobalDataCls.favoriteItemList.size(); j++) {

                                            String liID = GlobalDataCls.favoriteItemList.get(j);
                                            String skuID = listByProduct2.getResults().get(i).getSkuId().toString();
                                            //                   Log.d("id_list_sku", "liID : "+liID+"    skuID : "+skuID);
                                            if (liID.equals(skuID)) {
                                                listBool.set(listBool.size()-1, true);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            if (listByProduct.getNext() != null && listByProduct.getNext() != "null" && listByProduct.getNext().length() > 0) {

                                nextPage = listByProduct.getNext();
                            } else {
                                nextPage = null;
                            }

                            productListByProductItems = new ProductListByProductItems();

                            productListByProductItems.setCount(listByProduct.getCount());
                            productListByProductItems.setNext(listByProduct.getNext());
                            productListByProductItems.setPrevious(listByProduct.getPrevious());
                            productListByProductItems.setResults(listProducts);

                            adaptrCatByProduct.notifyDataSetChanged();
                        }
                    } else {
                        Toast.makeText(context, "Product not available.", Toast.LENGTH_SHORT).show();
                    }

                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();

                Log.d("response_api", error.toString());

                new ApiCallMethods().volleyErrMethod(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return headers;
            }
        };

        queue.add(request);
    }
}
