package com.hoplett.app.fragments.byproduct;


import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.android.volley.*;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.activities.ByProductActivity;
import com.hoplett.app.adapters.filter.AdapterCategory;
import com.hoplett.app.adapters.filter.AdaptrBrand;
import com.hoplett.app.adapters.filter.AdaptrColor;
import com.hoplett.app.adapters.filter.AdaptrSize;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.filter.brand.BrandItems;
import com.hoplett.app.pojoClasses.filter.catName.CatItems;
import com.hoplett.app.pojoClasses.filter.catName.CatItemsDetail;
import com.hoplett.app.pojoClasses.filter.catName.CatNameDetails;
import com.hoplett.app.pojoClasses.filter.catName.CatNames;
import com.hoplett.app.pojoClasses.filter.catName.CatSelectedItem;
import com.hoplett.app.pojoClasses.filter.color.ColorItems;
import com.hoplett.app.pojoClasses.filter.size.SizeItems;

import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragFilter extends Fragment {

    static View view;

    String url;
    static HashMap<String, String> headers;

    LinearLayout closeFilter;

    static SharedPreferences preferences;
    static SharedPreferences.Editor editor;
    static Context context;

    LinearLayout colorLay, brandLay, categoryLay, sizeLay, discntLay, rangeLay, priceLay;
    TextView colorLine, brandLine, categoryLine, sizeLine, discntLine, rangeLine, priceLine;
    TextView colorTxt, brandTxt, categoryTxt, sizeTxt, discntTxt, rangeTxt, priceTxt;

    LinearLayout layoutLL, layoutVisible;
    TextView lineTV, txtTV;

    RecyclerView recyclerVwColor, recyclerVwBrand, recyclerVwSize, recyclerVwDiscount;
    ExpandableListView expVwCategory;

    LinearLayout layoutColor, layoutBrand, layoutCategory, layoutSize, layoutDiscnt, layoutRange, layoutPrice;

    static Dialog dialog;

    Button btnClearFilter, btnApplyFilter;
    RangeSeekBar rangeSeekbar;

    public FragFilter() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_filter, container, false);

        context = this.getActivity();

        dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);

        closeFilter = (LinearLayout) view.findViewById(R.id.cancle_dialog);


        preferences = context.getSharedPreferences(GlobalDataCls.PreferencesName, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(GlobalDataCls.FragmentName, "FragCategoryByProduct");
        editor.commit();

        init();

        closeFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
				/*Intent intent = new Intent(context, HopletHome.class);
				getActivity().startActivity(intent);*/
                ByProductActivity activity = (ByProductActivity) context;
                activity.onBackPressed();
            }
        });

        headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        selectMethod();

        priceMinMax();
        colorFilterMethod();
        brandFilterMethod();
        categoryFilterMethod();
        sizeFilterMethod();
        return view;
    }

    private void priceMinMax() {

        String url = UrlAll.priceMinMax;

        RequestQueue queue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(0, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int minP = response.getInt("min_price");
                    Log.d("max_min", ""+minP);
                    int maxP = response.getInt("max_price");

                    rangeSeekbar.setRangeValues(minP, maxP);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        int socketTimeout = 16000;//16 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void init() {

        colorLay = (LinearLayout) view.findViewById(R.id.lay_color);
        colorLine = (TextView) view.findViewById(R.id.line_color);
        colorTxt = (TextView) view.findViewById(R.id.txt_color);

        brandLay = (LinearLayout) view.findViewById(R.id.lay_brand);
        brandLine = (TextView) view.findViewById(R.id.line_brand);
        brandTxt = (TextView) view.findViewById(R.id.txt_brand);

        categoryLay = (LinearLayout) view.findViewById(R.id.lay_category);
        categoryLine = (TextView) view.findViewById(R.id.line_category);
        categoryTxt = (TextView) view.findViewById(R.id.txt_category);

        sizeLay = (LinearLayout) view.findViewById(R.id.lay_size);
        sizeLine = (TextView) view.findViewById(R.id.line_size);
        sizeTxt = (TextView) view.findViewById(R.id.txt_size);

        discntLay = (LinearLayout) view.findViewById(R.id.lay_discnt);
        discntLine = (TextView) view.findViewById(R.id.line_discnt);
        discntTxt = (TextView) view.findViewById(R.id.txt_discnt);

        rangeLay = (LinearLayout) view.findViewById(R.id.lay_range);
        rangeLine = (TextView) view.findViewById(R.id.line_range);
        rangeTxt = (TextView) view.findViewById(R.id.txt_range);

        priceLay = (LinearLayout) view.findViewById(R.id.lay_price);
        priceLine = (TextView) view.findViewById(R.id.line_price);
        priceTxt = (TextView) view.findViewById(R.id.txt_price);


        layoutLL = colorLay;
        lineTV = colorLine;
        txtTV = colorTxt;

        colorLay.setBackgroundColor(getResources().getColor(R.color.white));
        colorLine.setBackgroundColor(getResources().getColor(R.color.light_red));
        colorTxt.setTextColor(getResources().getColor(R.color.light_red));

        layoutColor = (LinearLayout) view.findViewById(R.id.color_layout);
        layoutBrand = (LinearLayout) view.findViewById(R.id.brand_layout);
        layoutCategory = (LinearLayout) view.findViewById(R.id.category_layout);
        layoutSize = (LinearLayout) view.findViewById(R.id.size_layout);
        layoutDiscnt = (LinearLayout) view.findViewById(R.id.discount_layout);
        layoutRange = (LinearLayout) view.findViewById(R.id.range_layout);
        layoutPrice = (LinearLayout) view.findViewById(R.id.price_layout);

        layoutVisible = layoutColor;

        /*recyclerVwColor = (RecyclerView) view.findViewById(R.id.color_recycler_vw);*/
        /*recyclerVwBrand = (RecyclerView) view.findViewById(R.id.brand_recycler_vw);*/
        /*expVwCategory = (ExpandableListView) view.findViewById(R.id.category_exp_vw);*/
        /*recyclerVwSize = (RecyclerView) view.findViewById(R.id.size_recycler_vw);*/
        /*recyclerVwDiscount = (RecyclerView) view.findViewById(R.id.discount_recycler_vw);*/


        btnApplyFilter = (Button) view.findViewById(R.id.apply_filter);
        btnClearFilter = (Button) view.findViewById(R.id.clear_filter);

        rangeSeekbar = (RangeSeekBar) view.findViewById(R.id.rangeSeekbar);

        rangeSeekbar.setNotifyWhileDragging(true);

        /*rangeSeekbar.setBackgroundColor(R.color.gradient2);*/
        rangeSeekbar.setBackgroundResource(R.color.gradient2);

        rangeSeekbar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Object minVal, Object maxVal) {

                GlobalDataCls.minValue = (int) minVal;
                GlobalDataCls.maxValue = (int) maxVal;
    //            Toast.makeText(getActivity(), "Min Value- " + minVal + " & " + "Max Value- " + maxVal, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void selectMethod() {

        colorTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorChange(colorLay, colorLine, colorTxt, layoutColor);
                colorFilterMethod();
            }
        });

        brandTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorChange(brandLay, brandLine, brandTxt, layoutBrand);
                brandFilterMethod();
            }
        });

        categoryTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorChange(categoryLay, categoryLine, categoryTxt, layoutCategory);
                categoryFilterMethod();
            }
        });

        sizeTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorChange(sizeLay, sizeLine, sizeTxt, layoutSize);
                sizeFilterMethod();
            }
        });

        discntTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorChange(discntLay, discntLine, discntTxt, layoutDiscnt);
            }
        });

        rangeTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorChange(rangeLay, rangeLine, rangeTxt, layoutRange);
            }
        });

        priceTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorChange(priceLay, priceLine, priceTxt, layoutPrice);
            }
        });


        btnClearFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearAllFilters();
            }
        });
        btnApplyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyFilter();
            }
        });
    }

    private void applyFilter() {
        String colorrr = "";
        String sizeee = "";
        String branddd = "";
        String categoryyyy = "";
        for (String i : GlobalDataCls.listColor) {
            colorrr = colorrr + i + " ";
        }

        for (String i : GlobalDataCls.listSize) {
            sizeee = sizeee + i + " ";
        }

        for (String i : GlobalDataCls.listCategory) {
            categoryyyy = categoryyyy + i + " ";
        }

        for (String i : GlobalDataCls.listBrand) {
            branddd = branddd + i + " ";
        }

        Log.d("filter_items", "a\nColor: " + colorrr + "\nSize:  " + sizeee + "\nCategory:  " + categoryyyy + "\nBrand:  " + branddd);

        GlobalDataCls.jsonObject = new JSONObject();
        JSONArray jsonColor = new JSONArray();
        JSONArray jsonBrand = new JSONArray();
        JSONArray jsonSize = new JSONArray();
        JSONArray jsonCategory = new JSONArray();

        for (int i=0; i<GlobalDataCls.listColor.size(); i++){
            jsonColor.put(Integer.parseInt(GlobalDataCls.listColor.get(i)));
        }
        for (int i=0; i<GlobalDataCls.listBrand.size(); i++){
            jsonBrand.put(Integer.parseInt(GlobalDataCls.listBrand.get(i)));
        }
        for (int i=0; i<GlobalDataCls.listSize.size(); i++){
            jsonSize.put(Integer.parseInt(GlobalDataCls.listSize.get(i)));
        }
        for (int i=0; i<GlobalDataCls.listCategory.size(); i++){
            jsonCategory.put(Integer.parseInt(GlobalDataCls.listCategory.get(i)));
        }

        try {
            GlobalDataCls.jsonObject.put("department", preferences.getString(GlobalDataCls.DepartmentId, null));
            GlobalDataCls.jsonObject.put("brands", jsonBrand);
            GlobalDataCls.jsonObject.put("categories", jsonCategory);
            GlobalDataCls.jsonObject.put("colors", jsonColor);
            GlobalDataCls.jsonObject.put("sizes", jsonSize);
            GlobalDataCls.jsonObject.put("min_price", GlobalDataCls.minValue);
            GlobalDataCls.jsonObject.put("max_price", GlobalDataCls.maxValue);

            Log.d("json_filter", GlobalDataCls.jsonObject.toString());

            GlobalDataCls.boolApplyFilter = true;

            ByProductActivity activity = (ByProductActivity) context;
            activity.onBackPressed();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void clearAllFilters() {
        GlobalDataCls.listColor.clear();
        GlobalDataCls.boolListColor.clear();
        GlobalDataCls.listSize.clear();
        GlobalDataCls.boolListSize.clear();
        GlobalDataCls.listCategory.clear();
        GlobalDataCls.boolListCategory.clear();
        GlobalDataCls.listBrand.clear();
        GlobalDataCls.boolListBrand.clear();

        colorFilterMethod();
        brandFilterMethod();
        categoryFilterMethod();
        sizeFilterMethod();
    }

    private void colorChange(LinearLayout lay, TextView line, TextView txt, LinearLayout layVisible) {

        layoutLL.setBackgroundColor(getResources().getColor(R.color.gray_light2));
        lineTV.setBackgroundColor(getResources().getColor(R.color.gray_light2));
        txtTV.setTextColor(getResources().getColor(R.color.black));

        layVisible.setVisibility(View.VISIBLE);
        layoutVisible.setVisibility(View.GONE);
        layoutVisible = layVisible;

        layoutLL = lay;
        lineTV = line;
        txtTV = txt;

        lay.setBackgroundColor(getResources().getColor(R.color.white));
        line.setBackgroundColor(getResources().getColor(R.color.light_red));
        txt.setTextColor(getResources().getColor(R.color.light_red));
    }

    private void colorFilterMethod() {

        if (GlobalDataCls.boolFilterColor){
            GlobalDataCls.boolFilterColor = false;
        }else { dialog.show();}
        url = UrlAll.colorFilter;
        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        Log.d("response_colorFilter", response.toString());

                        try {
                            if (response.get("data").toString() != "" && response.get("data").toString().length() > 0 && !response.get("data").toString().isEmpty() && response.get("data").toString() != null) {

                                ColorItems items = new Gson().fromJson(response.toString(), ColorItems.class);

                                for (int i = 0; i < items.getData().size(); i++) {
                                    boolean b = true;
                                    for (int j = 0; j < GlobalDataCls.listColor.size(); j++) {
                                        if (String.valueOf(items.getData().get(i).getId()).equals(GlobalDataCls.listColor.get(j))) {
                                            b = false;
                                            break;
                                        }
                                    }
                                    if (b) {
                                        GlobalDataCls.boolListColor.add(true);
                                    } else {
                                        GlobalDataCls.boolListColor.add(false);
                                    }
                                }

                                for (int i = 0; i < GlobalDataCls.listColor.size(); ) {
                                    boolean b = true;
                                    for (int j = 0; j < items.getData().size(); j++) {
                                        if (String.valueOf(items.getData().get(j).getId()).equals(GlobalDataCls.listColor.get(i))) {
                                            b = false;
                                            //             Log.d("filter_remove", )
                                            break;
                                        }
                                    }
                                    if (b) {
                                        GlobalDataCls.listColor.remove(i);
                                    } else {
                                        i++;
                                    }
                                }
                                AdaptrColor adapter = new AdaptrColor(context, items.getData(), GlobalDataCls.boolListColor);
                                recyclerVwColor = (RecyclerView) view.findViewById(R.id.color_recycler_vw);
                                recyclerVwColor.setHasFixedSize(true);
                                recyclerVwColor.setLayoutManager(new LinearLayoutManager(context));
                                recyclerVwColor.setAdapter(adapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
            }
        };


        apiCallMethods.jsonRawDataPost(context, url, preferences.getString(GlobalDataCls.DepartmentId,
                null), headers, dialog, "color", GlobalDataCls.listBrand, GlobalDataCls.listCategory,
                GlobalDataCls.listSize);
    }

    private void brandFilterMethod() {

        if (GlobalDataCls.boolFilterBrand){
            GlobalDataCls.boolFilterBrand = false;
        }else { dialog.show();}
        url = UrlAll.brandFilter;
        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        Log.d("response_brandFilter", response.toString());

                        try {
                            if (response.get("data").toString() != "" && response.get("data").toString().length() > 0 && !response.get("data").toString().isEmpty() && response.get("data").toString() != null) {

                                BrandItems itemBrand = new Gson().fromJson(response.toString(),
                                        BrandItems.class);

                                for (int i = 0; i < itemBrand.getData().size(); i++) {
                                    boolean b = true;
                                    for (int j = 0; j < GlobalDataCls.listBrand.size(); j++) {
                                        if (String.valueOf(itemBrand.getData().get(i).getId()).equals(GlobalDataCls.listBrand.get(j))) {
                                            b = false;
                                            break;
                                        }
                                    }
                                    if (b) {
                                        GlobalDataCls.boolListBrand.add(true);
                                    } else {
                                        GlobalDataCls.boolListBrand.add(false);
                                    }
                                }

                                for (int i = 0; i < GlobalDataCls.listBrand.size(); ) {
                                    boolean b = true;
                                    for (int j = 0; j < itemBrand.getData().size(); j++) {
                                        if (String.valueOf(itemBrand.getData().get(j).getId()).equals(GlobalDataCls.listBrand.get(i))) {
                                            b = false;
                                            break;
                                        }
                                    }
                                    if (b) {
                                        GlobalDataCls.listBrand.remove(i);
                                    } else {
                                        i++;
                                    }
                                }
                                AdaptrBrand adapter = new AdaptrBrand(context, itemBrand.getData(), GlobalDataCls.boolListBrand);
                                recyclerVwBrand = (RecyclerView) view.findViewById(R.id.brand_recycler_vw);
                                recyclerVwBrand.setHasFixedSize(true);
                                recyclerVwBrand.setLayoutManager(new LinearLayoutManager(context));
                                recyclerVwBrand.setAdapter(adapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
            }
        };
        if (preferences.getString(GlobalDataCls.DepartmentId, null) != null) {

            String depID = preferences.getString(GlobalDataCls.DepartmentId, null);
            apiCallMethods.jsonRawDataPost(context, url, depID, headers, dialog, "brand", GlobalDataCls.listColor, GlobalDataCls.listCategory, GlobalDataCls.listSize);
        }else {
            preferences = context.getSharedPreferences(GlobalDataCls.PreferencesName, Context.MODE_PRIVATE);
            String depID = preferences.getString(GlobalDataCls.DepartmentId, null);
            apiCallMethods.jsonRawDataPost(context, url, depID, headers, dialog, "brand", GlobalDataCls.listColor, GlobalDataCls.listCategory, GlobalDataCls.listSize);
        }
    }

    int LAST_EXPANDED_POSITION = -1;
    ArrayList<CatNames> listCatNames;
    HashMap<String, List<CatItems>> listCatItems;
    ArrayList<CatSelectedItem> boolList;

    CatNames itemCatName;
    CatItems itemCatItems;
    static AdapterCategory adapterCategory;

    private void categoryFilterMethod() {

        listCatNames = new ArrayList<>();
        listCatItems = new HashMap<>();
        boolList = new ArrayList<>();

        PrepareCatNameList();

        /*adapterCategory = new AdapterCategory(context, listCatNames, listCatItems, boolList);

        expVwCategory.setAdapter(adapterCategory);

        expVwCategory.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return false;
            }
        });

        expVwCategory.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                if (LAST_EXPANDED_POSITION != -1 && groupPosition != LAST_EXPANDED_POSITION) {
                    expVwCategory.collapseGroup(LAST_EXPANDED_POSITION);
                }

                LAST_EXPANDED_POSITION = groupPosition;
            }
        });

        expVwCategory.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {

            }
        });*/
    }

    private void PrepareCatNameList() {

        if (GlobalDataCls.boolFilterCategory){
            GlobalDataCls.boolFilterCategory = false;
        }else { dialog.show();}
        url = UrlAll.categoryFilterName + preferences.getString(GlobalDataCls.DepartmentId, null);

        HashMap headers = new HashMap();
        headers.put("Content-Type", "application/json");

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("response_categoryFilter", response.toString());

                        try {
                            if (response.get("data").toString() != "" && response.get("data").toString().length() > 0 && !response.get("data").toString().isEmpty() && response.get("data").toString() != null) {

                                CatNameDetails catNameDetails = new Gson().fromJson(response.toString(), CatNameDetails.class);

                                if (catNameDetails.getData() != null && catNameDetails.getData().size() > 0) {

                                    for (int i = 0; i < catNameDetails.getData().size(); i++) {

                                        if (catNameDetails.getData().get(i).getActive()) {
                                            itemCatName = new CatNames();

                                            itemCatName.setId(catNameDetails.getData().get(i).getId().toString());
                                            itemCatName.setIsActive(catNameDetails.getData().get(i).getActive());
                                            itemCatName.setName(catNameDetails.getData().get(i).getName());
                                            itemCatName.setParent(String.valueOf(catNameDetails.getData().get(i).getParent()));

                                            listCatNames.add(itemCatName);
                                        }
                                    }

                                    prepareCatItemsList();
                                }

                                /*if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }*/
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(context, url, headers, dialog);
    }

    private void prepareCatItemsList() {

        /*dialog.show();*/
        url = UrlAll.categoryFilterItems;

        HashMap headers = new HashMap();
        headers.put("Content-Type", "application/json");

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("response_category2222", response.toString());

                        try {
                            if (response.get("data").toString() != "" && response.get("data").toString().length() > 0 && !response.get("data").toString().isEmpty() && response.get("data").toString() != null) {

                                CatItemsDetail catItemsDetail = new Gson().fromJson(response.toString(), CatItemsDetail.class);

                                if (catItemsDetail.getData() != null && catItemsDetail.getData().size() > 0) {

                                    for (int cat = 0; cat < listCatNames.size(); ) {

                                        ArrayList<CatItems> items = new ArrayList<>();

                                        for (int i = 0; i < catItemsDetail.getData().size(); i++) {

                                            if (listCatNames.get(cat).getId().equals(catItemsDetail.getData().get(i).getId().toString())) {

                                                if (catItemsDetail.getData().get(i).getActive()) {

                                                    itemCatItems = new CatItems();

                                                    itemCatItems.setId(catItemsDetail.getData().get(i).getId().toString());
                                                    itemCatItems.setIsActive(catItemsDetail.getData().get(i).getActive());
                                                    itemCatItems.setName(catItemsDetail.getData().get(i).getName());
                                                    itemCatItems.setParent(catItemsDetail.getData().get(i).getParent().toString());

                                                    items.add(itemCatItems);

                                                    boolean b = true;
                                                    for (int j = 0; j < GlobalDataCls.listCategory.size(); j++) {
                                                        if (String.valueOf(catItemsDetail.getData().get(i).getId()).equals(GlobalDataCls.listCategory.get(j))) {
                                                            b = false;
                                                            break;
                                                        }
                                                    }

                                                    CatSelectedItem item = new CatSelectedItem();
                                                    item.setId(catItemsDetail.getData().get(i).getId().toString());
                                                    item.setBool(true);

                                                    if (b) {
                                                        item.setBool(true);
                                                    } else {
                                                        item.setBool(false);
                                                    }

                                                    boolList.add(item);
                                                    GlobalDataCls.boolListCategory.add(item);
                                                }
                                            }
                                        }

                                        if (items.size() > 0) {

                                            listCatItems.put(listCatNames.get(cat).getId(), items);
                                   //         adapterCategory.notifyDataSetChanged();
                                            cat++;
                                        } else {
                                            listCatNames.remove(cat);
                                   //         adapterCategory.notifyDataSetChanged();
                                        }
                                    }


                                    for (int i = 0; i < GlobalDataCls.listCategory.size(); ) {
                                        boolean b = true;

                                        for (int c = 0; c < listCatNames.size(); c++) {
                                            for (int j = 0; j < listCatItems.get(listCatNames.get(c).getId()).size(); j++) {
                                                if (listCatItems.get(listCatNames.get(c).getId()).get(j).getId().equals(GlobalDataCls.listCategory.get(i))) {
                                                    b = false;
                                                    break;
                                                }
                                            }
                                        }

                                        if (b) {
                                            GlobalDataCls.listCategory.remove(i);
                                        } else {
                                            i++;
                                        }
                                    }
                                }

                                adapterCategory = new AdapterCategory(context, listCatNames, listCatItems, boolList);

                                expVwCategory = (ExpandableListView) view.findViewById(R.id.category_exp_vw);
                                expVwCategory.setAdapter(adapterCategory);

                                expVwCategory.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                                    @Override
                                    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                                        return false;
                                    }
                                });

                                expVwCategory.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                                    @Override
                                    public void onGroupExpand(int groupPosition) {
                                        if (LAST_EXPANDED_POSITION != -1 && groupPosition != LAST_EXPANDED_POSITION) {
                                            expVwCategory.collapseGroup(LAST_EXPANDED_POSITION);
                                        }

                                        LAST_EXPANDED_POSITION = groupPosition;
                                    }
                                });

                                expVwCategory.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
                                    @Override
                                    public void onGroupCollapse(int groupPosition) {

                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }
                };
            }
        };
        apiCallMethods.jsonRawDataPost(context, url, preferences.getString(GlobalDataCls.DepartmentId,
                null), headers, dialog, "category", GlobalDataCls.listColor, GlobalDataCls.listBrand,
                GlobalDataCls.listSize);
    }

    private void sizeFilterMethod() {

        if (GlobalDataCls.boolFilterSize){
            GlobalDataCls.boolFilterSize = false;
        }else { dialog.show();}
        url = UrlAll.sizeFilter;
        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        Log.d("response_sizeFilter", response.toString());

                        try {
                            if (response.get("data").toString() != "" && response.get("data").toString().length() > 0 && !response.get("data").toString().isEmpty() && response.get("data").toString() != null) {

                                SizeItems items = new Gson().fromJson(response.toString(), SizeItems.class);

                                for (int i = 0; i < items.getData().size(); i++) {
                                    boolean b = true;
                                    for (int j = 0; j < GlobalDataCls.listSize.size(); j++) {
                                        if (String.valueOf(items.getData().get(i).getId()).equals(GlobalDataCls.listSize.get(j))) {
                                            b = false;
                                            break;
                                        }
                                    }
                                    if (b) {
                                        GlobalDataCls.boolListSize.add(true);
                                    } else {
                                        GlobalDataCls.boolListSize.add(false);
                                    }
                                }

                                for (int i = 0; i < GlobalDataCls.listSize.size(); ) {
                                    boolean b = true;
                                    for (int j = 0; j < items.getData().size(); j++) {
                                        if (String.valueOf(items.getData().get(j).getId()).equals(GlobalDataCls.listSize.get(i))) {
                                            b = false;
                                            break;
                                        }
                                    }
                                    if (b) {
                                        GlobalDataCls.listSize.remove(i);
                                    } else {
                                        i++;
                                    }
                                }
                                AdaptrSize adapter = new AdaptrSize(context, items.getData(), GlobalDataCls.boolListSize);
                                recyclerVwSize = (RecyclerView) view.findViewById(R.id.size_recycler_vw);
                                recyclerVwSize.setHasFixedSize(true);
                                recyclerVwSize.setLayoutManager(new LinearLayoutManager(context));
                                recyclerVwSize.setAdapter(adapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
            }
        };
        apiCallMethods.jsonRawDataPost(context, url, preferences.getString(GlobalDataCls.DepartmentId,
                null), headers, dialog, "size", GlobalDataCls.listColor, GlobalDataCls.listBrand,
                GlobalDataCls.listCategory);
    }

    public void callAllApis(int val){
        switch (val){

            case 1:
                brandFilterMethod();
                categoryFilterMethod();
                sizeFilterMethod();
                break;

            case 2:
                colorFilterMethod();
                categoryFilterMethod();
                sizeFilterMethod();
                break;

            case 3:
                brandFilterMethod();
                colorFilterMethod();
                sizeFilterMethod();
                break;

            case 4:
                brandFilterMethod();
                categoryFilterMethod();
                colorFilterMethod();
                break;
        }
    }
}
