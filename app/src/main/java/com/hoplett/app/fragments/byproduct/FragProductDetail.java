package com.hoplett.app.fragments.byproduct;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.activities.ByProductActivity;
import com.hoplett.app.activities.HopletHome;
import com.hoplett.app.activities.SignInActivity;
import com.hoplett.app.activities.WishListActivity;
import com.hoplett.app.adapters.AdapterSlidingImg;
import com.hoplett.app.adapters.byProduct.AdaptrProductSize;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.byProductShopping.productDetail.ProductDetail;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.LOCATION_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragProductDetail extends Fragment {

    ImageButton backBtn;
    public static ImageView imgProduct, imgPriceOld;
    public static TextView nameBrand, nameProduct, price, oldPrice, discount;
    RadioGroup radioGroup;
    Button selectStoreBtn, addToWishlist;
    RecyclerView recyclerViewSize;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    private static ViewPager viewPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    View view;

    public FragProductDetail() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_product_detail, container, false);

        preferences = getActivity().getSharedPreferences(GlobalDataCls.PreferencesName, Context.MODE_PRIVATE);
        editor = preferences.edit();

        backBtn = (ImageButton) view.findViewById(R.id.back_btn);

        imgPriceOld = (ImageView) view.findViewById(R.id.old_price_img);
        nameBrand = (TextView) view.findViewById(R.id.brand_name);
        nameProduct = (TextView) view.findViewById(R.id.product_name);
        price = (TextView) view.findViewById(R.id.price_product);
        oldPrice = (TextView) view.findViewById(R.id.old_price_product);
        discount = (TextView) view.findViewById(R.id.price_discount);

        viewPager = (ViewPager) view.findViewById(R.id.viewpager);

//		radioGroup = (RadioGroup) view.findViewById(R.id.radio_grp);

        selectStoreBtn = (Button) view.findViewById(R.id.select_store_btn);
        addToWishlist = (Button) view.findViewById(R.id.add_to_wishlist_btn);

        recyclerViewSize = (RecyclerView) view.findViewById(R.id.recycler_vw_size);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (preferences.getString(GlobalDataCls.previousFragmentName, null) != null && preferences.getString(GlobalDataCls.previousFragmentName, null).equals("FragSearchProduct")) {
                    editor.remove(GlobalDataCls.previousFragmentName);
                    editor.putString(GlobalDataCls.FragmentName, "FragSearchProduct");
                } else {
                    editor.putString(GlobalDataCls.FragmentName, "FragCategoryByProduct");
                }
                editor.commit();

                Intent intent = new Intent(getContext(), HopletHome.class);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivity(intent);
            }
        });

        selectStoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationManager service = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
                boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

                // Check if enabled and if not send user to the GPS settings
                if (!enabled) {
			/*Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);*/
                    Toast.makeText(getActivity(), "Enable Your GPS Location", Toast.LENGTH_SHORT).show();
                }else {
                    ByProductActivity activity = (ByProductActivity) getActivity();
                    activity.startStoreListFragment();
                }
            }
        });

        addToWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (preferences.getString(GlobalDataCls.toKen, null) != null) {

                    if (addToWishlist.getText().toString().equals("WishListed")) {
                        getActivity().startActivity(new Intent(getContext(), WishListActivity.class));
                    } else {
                        //         addToWishlist.setText("Add to Wishlist");
                        addToWishListMethod();
                    }
                } else {
                    getActivity().startActivity(new Intent(getContext(), SignInActivity.class));
                }
            }
        });

        if (GlobalDataCls.proDetailBool) {
            addToWishlist.setText("WishListed");
        }


        productDetailMethod();

        return view;
    }

    private void addToWishListMethod() {

        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        String pro_id = preferences.getString(GlobalDataCls.ProductId, null);
        String sku_id = preferences.getString(GlobalDataCls.Sku_ID, null);
        String url = UrlAll.WishlistAddUrl + pro_id + "&sku_idd=" + sku_id;
        HashMap headers = new HashMap();
        headers.put("Authorization", "Token " + preferences.getString(GlobalDataCls.toKen, null));


        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        dialog.dismiss();
                        addToWishlist.setText("WishListed");
                        GlobalDataCls.FavoriteCounter++;
                        try {
                            if (response.getInt("count")>0)
                            HopletHome.favoriteCount.setText(String.valueOf(response.getInt("count")));
                            else HopletHome.favoriteCount.setText("");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(getContext(), url, headers, dialog);
    }

    private void productDetailMethod() {

        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        String url = UrlAll.ProductDetailUrl + preferences.getString(GlobalDataCls.ProductId, null);

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                final ProductDetail productDetail = new Gson().fromJson(response.toString(), ProductDetail.class);

                if (productDetail.getData() != null) {

                    ArrayList<String> ImagesArray = new ArrayList<>();

                    for (int i = 0; i < productDetail.getData().getImage().size() && i < 4; i++) {

                        String imgStr = productDetail.getData().getImage().get(i).getName();

                        if (imgStr != null && imgStr.length() > 0 && imgStr != "null") {

                            final String img = imgStr.substring(0, imgStr.length() - 4) + "raw=1";

                            final int[] width = new int[1];
                            final int[] height = new int[1];

                            ImagesArray.add(img);

                            /*ViewTreeObserver vto = imgProduct.getViewTreeObserver();

                            final int finalI = i;
                            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                                public boolean onPreDraw() {
                                    imgProduct.getViewTreeObserver().removeOnPreDrawListener(this);
                                    height[0] = imgProduct.getMeasuredHeight();
                                    width[0] = imgProduct.getMeasuredWidth();

                                    Picasso.with(getContext()).load(img).fit().error(R.drawable.default_img_back).into(imgProduct);
                                    return true;
                                }
                            });*/
                        }
                    }

                    viewPager.setAdapter(new AdapterSlidingImg(getActivity(), ImagesArray, currentPage));

                    viewPager.setCurrentItem(currentPage, true);
                    CirclePageIndicator indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);

                    indicator.setViewPager(viewPager);

                    final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
                    indicator.setRadius(3 * density);

                    NUM_PAGES = ImagesArray.size();

                    // Pager listener over indicator
                    indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                        @Override
                        public void onPageSelected(int position) {
                            currentPage = position;
                        }
                        @Override
                        public void onPageScrolled(int pos, float arg1, int arg2) {
                        }
                        @Override
                        public void onPageScrollStateChanged(int pos) {
                        }
                    });

                    nameBrand.setText(productDetail.getData().getBrand());
                    nameProduct.setText(productDetail.getData().getProductName());

                    GlobalDataCls.productName = productDetail.getData().getProductName();
                    GlobalDataCls.brandName = productDetail.getData().getBrand();

                    if (productDetail.getData().getSize() != null && productDetail.getData().getSize().size() > 0 && !productDetail.getData().getSize().isEmpty()) {

                        /*editor.putString(GlobalDataCls.Sku_ID,
                                productDetail.getData().getSize().get(0).getSkuId().toString());
                        editor.commit();*/

                        AdaptrProductSize adaptrProductSize = new AdaptrProductSize(getContext(),
                                productDetail, selectStoreBtn);

                        recyclerViewSize.setHasFixedSize(true);
                        recyclerViewSize.setLayoutManager(new LinearLayoutManager(getContext(),
                                LinearLayoutManager.HORIZONTAL, false));
                        recyclerViewSize.setAdapter(adaptrProductSize);

                        GlobalDataCls.sizeList = new ArrayList<>();
                        GlobalDataCls.sizeSkuList = new ArrayList<>();
                        for (int i = 0; i < productDetail.getData().getSize().size(); i++) {
                            GlobalDataCls.sizeList.add(productDetail.getData().getSize().get(i).getSize());
                            GlobalDataCls.sizeSkuList.add(productDetail.getData().getSize().get(i).getSkuId().toString());

                            /*String sku_id = productDetail.getData().getSize().get(i).getSize();
                            String sku_id_size = productDetail.getData().getSize().get(i).getSkuId().toString();

                            Log.d("sku_ids", "sku_id:"+sku_id+"    sku_size:"+sku_id_size);*/
                        }
                    }
                }
                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("errResponse", error.toString());
                dialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        requestQueue.add(request);
    }
}
