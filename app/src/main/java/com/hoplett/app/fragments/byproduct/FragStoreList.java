package com.hoplett.app.fragments.byproduct;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.activities.ByProductActivity;
import com.hoplett.app.adapters.byProduct.AdapterResrvByProduct;
import com.hoplett.app.adapters.byProduct.AdaptrSizeFab;
import com.hoplett.app.adapters.byProduct.AdaptrStoreListByProduct;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.CurrentLocation;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.byProductShopping.storeList.StoreListByProduct;
import com.hoplett.app.pojoClasses.res_cls.ResCls;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.LOCATION_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragStoreList extends Fragment {

	static SharedPreferences preferences;
	static SharedPreferences.Editor editor;

	static SharedPreferences preferencesConfrm;
	static SharedPreferences.Editor editorConfrm;

	RecyclerView recyclerView;
	TextView favoriteDot;
	static ImageButton backBtn;
	static View view;
	static LinearLayout reserveLayout, inactiveLayout;
	static Button sortByBtn;
	static Context context;
	String url;

/**
 *  Fab Button details
 * **/
	CircleImageView imgVwFab;
	ImageView imgVwRectFab, imgOldPrice;
	TextView closeBtnFab, productNameFab, brandNameFab, priceFab, oldPriceFab, discountFab;
	RecyclerView recyclerVwFab;
	LinearLayout fabLayout, circleImgLayout;

	public FragStoreList() {
		// Required empty public constructor
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
													 Bundle savedInstanceState) {

		view = inflater.inflate(R.layout.fragment_store_list, container, false);

		context = getActivity();

//		Toast.makeText(context, "Store List Fragment...!", Toast.LENGTH_SHORT).show();

		preferences = context.getSharedPreferences(GlobalDataCls.PreferencesName,
			Context.MODE_PRIVATE);
		editor = preferences.edit();

		preferencesConfrm = context.getSharedPreferences(GlobalDataCls.confrmPreferences,
				Context.MODE_PRIVATE);
		editorConfrm = preferencesConfrm.edit();

		recyclerView = (RecyclerView) view.findViewById(R.id.recyclr_vw_store_list_by_product);
		favoriteDot = (TextView) view.findViewById(R.id.favorite_dot);
		backBtn = (ImageButton) view.findViewById(R.id.back_btn);
		sortByBtn = (Button) view.findViewById(R.id.sort_by_btn);
		sortByBtn.setVisibility(View.VISIBLE);

	/**
	 *  Fab layout items
	 * **/
		imgVwFab = (CircleImageView) view.findViewById(R.id.fab_circle_img_vw);
		imgVwRectFab = (ImageView) view.findViewById(R.id.fab_img_vw);
		imgOldPrice = (ImageView) view.findViewById(R.id.img_old_price);
		closeBtnFab = (TextView) view.findViewById(R.id.close_dialog_fab);
		productNameFab = (TextView) view.findViewById(R.id.product_name_fab);
		brandNameFab = (TextView) view.findViewById(R.id.brand_name_fab);
		priceFab = (TextView) view.findViewById(R.id.product_price_fab);
		oldPriceFab = (TextView) view.findViewById(R.id.product_price_old_fab);
		discountFab = (TextView) view.findViewById(R.id.product_discount_fab);
		recyclerVwFab = (RecyclerView) view.findViewById(R.id.recycler_vw_fab);
		fabLayout = (LinearLayout) view.findViewById(R.id.fab_layout);
		circleImgLayout = (LinearLayout) view.findViewById(R.id.circle_img_layout);

		if (GlobalDataCls.FavoriteCounter > 0) {
			favoriteDot.setText(String.valueOf(GlobalDataCls.FavoriteCounter));
		}

		backBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ByProductActivity activity = (ByProductActivity) getActivity();
				activity.startProductDetailFragment();
			}
		});

		recyclerViewReserve = (RecyclerView) view.findViewById(R.id.recycler_vw_reserve);

		reserveLayout = (LinearLayout) view.findViewById(R.id.reserve_layout);
		inactiveLayout = (LinearLayout) view.findViewById(R.id.inactive_layout);
		reserveLayout.setVisibility(View.GONE);
		inactiveLayout.setVisibility(View.GONE);

		url = UrlAll.productShop+preferences.getString(GlobalDataCls.Sku_ID, null)+"&latt="+GlobalDataCls.latitude+ "&long="+GlobalDataCls.longitude;
		locationLive();

		if (preferences.getString(GlobalDataCls.SignInFragName, null) != null){

			editor.remove(GlobalDataCls.SignInFragName);
			editor.commit();
//			reserveMethod();
		}

		fabMethod();


		sortByBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sortByMethod();
			}
		});
		return view;
	}

	private void sortByMethod() {

		final Dialog dialog = new Dialog(getContext());
		dialog.setContentView(R.layout.dialog_sort_by_layout);

		Button btnWhatsNew = (Button) dialog.findViewById(R.id.whats_new_btn);
		Button btnPriceHighToLow = (Button) dialog.findViewById(R.id.price_high_to_low_btn);
		Button btnPriceLowToHigh = (Button) dialog.findViewById(R.id.price_low_to_high_btn);
		Button btnDiscount = (Button) dialog.findViewById(R.id.discount_btn);

		TextView cancelDialog = (TextView) dialog.findViewById(R.id.cancle_dialog);

		btnWhatsNew.setText("Distance");

		cancelDialog.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		btnWhatsNew.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();
				url = UrlAll.productShop+preferences.getString(GlobalDataCls.Sku_ID, null)+"&latt="+GlobalDataCls.latitude+ "&long="+GlobalDataCls.longitude+"&filter=distance";
				storeListMethod();
			}
		});
		btnPriceLowToHigh.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();
				url = UrlAll.productShop+preferences.getString(GlobalDataCls.Sku_ID, null)+"&latt="+GlobalDataCls.latitude+ "&long="+GlobalDataCls.longitude+"&filter=lowhigh";
				storeListMethod();
			}
		});
		btnPriceHighToLow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();
				url = UrlAll.productShop+preferences.getString(GlobalDataCls.Sku_ID, null)+"&latt="+GlobalDataCls.latitude+ "&long="+GlobalDataCls.longitude+"&filter=highlow";
				storeListMethod();
			}
		});
		btnDiscount.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();
				url = UrlAll.productShop+preferences.getString(GlobalDataCls.Sku_ID, null)+"&latt="+GlobalDataCls.latitude+ "&long="+GlobalDataCls.longitude+"&filter=discount";
				storeListMethod();
			}
		});

		dialog.setCancelable(false);
		dialog.show();
	}


	private void locationLive() {

		LocationManager service = (LocationManager) context.getSystemService(LOCATION_SERVICE);
		boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

		// Check if enabled and if not send user to the GPS settings
		if (!enabled) {
			/*Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);*/
			Toast.makeText(context, "Enable Your GPS", Toast.LENGTH_SHORT).show();
		}else {
			if (GlobalDataCls.latitude != null) {
				storeListMethod();
			} else {
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						new CurrentLocation(context).lattLong();
						locationLive();
					}
				}, 1000);
			}
		}
	}

	private void fabMethod() {

		circleImgLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sortByBtn.setVisibility(View.GONE);
				fabLayout.setVisibility(View.VISIBLE);
			}
		});

		closeBtnFab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sortByBtn.setVisibility(View.VISIBLE);
				fabLayout.setVisibility(View.GONE);
			}
		});

		String imgUrl = preferences.getString(GlobalDataCls.ProductImg, null);

		if (imgUrl != null && imgUrl != "null" && imgUrl.length()>0) {

			final String img = imgUrl.substring(0, imgUrl.length() - 4) + "raw=1";

			final int[] width = new int[1];
			final int[] height = new int[1];

			ViewTreeObserver vto = imgVwFab.getViewTreeObserver();

			vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
				public boolean onPreDraw() {
					imgVwFab.getViewTreeObserver().removeOnPreDrawListener(this);
					height[0] = imgVwFab.getMeasuredHeight();
					width[0] = imgVwFab.getMeasuredWidth();

					Picasso.with(getContext()).load(img).fit().placeholder(R.drawable.loading_default_image).error(R.drawable.default_img_back).into(imgVwFab);
					return true;
				}
			});

			Picasso.with(getContext()).load(img).error(R.drawable.default_img_back).into(imgVwRectFab);
		}else {
			imgVwFab.setImageResource(R.drawable.default_img_back);
			imgVwRectFab.setImageResource(R.drawable.default_img_back);
		}

		productNameFab.setText(GlobalDataCls.productName);
		brandNameFab.setText(GlobalDataCls.brandName);
		priceFab.setText(preferences.getString(GlobalDataCls.PriceNew, null));
		if (preferences.getString(GlobalDataCls.PriceNew, null).equals(preferences.getString(GlobalDataCls.PriceOld, null))){
			imgOldPrice.setVisibility(View.GONE);
		}else {
			imgOldPrice.setVisibility(View.VISIBLE);
			String oldP = "<del>" + preferences.getString(GlobalDataCls.PriceOld, null) + "</del>";
			oldPriceFab.setText(Html.fromHtml(oldP));

			int priceNew = Integer.parseInt(preferences.getString(GlobalDataCls.PriceNew, null));

			if (preferences.getString(GlobalDataCls.PriceOld, null) != null) {

				int priceOld = Integer.parseInt(preferences.getString(GlobalDataCls.PriceOld, null));
				String discPrice = String.valueOf(100 - Math.round(priceNew * 100 / priceOld));
				String discnt = "(<font color=#F4684E>" + discPrice + "%Off</font>)";
				discountFab.setText(Html.fromHtml(discnt));
			}
		}
		boolean bool = true;

		/*GlobalDataCls.sizeList = new ArrayList<>();
		GlobalDataCls.sizeSkuList = new ArrayList<>();*/

		AdaptrSizeFab adapter = new AdaptrSizeFab(getContext(), GlobalDataCls.sizeList,
			GlobalDataCls.sizeSkuList, bool);

		recyclerVwFab.setHasFixedSize(true);
		recyclerVwFab.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
		recyclerVwFab.setAdapter(adapter);
	}

	private void storeListMethod() {

		final Dialog dialog = new Dialog(getContext());
		dialog.setContentView(R.layout.dialog_category_layout);
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		dialog.setCancelable(false);
		dialog.show();

		HashMap<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");

		ApiCallMethods apiCallMethods = new ApiCallMethods(){
			@Override
			public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
				return new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {

						StoreListByProduct listByProduct = new Gson().fromJson(response.toString(), StoreListByProduct.class);

						AdaptrStoreListByProduct adapter = new AdaptrStoreListByProduct(getContext(),
							listByProduct.getData());

						recyclerView.setHasFixedSize(true);
						recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
						recyclerView.setAdapter(adapter);
						dialog.dismiss();
					}
				};
			}
		};
		apiCallMethods.volleyJSONObjectGET(getContext(), url, headers, dialog);
	}


/**
 *  Reserve Item Layout
 * **/
	static Button doneBtn, cancelBtn;

	static LinearLayout todayLayout, tomorrowLayout;
	static TextView openTime, closeTime, closeBtn;
	static TextView todayTxtVw, todaySmallTxtVw, todayTxtLine, tomorrowTxtVw, tomorrowSmallTxtVw, tomorrowTxtLine;
	static RecyclerView recyclerViewReserve;
	static String[] bookingTime = new String[]{"10:00 AM - 12:00 PM", "04:00 PM - 06:00 PM", "12:00 PM - 02:00 PM", "06:00 PM - 08:00 PM", "02:00 PM - 04:00 PM", "08:00 PM - 09:00 PM"};


	public static void reserveMethod() {

		sortByBtn.setVisibility(View.GONE);
		backBtn.setEnabled(false);

		ArrayList<String> list = new ArrayList<>();
		ArrayList<Boolean> listBool = new ArrayList<>();

		for (int i=0; i<bookingTime.length; i++){

			list.add(bookingTime[i]);
			if (i==0){
				listBool.add(false);
			}else {
				listBool.add(true);
			}
		}

		final AdapterResrvByProduct adapter = new AdapterResrvByProduct(context, list, listBool);

		recyclerViewReserve.setHasFixedSize(true);
		recyclerViewReserve.setLayoutManager(new GridLayoutManager(context, 2));
		recyclerViewReserve.setAdapter(adapter);

		reserveLayout.setVisibility(View.VISIBLE);
		inactiveLayout.setVisibility(View.VISIBLE);

		Calendar c = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
		SimpleDateFormat dateFormatRD = new SimpleDateFormat("yyyy-M-dd");

		Date toD = c.getTime();
		final String currentDateRD = dateFormatRD.format(toD);
		String currentDate = dateFormat.format(toD);
		String today = "<b><font size=20>"+currentDate+"</font></b>";

		editorConfrm.putString(GlobalDataCls.DateReserve, currentDateRD);
		editorConfrm.commit();

		c.add(Calendar.DATE, 1);
		Date nextD = c.getTime();
		String nextDay = dateFormat.format(nextD);
		String tomorrow = "<b><font size=20>"+nextDay+"</font></b>";
		final String nextDayRD = dateFormatRD.format(nextD);

		todayTxtVw = (TextView) view.findViewById(R.id.today_txt_vw);
		todaySmallTxtVw = (TextView) view.findViewById(R.id.today_small_txt_vw);
		todayTxtLine = (TextView) view.findViewById(R.id.today_txt_line);

		tomorrowTxtVw = (TextView) view.findViewById(R.id.tomorrow_txt_vw);
		tomorrowSmallTxtVw = (TextView) view.findViewById(R.id.tomorrow_small_txt_vw);
		tomorrowTxtLine = (TextView) view.findViewById(R.id.tomorrow_txt_line);

		doneBtn = (Button) view.findViewById(R.id.booking_done);
		cancelBtn = (Button) view.findViewById(R.id.booking_cancel);

		todayLayout = (LinearLayout) view.findViewById(R.id.today_layout);
		tomorrowLayout = (LinearLayout) view.findViewById(R.id.tomorrow_layout);

		openTime = (TextView) view.findViewById(R.id.open_time);
		closeTime = (TextView) view.findViewById(R.id.close_time);
		closeBtn = (TextView) view.findViewById(R.id.close_dialog);

		String openT = "<b>Opening time : <font color=#000000>10:00 AM</font</b>";
		openTime.setText(Html.fromHtml(openT));
		String closeT = "<b>Closing time : <font color=#000000>09:00 PM</font</b>";
		closeTime.setText(Html.fromHtml(closeT));

		todayTxtVw.setText(Html.fromHtml(today));
		tomorrowTxtVw.setText(Html.fromHtml(tomorrow));


		closeBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				 backBtn.setEnabled(true);
				sortByBtn.setVisibility(View.VISIBLE);
				reserveLayout.setVisibility(View.GONE);
				inactiveLayout.setVisibility(View.GONE);
			}
		});

		cancelBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				 backBtn.setEnabled(true);
				sortByBtn.setVisibility(View.VISIBLE);
				reserveLayout.setVisibility(View.GONE);
				inactiveLayout.setVisibility(View.GONE);
			}
		});

		doneBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				LocationManager service = (LocationManager) context.getSystemService(LOCATION_SERVICE);
				boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
				// Check if enabled and if not send user to the GPS settings
				if (!enabled) {
			/*Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);*/
					Toast.makeText(context, "Enable Your GPS Location", Toast.LENGTH_SHORT).show();
				}else {
					backBtn.setEnabled(true);

					bookVisitMethod();
				}
			}
		});

		todayLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				editorConfrm.putString(GlobalDataCls.DateReserve, currentDateRD);
				editorConfrm.commit();
				todayTxtVw.setTextColor(Color.parseColor("#FF4949"));
				todaySmallTxtVw.setTextColor(Color.parseColor("#FF4949"));
				todayTxtLine.setVisibility(View.VISIBLE);

				tomorrowTxtVw.setTextColor(Color.parseColor("#888888"));
				tomorrowSmallTxtVw.setTextColor(Color.parseColor("#888888"));
				tomorrowTxtLine.setVisibility(View.INVISIBLE);

				adapter.notifyDataSetChanged();
			}
		});

		tomorrowLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				editorConfrm.putString(GlobalDataCls.DateReserve, nextDayRD);
				editorConfrm.commit();
				todayTxtVw.setTextColor(Color.parseColor("#888888"));
				todaySmallTxtVw.setTextColor(Color.parseColor("#888888"));
				todayTxtLine.setVisibility(View.INVISIBLE);

				tomorrowTxtVw.setTextColor(Color.parseColor("#FF4949"));
				tomorrowSmallTxtVw.setTextColor(Color.parseColor("#FF4949"));
				tomorrowTxtLine.setVisibility(View.VISIBLE);

				adapter.notifyDataSetChanged();
			}
		});
	}

	private static void bookVisitMethod() {

		final Dialog dialog = new Dialog(context);
		dialog.setContentView(R.layout.dialog_category_layout);
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		dialog.setCancelable(false);
		dialog.show();

		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.put("sku", preferences.getString(GlobalDataCls.Sku_ID, null));
			String shopId = preferences.getString(GlobalDataCls.Shop_ID, null);
			jsonObject.put("shop", shopId);
			jsonObject.put("from",preferencesConfrm.getString(GlobalDataCls.DateReserve, null)+"T"+ preferencesConfrm.getString(GlobalDataCls.TimeReserveFrom, null));
			jsonObject.put("to", preferencesConfrm.getString(GlobalDataCls.DateReserve, null)+ "T"+preferencesConfrm.getString(GlobalDataCls.TimeReserveTo, null));
		} catch (JSONException e) {
			e.printStackTrace();
		}

		Log.d("bookingVisitData", jsonObject.toString());

		String url = UrlAll.BookingVisitUrl;
		HashMap<String, String> headers = new HashMap();
		headers.put("Content-Type", "application/json");
		String token = preferences.getString(GlobalDataCls.toKen, null);
		headers.put("Authorization", "Token "+token);

		ApiCallMethods apiCallMethods = new ApiCallMethods(){

			@Override
			public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
				return new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {

						Log.d("bookingCnfrmRes", response.toString());
						dialog.dismiss();

						ResCls resCls = new Gson().fromJson(response.toString(), ResCls.class);
						editor.putInt(GlobalDataCls.reserveID, resCls.getData().getReserveId());

						editor.putString(GlobalDataCls.FragmentName, "FragConfirmVisit");
						editor.commit();


						Intent intent = new Intent(context, ByProductActivity.class);
						context.startActivity(intent);
					}
				};
			}
		};
		apiCallMethods.volleyJSONObjectPOST(context, url, jsonObject, headers, dialog);
	}
}
