package com.hoplett.app.fragments.bystore;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.hoplett.app.R;
import com.hoplett.app.activities.ByStoreActivity;
import com.hoplett.app.classes.GlobalDataCls;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class DialogReserveFragment extends DialogFragment {

	FragTodayByStore fragTodayByStore;
	FragTomorrowByStore fragTomorrowByStore;

	TabLayout tabLayout;

	Button doneBtn, cancelBtn;

	public DialogReserveFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
													 Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_dialog_reserve, container, false);

		Date c = Calendar.getInstance().getTime();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM,yyyy");
		String currentDate = dateFormat.format(c);

		String today = "<b><font size=20>"+currentDate+"</font></b> <br> Today";

		String tomorrow = "<b><font size=20>Sandy</font></b> <br> Today";

		fragTodayByStore = new FragTodayByStore();
		fragTomorrowByStore = new FragTomorrowByStore();

		tabLayout = (TabLayout) view.findViewById(R.id.tabs);
		doneBtn = (Button) view.findViewById(R.id.booking_done);
		cancelBtn = (Button) view.findViewById(R.id.booking_cancel);

		tabLayout.addTab(tabLayout.newTab().setText("One"), true);
		tabLayout.addTab(tabLayout.newTab().setText("Two"));


		tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
			@Override
			public void onTabSelected(TabLayout.Tab tab) {
				setCurrentTabFragment(tab.getPosition());
			}

			@Override
			public void onTabUnselected(TabLayout.Tab tab) {

			}

			@Override
			public void onTabReselected(TabLayout.Tab tab) {

			}
		});


		doneBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SharedPreferences preferences =
								getActivity().getSharedPreferences(GlobalDataCls.PreferencesName,
												Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = preferences.edit();

				editor.putString(GlobalDataCls.FragmentName, "FragConfirmVisit");
				editor.commit();

				Intent intent = new Intent(getContext(), ByStoreActivity.class);
				getActivity().startActivity(intent);
			}
		});

		return view;
	}

	private void setCurrentTabFragment(int position) {

		switch (position) {
			case 0:
				replaceFragment(fragTodayByStore);
				break;
			case 1:
				replaceFragment(fragTomorrowByStore);
				break;
		}
	}
	public void replaceFragment(Fragment fragment) {

		FragmentManager fm = getChildFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(R.id.dialog_reserve_layout, fragment);
		ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		ft.commit();
	}

	@Override
	public void onStart() {
		super.onStart();
		final Dialog dialog = getDialog();
		if (dialog != null) {
			int width = ViewGroup.LayoutParams.MATCH_PARENT;
			int height = ViewGroup.LayoutParams.WRAP_CONTENT;
			dialog.getWindow().setLayout(width, height);

			Window window = dialog.getWindow();
			WindowManager.LayoutParams wlp = window.getAttributes();

			wlp.gravity = Gravity.BOTTOM;
			wlp.flags &= ~WindowManager.LayoutParams.ANIMATION_CHANGED;
			window.setAttributes(wlp);

			dialog.setCancelable(false);

			TextView closeDialog = (TextView) dialog.findViewById(R.id.close_dialog);
			closeDialog.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			cancelBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			FragmentManager fm = getChildFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			ft.add(R.id.dialog_reserve_layout, fragTodayByStore);
		}
	}
}
