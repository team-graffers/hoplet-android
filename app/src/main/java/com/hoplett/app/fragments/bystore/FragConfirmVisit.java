package com.hoplett.app.fragments.bystore;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.*;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.activities.HopletHome;
import com.hoplett.app.classes.CurrentLocation;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.confirmVisitDetail.ConfirmVisitDetail;
import com.squareup.picasso.Picasso;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragConfirmVisit extends Fragment {

    TextView timeDate, timeRemains, nameProduct, nameBrand, sizeProduct, price, storeName, address,
            distance;
    ImageView imgProduct, imgStore;

    ImageButton backBtnCnfm;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public FragConfirmVisit() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_confirm_visit, container, false);

        preferences = getActivity().getSharedPreferences(GlobalDataCls.PreferencesName, Context.MODE_PRIVATE);
        editor = preferences.edit();

        timeDate = (TextView) view.findViewById(R.id.reserved_date_time);
        timeRemains = (TextView) view.findViewById(R.id.time_remain);

        nameProduct = (TextView) view.findViewById(R.id.product_name);
        nameBrand = (TextView) view.findViewById(R.id.brand_name);
        sizeProduct = (TextView) view.findViewById(R.id.size_product);
        price = (TextView) view.findViewById(R.id.price_product);

        storeName = (TextView) view.findViewById(R.id.store_name);
        address = (TextView) view.findViewById(R.id.address);
        distance = (TextView) view.findViewById(R.id.distance);

        imgProduct = (ImageView) view.findViewById(R.id.img_product);
        imgStore = (ImageView) view.findViewById(R.id.img_store);

        backBtnCnfm = (ImageButton) view.findViewById(R.id.back_btn_cnfm);

        confirmLocation();

        backBtnCnfm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences =
                        getActivity().getSharedPreferences(GlobalDataCls.PreferencesName,
                                Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();

                editor.putString(GlobalDataCls.FragmentName, "FragConfirm");
                editor.commit();

                Intent intent = new Intent(getContext(), HopletHome.class);
                getActivity().startActivity(intent);
            }
        });
        return view;
    }

    private void confirmLocation() {

        if (GlobalDataCls.latitude != null) {
            confirmVisitDetail();
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    new CurrentLocation(getActivity()).lattLong();
                    confirmLocation();
                }
            }, 1000);
        }
    }

    private void confirmVisitDetail() {

        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        String url = UrlAll.confirmVisitDetail;
        JSONObject jsonObject = new JSONObject();
        try {
            int resId = preferences.getInt(GlobalDataCls.reserveID, -1);
            jsonObject.put("res_id", resId);
            jsonObject.put("latx", GlobalDataCls.latitude);
            jsonObject.put("longY", GlobalDataCls.longitude);
            Log.d("response_cnfm_json_d", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final HashMap<String, String> headers = new HashMap();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Token " + preferences.getString(GlobalDataCls.toKen, null));

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                dialog.dismiss();
                Log.d("response_cnfm_visit", response.toString());
                ConfirmVisitDetail visitDetail = new Gson().fromJson(response.toString(), ConfirmVisitDetail.class);

                nameProduct.setText(visitDetail.getProductDetail().getName());
                nameBrand.setText(visitDetail.getProductDetail().getBrandName());
                sizeProduct.setText("Size : " + visitDetail.getProductDetail().getSize());
                price.setText(String.valueOf(visitDetail.getProductDetail().getPrice()));

                storeName.setText(visitDetail.getStoreDetail().getName());
                address.setText(visitDetail.getStoreDetail().getAddress());
                distance.setText(String.valueOf(new DecimalFormat("###.##").format(visitDetail.getStoreDetail().getDistance()) + "km away"));

                String imgg = visitDetail.getStoreDetail().getImage();
                if (imgg != null) {
                    Picasso.with(getActivity()).load(imgg).placeholder(R.drawable.loading_default_image).error(R.drawable.default_img_back).into(imgStore);
                 //   imgStore.setImageResource(R.drawable.levis);
                }

                String imgStr = visitDetail.getProductDetail().getImage();
                if (imgStr != null && imgStr != "" && imgStr != "null") {

                    String img = imgStr.substring(0, imgStr.length() - 4) + "raw=1";
                    Picasso.with(getActivity()).load(img).into(imgProduct);
                    /*Bitmap bitmap = ((BitmapDrawable) imgProduct.getDrawable()).getBitmap();
                    Bitmap mbitmap*//* = ((BitmapDrawable) getActivity().getResources().getDrawable(R.drawable.default_img_back)).getBitmap()*//*;
                    mbitmap = bitmap;
                    Bitmap imageRounded = Bitmap.createBitmap(mbitmap.getWidth(), mbitmap.getHeight(), mbitmap.getConfig());
                    Canvas canvas = new Canvas(imageRounded);
                    Paint mpaint = new Paint();
                    mpaint.setAntiAlias(true);
                    mpaint.setShader(new BitmapShader(mbitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
                    canvas.drawRoundRect((new RectF(0, 0, mbitmap.getWidth(), mbitmap.getHeight())), 30, 30,
                            mpaint);// Round Image Corner 100 100 100 100
                    imgProduct.setImageBitmap(imageRounded);*/

                } else {
                }

                setVisitingDateandTime(visitDetail.getTime());
                showRemainTimeForVisit(visitDetail.getTime());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
                Log.d("response_cnfm_visit", error.toString());
                volleyErrMethod(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        // Add the request to the RequestQueue.
        queue.add(request);
    }

    private void setVisitingDateandTime(String time) {

        // time : 2019-06-11T18:00:00
        Calendar calendar = Calendar.getInstance();

        Date date = calendar.getTime();

        SimpleDateFormat dateFormatRD = new SimpleDateFormat("yyyy-M-dd");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");

        String[] prefDate = time.split("T");

        String dd = dateFormatRD.format(date);

        String[] dateDD = prefDate[0].split("-");
        int d = Integer.parseInt(dateDD[2]);

        String day;

        if (d == 1 || d == 21 || d == 31) {
            day = String.valueOf(d) + "st";
        } else if (d == 2 || d == 22) {
            day = String.valueOf(d) + "nd";
        } else if (d == 3 || d == 23) {
            day = String.valueOf(d) + "rd";
        } else {
            day = String.valueOf(d) + "th";
        }

        Date d11 = null;
        Date d22 = null;
        Date d33 = null;

        try {
            d33 = new SimpleDateFormat("yyyy-MM-dd").parse(prefDate[0]);
            String d3333 = dateFormatRD.format(d33);
            d11 = dateFormatRD.parse(dd);
            d22 = dateFormatRD.parse(d3333);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] d2 = dateFormat.format(d22).split(" ");
        String dateTimeAndDay;

        Date d12;
        String d13 = "";
        try {
            d12 = new SimpleDateFormat("HH:mm:ss").parse(prefDate[1]);
            d13 = new SimpleDateFormat("hh:mm aa").format(d12);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (d22.compareTo(d11) == 0) {

            dateTimeAndDay = "Today, " + day + " " + d2[1] + " at " + d13 + ".";
            timeDate.setText(dateTimeAndDay);
        } else {

            dateTimeAndDay = "Tomorrow, " + day + " " + d2[1] + " at " + d13 + ".";
            timeDate.setText(dateTimeAndDay);
        }
    }

    /**
     * Remaining Time
     *
     * @param time
     */
    private void showRemainTimeForVisit(final String time) {

        //  time : 2019-06-11T18:00:00

        SimpleDateFormat currentDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date dateCurrentFormat = null;
        try {
            dateCurrentFormat = currentDateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();

        Date date = calendar.getTime();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-M-dd HH:mm:ss");

        String startDate = simpleDateFormat.format(date);
        String stopDate = simpleDateFormat.format(dateCurrentFormat);

        Date dateStart = null;
        Date dateStop = null;

        try {
            dateStart = simpleDateFormat.parse(startDate);
            dateStop = simpleDateFormat.parse(stopDate);

            //in milliseconds
            long diff = dateStop.getTime() - dateStart.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            String remainTime = String.valueOf(diffDays) + "d:" + String.valueOf(diffHours) + "h:" + String.valueOf(diffMinutes) + "m"/*+String.valueOf(diffSeconds)*/;

            timeRemains.setText(remainTime);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showRemainTimeForVisit(time);
                }
            }, 1000);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void volleyErrMethod(VolleyError error) {

        if (error.networkResponse != null) {
            Log.d("response_cnfm_visit2", String.valueOf(error.networkResponse.statusCode));
        } else {
//			 Toast.makeText(context, "Please check your Internet Connection..!!", Toast.LENGTH_SHORT).show();
            Log.d("response_cnfm_visit7", "Please check your Internet Connection..!!");
        }

        NetworkResponse response = error.networkResponse;
        if (error instanceof ServerError && response != null) {
            try {
                String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));

                // Now you can use any deserializer to make sense of data
                JSONObject obj = new JSONObject(res);

                Log.d("response_cnfm_visit3", obj.toString());
            } catch (UnsupportedEncodingException e1) {
                // Couldn't properly decode data to string
                e1.printStackTrace();
            } catch (JSONException e2) {
                // returned data is not JSONObject?
                e2.printStackTrace();
            }
        } else {
//			 Toast.makeText(context, "Check Your Internet Connection and try again", Toast.LENGTH_SHORT).show();
        }
    }
}
