package com.hoplett.app.fragments.bystore;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.android.volley.Response;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.activities.ByStoreActivity;
import com.hoplett.app.activities.HopletHome;
import com.hoplett.app.activities.SignInActivity;
import com.hoplett.app.activities.WishListActivity;
import com.hoplett.app.adapters.AdapterSlidingImg;
import com.hoplett.app.adapters.byStore.AdapterProSizeStore;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.byProductShopping.productDetail.ProductDetail;
import com.hoplett.app.pojoClasses.res_cls.ResCls;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static android.content.Context.LOCATION_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragProductDetailByStore extends Fragment {

    ImageButton backBtn;
    public static ImageView imgProduct, imgPriceOld;
    public static TextView nameBrand, nameProduct, price, oldPrice, discount;
    RadioGroup radioGroup;
    Button reserveBtn, addToWishlist;

    RecyclerView recyclerViewSize;

    SharedPreferences preferences, confrmPrefernces;
    SharedPreferences.Editor editor, editorConfrm;
    View view;
    LinearLayout reserveLayout, inactiveLayout;

    private static ViewPager viewPager;
    private static int currentPage=0;
    private static int NUM_PAGES = 0;

    public FragProductDetailByStore() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_product_detail_by_store, container, false);

        preferences = getActivity().getSharedPreferences(GlobalDataCls.PreferencesName, Context.MODE_PRIVATE);
        editor = preferences.edit();

        confrmPrefernces = getActivity().getSharedPreferences(GlobalDataCls.confrmPreferences, Context.MODE_PRIVATE);
        editorConfrm = confrmPrefernces.edit();

        backBtn = (ImageButton) view.findViewById(R.id.back_btn);

        imgPriceOld = (ImageView) view.findViewById(R.id.old_price_img);
        nameBrand = (TextView) view.findViewById(R.id.brand_name);
        nameProduct = (TextView) view.findViewById(R.id.product_name);
        price = (TextView) view.findViewById(R.id.price_product);
        oldPrice = (TextView) view.findViewById(R.id.old_price_product);
        discount = (TextView) view.findViewById(R.id.price_discount);

        viewPager = (ViewPager) view.findViewById(R.id.viewpager);

//        radioGroup = (RadioGroup) view.findViewById(R.id.radio_grp);

        reserveBtn = (Button) view.findViewById(R.id.reserve_btn);
        addToWishlist = (Button) view.findViewById(R.id.add_to_wishlist_btn);

        reserveLayout = (LinearLayout) view.findViewById(R.id.reserve_layout);
        inactiveLayout = (LinearLayout) view.findViewById(R.id.inactive_layout);

        reserveLayout.setVisibility(View.GONE);
        inactiveLayout.setVisibility(View.GONE);

        recyclerViewSize = (RecyclerView) view.findViewById(R.id.recycler_vw_size);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ByStoreActivity activity = (ByStoreActivity) getActivity();
                activity.startProductListFragment();
            }
        });

        reserveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (preferences.getString(GlobalDataCls.toKen, null) != null) {
                    backBtn.setEnabled(false);
                    reserveBtnMethod();
                } else {
                    editor.putString(GlobalDataCls.SignInActivityName, "ByStoreActivity");
                    editor.putString(GlobalDataCls.SignInFragName, "FragProductDetailByStore");
                    editor.commit();

                    Intent intent = new Intent(getContext(), SignInActivity.class);
                    getActivity().startActivity(intent);
                }
            }
        });

        addToWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (preferences.getString(GlobalDataCls.toKen, null) != null) {

                    if (addToWishlist.getText().toString().equals("WishListed")) {
                        getActivity().startActivity(new Intent(getContext(), WishListActivity.class));
                    } else {
                        //         addToWishlist.setText("Add to Wishlist");
                        addToWishListMethod();
                    }
                } else {
                    getActivity().startActivity(new Intent(getContext(), SignInActivity.class));
                }
            }
        });

        if (GlobalDataCls.favoriteItemList.size() > 0) {
            for (int i = 0; i < GlobalDataCls.favoriteItemList.size(); i++) {

                if (preferences.getString(GlobalDataCls.Sku_ID, null).equals(GlobalDataCls.favoriteItemList.get(i))) {

                    addToWishlist.setText("WishListed");
                    break;
                }
            }
        }

        fragTodayByStore = new FragTodayByStore();
        fragTomorrowByStore = new FragTomorrowByStore();

        ft = getChildFragmentManager().beginTransaction();
        ft.add(R.id.dialog_reserve_layout, fragTodayByStore);
        ft.commit();


        Calendar c = Calendar.getInstance();

        SimpleDateFormat dateFormatRD = new SimpleDateFormat("yyyy-MM-dd");

        Date toD = c.getTime();

        final String currentDateRD = dateFormatRD.format(toD);

        editor.putString(GlobalDataCls.DateReserve, currentDateRD);
        editor.commit();

        editorConfrm.putString(GlobalDataCls.DateReserve, currentDateRD);
        editorConfrm.commit();

        proDetailMethod();


        if (preferences.getString(GlobalDataCls.SignInFragName, null) != null) {

            editor.remove(GlobalDataCls.SignInFragName);
            editor.commit();
            reserveBtnMethod();
        }

        return view;
    }

    private void addToWishListMethod() {

        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        String pro_id = preferences.getString(GlobalDataCls.ProductId, null);
        String sku_id = preferences.getString(GlobalDataCls.Sku_ID, null);
        String url = UrlAll.WishlistAddUrl + pro_id+"&sku_id="+sku_id;
        HashMap headers = new HashMap();
        headers.put("Authorization", "Token " + preferences.getString(GlobalDataCls.toKen, null));


        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        dialog.dismiss();
                        addToWishlist.setText("WishListed");
                        GlobalDataCls.FavoriteCounter++;
                        HopletHome.favoriteCount.setText(String.valueOf(GlobalDataCls.FavoriteCounter));
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(getContext(), url, headers, dialog);
    }

    String skuId;

    private void proDetailMethod() {

        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        String url = UrlAll.ProductDetailUrl + preferences.getString(GlobalDataCls.ProductId, null);

        HashMap headers = new HashMap();
        headers.put("Content-Type", "application/json");

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Gson gson = new Gson();

                        ProductDetail productDetail = gson.fromJson(response.toString(), ProductDetail.class);

                        if (productDetail.getData() != null) {

                            ArrayList<String> ImagesArray = new ArrayList<>();

                            for (int i=0; i<productDetail.getData().getImage().size() && i<4; i++){

                                String imgStr = productDetail.getData().getImage().get(i).getName();

                                if (imgStr != null && imgStr.length() > 0 && imgStr != "null") {

                                    String img = imgStr.substring(0, imgStr.length() - 4) + "raw=1";
                                    ImagesArray.add(img);
                                }
                            }

                            viewPager.setAdapter(new AdapterSlidingImg(getActivity(), ImagesArray, currentPage));

                            viewPager.setCurrentItem(currentPage,true);
                            CirclePageIndicator indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);

                            indicator.setViewPager(viewPager);

                            final float density = getResources().getDisplayMetrics().density;

                            //Set circle indicator radius
                            indicator.setRadius(3 * density);

                            NUM_PAGES =ImagesArray.size();

                            // Pager listener over indicator
                            indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                @Override
                                public void onPageSelected(int position) {
                                    currentPage = position;
                                }
                                @Override
                                public void onPageScrolled(int pos, float arg1, int arg2) {
                                }
                                @Override
                                public void onPageScrollStateChanged(int pos) {
                                }
                            });















                            editorConfrm.putString(GlobalDataCls.confrmProName, productDetail.getData().getProductName());
                            editorConfrm.putString(GlobalDataCls.confrmBrandName, productDetail.getData().getBrand());
                            editorConfrm.putString(GlobalDataCls.confrmPrice, preferences.getString(GlobalDataCls.PriceNew, null));
                            editorConfrm.commit();


                            nameBrand.setText(productDetail.getData().getBrand());
                            nameProduct.setText(productDetail.getData().getProductName());

                            /*price.setText(preferences.getString(GlobalDataCls.PriceNew, null));

                            int priceNew = Integer.parseInt(preferences.getString(GlobalDataCls.PriceNew, null));
                            int priceOld = Integer.parseInt(preferences.getString(GlobalDataCls.PriceOld, null));
                            String discPrice = String.valueOf(100 - Math.round(priceNew * 100 / priceOld));

                            if (discPrice.equals("0")){
                                discount.setVisibility(View.GONE);
                                oldPrice.setVisibility(View.GONE);
                                imgPriceOld.setVisibility(View.GONE);
                            }else {
                                String oldP = "<del>" + preferences.getString(GlobalDataCls.PriceOld, null) + "</del>";
                                oldPrice.setText(Html.fromHtml(oldP));
                                String discnt = "(<font color=#F4684E>" + discPrice + "%Off</font>)";
                                discount.setText(Html.fromHtml(discnt));
                            }*/

                            if (productDetail.getData().getSize() != null && productDetail.getData().getSize().size() > 0 && !productDetail.getData().getSize().isEmpty()) {

                                AdapterProSizeStore adaptrProductSize = new AdapterProSizeStore(getContext(), productDetail);

                                recyclerViewSize.setHasFixedSize(true);
                                recyclerViewSize.setLayoutManager(new LinearLayoutManager(getContext(),
                                        LinearLayoutManager.HORIZONTAL, false));
                                recyclerViewSize.setAdapter(adaptrProductSize);
                                skuId = productDetail.getData().getSize().get(0).getSkuId().toString();
                            }
                        }

                        dialog.dismiss();
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(getContext(), url, headers, dialog);
    }


    FragTodayByStore fragTodayByStore;
    FragTomorrowByStore fragTomorrowByStore;

    Button doneBtn, cancelBtn;
    FragmentTransaction ft;

    LinearLayout todayLayout, tomorrowLayout;
    TextView openTime, closeTime, closeBtn;
    TextView todayTxtVw, todaySmallTxtVw, todayTxtLine, tomorrowTxtVw, tomorrowSmallTxtVw, tomorrowTxtLine;

    public void reserveBtnMethod() {

        reserveLayout.setVisibility(View.VISIBLE);
        inactiveLayout.setVisibility(View.VISIBLE);

        Calendar c = Calendar.getInstance();

        SimpleDateFormat dateFormatRD = new SimpleDateFormat("yyyy-M-dd");

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");

        Date toD = c.getTime();

        final String currentDateRD = dateFormatRD.format(toD);
        String currentDate = dateFormat.format(toD);

        String today = "<b><font size=20>" + currentDate + "</font></b>";

        editor.putString(GlobalDataCls.DateReserve, currentDateRD);
        editor.commit();
        editorConfrm.putString(GlobalDataCls.DateReserve, currentDateRD);
        editorConfrm.commit();

        c.add(Calendar.DATE, 1);
        Date nextD = c.getTime();
        String nextDay = dateFormat.format(nextD);

        final String nextDayRD = dateFormatRD.format(nextD);

        String tomorrow = "<b><font size=20>" + nextDay + "</font></b>";

        todayTxtVw = (TextView) view.findViewById(R.id.today_txt_vw);
        todaySmallTxtVw = (TextView) view.findViewById(R.id.today_small_txt_vw);
        todayTxtLine = (TextView) view.findViewById(R.id.today_txt_line);

        tomorrowTxtVw = (TextView) view.findViewById(R.id.tomorrow_txt_vw);
        tomorrowSmallTxtVw = (TextView) view.findViewById(R.id.tomorrow_small_txt_vw);
        tomorrowTxtLine = (TextView) view.findViewById(R.id.tomorrow_txt_line);

        doneBtn = (Button) view.findViewById(R.id.booking_done);
        cancelBtn = (Button) view.findViewById(R.id.booking_cancel);

        todayLayout = (LinearLayout) view.findViewById(R.id.today_layout);
        tomorrowLayout = (LinearLayout) view.findViewById(R.id.tomorrow_layout);

        openTime = (TextView) view.findViewById(R.id.open_time);
        closeTime = (TextView) view.findViewById(R.id.close_time);
        closeBtn = (TextView) view.findViewById(R.id.close_dialog);

        String openT = "<b>Opening time : <font color=#000000>10:00 AM</font</b>";
        openTime.setText(Html.fromHtml(openT));
        String closeT = "<b>Closing time : <font color=#000000>09:00 PM</font</b>";
        closeTime.setText(Html.fromHtml(closeT));

        todayTxtVw.setText(Html.fromHtml(today));
        tomorrowTxtVw.setText(Html.fromHtml(tomorrow));


        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backBtn.setEnabled(true);
                reserveLayout.setVisibility(View.GONE);
                inactiveLayout.setVisibility(View.GONE);
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backBtn.setEnabled(true);
                reserveLayout.setVisibility(View.GONE);
                inactiveLayout.setVisibility(View.GONE);
            }
        });

        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LocationManager service = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
                boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

                // Check if enabled and if not send user to the GPS settings
                if (!enabled) {
			/*Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);*/
                    Toast.makeText(getActivity(), "Enable Your GPS Location", Toast.LENGTH_SHORT).show();
                }else {
                    backBtn.setEnabled(true);
                    bookVisitMethod();
                }
            }
        });

        todayLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putString(GlobalDataCls.DateReserve, currentDateRD);
                editor.commit();
                editorConfrm.putString(GlobalDataCls.DateReserve, currentDateRD);
                editorConfrm.commit();

                todayTxtVw.setTextColor(Color.parseColor("#FF4949"));
                todaySmallTxtVw.setTextColor(Color.parseColor("#FF4949"));
                todayTxtLine.setVisibility(View.VISIBLE);

                tomorrowTxtVw.setTextColor(Color.parseColor("#888888"));
                tomorrowSmallTxtVw.setTextColor(Color.parseColor("#888888"));
                tomorrowTxtLine.setVisibility(View.INVISIBLE);

                ft = getChildFragmentManager().beginTransaction();
                ft.replace(R.id.dialog_reserve_layout, fragTodayByStore);
                ft.commit();
            }
        });

        tomorrowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putString(GlobalDataCls.DateReserve, nextDayRD);
                editor.commit();
                editorConfrm.putString(GlobalDataCls.DateReserve, nextDayRD);
                editorConfrm.commit();

                todayTxtVw.setTextColor(Color.parseColor("#888888"));
                todaySmallTxtVw.setTextColor(Color.parseColor("#888888"));
                todayTxtLine.setVisibility(View.INVISIBLE);

                tomorrowTxtVw.setTextColor(Color.parseColor("#FF4949"));
                tomorrowSmallTxtVw.setTextColor(Color.parseColor("#FF4949"));
                tomorrowTxtLine.setVisibility(View.VISIBLE);

                ft = getChildFragmentManager().beginTransaction();
                ft.replace(R.id.dialog_reserve_layout, fragTomorrowByStore);
                ft.commit();
            }
        });
    }

    private void bookVisitMethod() {

        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("sku", preferences.getString(GlobalDataCls.Sku_ID, null));
            jsonObject.put("shop", preferences.getString(GlobalDataCls.Shop_ID, null));
            jsonObject.put("from", preferences.getString(GlobalDataCls.DateReserve, null) + "T" + preferences.getString(GlobalDataCls.TimeReserveFrom, null));
            jsonObject.put("to", preferences.getString(GlobalDataCls.DateReserve, null) + "T" + preferences.getString(GlobalDataCls.TimeReserveTo, null));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("bookingVisitData", jsonObject.toString());

        String url = UrlAll.BookingVisitUrl;
        HashMap<String, String> headers = new HashMap();
		headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Token " + preferences.getString(GlobalDataCls.toKen, null));

        ApiCallMethods apiCallMethods = new ApiCallMethods() {

            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("bookingCnfrmRes", response.toString());
                        ResCls resCls = new Gson().fromJson(response.toString(), ResCls.class);
                        editor.putInt(GlobalDataCls.reserveID, resCls.getData().getReserveId());
                        editor.commit();


                        dialog.dismiss();
                        ByStoreActivity activity = (ByStoreActivity) getActivity();
                        activity.startVisitCnfmFrag();
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectPOST(getContext(), url, jsonObject, headers, dialog);
    }

}
