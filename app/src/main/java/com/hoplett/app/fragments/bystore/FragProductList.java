package com.hoplett.app.fragments.bystore;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.*;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.activities.ByProductActivity;
import com.hoplett.app.activities.HopletHome;
import com.hoplett.app.adapters.byStore.AdaptrProductByStore;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.FavoriteItemsCls;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.byProductShopping.productList.ProductListByProductItems;
import com.hoplett.app.pojoClasses.byProductShopping.productList.ResultList;

import com.hoplett.app.pojoClasses.byStoreShopping.proList.ProList;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.LOCATION_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragProductList extends Fragment {

    Toolbar toolbar;
    TextView titleToolbar, subTitleToolbar;
    ImageButton backBtn;


    View view;
    RecyclerView recyclerView;
    AdaptrProductByStore adapter;
    Dialog dialog;
    Context context;

    private boolean isScrolling = false, isPaginate = false;
    private int currentItems, totalItems, scrollOutItems;
    GridLayoutManager layoutManager;
    ProgressBar progressLoader;
    String nextPage, previousPage;

    SwipeRefreshLayout swipeRefreshLayout55;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    ArrayList<Boolean> listBool;
    private List<ResultList> listProducts;
    private ProductListByProductItems productListByProductItems;
    Button sortByBtn, filterBtn;
    String url;
    HashMap headers;

    LinearLayout deptLayout, storeLayout;
    public static TextView favoriteCount;
    LinearLayout noProducts;
    String shopID;

    public FragProductList() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_product_list, container, false);

        context = getContext();

        preferences = getActivity().getSharedPreferences(GlobalDataCls.PreferencesName, Context.MODE_PRIVATE);
        editor = preferences.edit();

        swipeRefreshLayout55 = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout_swipe55);

        noProducts = (LinearLayout) view.findViewById(R.id.no_product_layout);

        progressLoader = (ProgressBar) view.findViewById(R.id.progress_loader);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        titleToolbar = (TextView) toolbar.findViewById(R.id.toolbar_title_txt_vw);
        subTitleToolbar = (TextView) toolbar.findViewById(R.id.toolbar_subtitle_txt_vw);

        String brandName = preferences.getString(GlobalDataCls.BrandName, null);
        String noOfProducts = preferences.getString(GlobalDataCls.NoOfProducts, null);

        titleToolbar.setText(brandName);
        subTitleToolbar.setText(noOfProducts);

        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);

        favoriteCount = (TextView) view.findViewById(R.id.favorite_dot);

        backBtn = (ImageButton) view.findViewById(R.id.back_btn);
        deptLayout = (LinearLayout) view.findViewById(R.id.progress_dept_lay);
        storeLayout = (LinearLayout) view.findViewById(R.id.progress_store_lay);
        sortByBtn = (Button) view.findViewById(R.id.sort_by_btn);
        filterBtn = (Button) view.findViewById(R.id.filter_btn);

        if (GlobalDataCls.FavoriteCounter > 0) {
            favoriteCount.setText(String.valueOf(GlobalDataCls.FavoriteCounter));
        }

        int itemList[] = new int[]{R.drawable.default_img_back, R.drawable.default_img_back,
                R.drawable.default_img_back, R.drawable.default_img_back, R.drawable.default_img_back};

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString(GlobalDataCls.FragmentName, "FragCategoryByProduct");
                editor.putString(GlobalDataCls.ByStore, "ByStore");
                editor.commit();

                Intent intent = new Intent(getContext(), HopletHome.class);
                getActivity().startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (preferences.getString(GlobalDataCls.toKen, null) != null) {
            new FavoriteItemsCls(getContext());
        }

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_vw_product_list_by_store);

        GlobalDataCls.listProductByProduct = new ArrayList<>();

        shopID = preferences.getString(GlobalDataCls.Shop_ID, null);

        headers = new HashMap();
        if (preferences.getString(GlobalDataCls.toKen, null) != null) {
            if (GlobalDataCls.boolApplyFilter){
                url = UrlAll.filterProduct_Token;
            }else {
                url = UrlAll.ProductRelatedToShopUrl + shopID + "&department=" + preferences.getString(GlobalDataCls.DepartmentId, null);
            }
            headers.put("Content-Type", "application/json");
            String token = preferences.getString(GlobalDataCls.toKen, null);
            headers.put("Authorization", "Token " + token);
        } else {
            if (GlobalDataCls.boolApplyFilter){
                url = UrlAll.filterProduct;
            }else {
                url = UrlAll.ProductRelatedToShopUrl2 + shopID + "&department=" + preferences.getString(GlobalDataCls.DepartmentId, null);
            }
            headers.put("Content-Type", "application/json");
        }

        listBool = new ArrayList<>();
        listProducts = new ArrayList<>();
        productListByProductItems = new ProductListByProductItems();

        adapter = new AdaptrProductByStore(getContext(), listProducts, listBool);

        layoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        LocationManager service = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // Check if enabled and if not send user to the GPS settings
        if (!enabled) {
			/*Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);*/
            Toast.makeText(context, "Enable Your GPS Location", Toast.LENGTH_SHORT).show();
        }else {

            sortByBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sortByMethod();
                }
            });

            filterBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    filterMethod();
                }
            });

            swipeRefreshLayout55.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    onStart();
                }
            });

            proListMethod(url);

            paginationMethod();
        }
    }

    private void paginationMethod() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems + 2 == totalItems)) {

                    isScrolling = false;
                    String ss = nextPage;
                    if (ss != null) {

                        progressLoader.setLayoutParams(new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.MATCH_PARENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM));

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressLoader.setVisibility(View.GONE);
                                isPaginate = true;
                                proListMethod(nextPage);
                            }
                        }, 300);
                    }
                }
            }
        });
    }

    private void proListMethod(String url) {

        if (!swipeRefreshLayout55.isRefreshing()) {
            if (isPaginate){
                isPaginate = false;
            }else {dialog.show();}
        }

        int method;
        if (GlobalDataCls.boolApplyFilter){
            method = Request.Method.POST;
            try {
                GlobalDataCls.jsonObject.put("shop_id", Integer.parseInt(shopID));
                Log.d("json_filter_sh", GlobalDataCls.jsonObject.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            method = Request.Method.GET;
            GlobalDataCls.jsonObject = null;
        }

        //     url = "http://52.66.151.82:8001/prod_in_shop?shop=" + shopID;

        RequestQueue queue = Volley.newRequestQueue(getContext());
        JsonObjectRequest request = new JsonObjectRequest(method, url, GlobalDataCls.jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (swipeRefreshLayout55.isRefreshing()) {
                    swipeRefreshLayout55.setRefreshing(false);
                }

                Log.d("response_pro_list", response.toString());
                try {
                    if (response.getInt("count") > 0) {

                        ProList proList = new Gson().fromJson(response.toString(), ProList.class);

                        ResultList resultList;
                        if (proList.getNext() != null && proList.getNext() != "null" && proList.getNext().length() > 0) {
                            nextPage = proList.getNext();
                        } else {
                            nextPage = null;
                        }

                        for (int i = 0; i < proList.getResults().size(); i++) {

                            listBool.add(proList.getResults().get(i).getFav());
                            resultList = new ResultList();

                            resultList.setBrand(proList.getResults().get(i).getBrand());
                            resultList.setProduct(proList.getResults().get(i).getProduct());
                            resultList.setProductId(proList.getResults().get(i).getProductId());
                            resultList.setImage(proList.getResults().get(i).getImage());
                            resultList.setShopCount(0);
                            resultList.setPrice(proList.getResults().get(i).getPrice());
                            resultList.setDiscPrice(String.valueOf(proList.getResults().get(i).getDiscPrice()));
                            resultList.setSkuId(proList.getResults().get(i).getSkuId());

                            listProducts.add(resultList);
                        }

                        productListByProductItems.setCount(proList.getCount());
                        productListByProductItems.setNext(proList.getNext());
                        productListByProductItems.setPrevious(proList.getPrevious());

                        productListByProductItems.setResults(listProducts);

                        adapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getContext(), "Product not available", Toast.LENGTH_SHORT).show();
                        noProducts.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (dialog.isShowing())
                    dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();

                Log.d("response_product", error.toString());

                new ApiCallMethods().volleyErrMethod(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return headers;
            }
        };
        queue.add(request);
    }

    private void initializeRecyclrVw() {

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_vw_product_list_by_store);

        /**
         *  Product list by sort
         * **/
        listBool = new ArrayList<>();
        listProducts = new ArrayList<>();

        adapter = new AdaptrProductByStore(getContext(), listProducts, listBool);
        layoutManager = new GridLayoutManager(getContext(), 2);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void sortByMethod() {
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_sort_by_layout);

        Button btnWhatsNew = (Button) dialog.findViewById(R.id.whats_new_btn);
        Button btnPriceHighToLow = (Button) dialog.findViewById(R.id.price_high_to_low_btn);
        Button btnPriceLowToHigh = (Button) dialog.findViewById(R.id.price_low_to_high_btn);
        Button btnDiscount = (Button) dialog.findViewById(R.id.discount_btn);
        TextView cancelDialog = (TextView) dialog.findViewById(R.id.cancle_dialog);
        cancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnWhatsNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                initializeRecyclrVw();

                dialog.dismiss();

                headers = new HashMap();
                if (preferences.getString(GlobalDataCls.toKen, null) != null){
                    if (GlobalDataCls.boolApplyFilter){
                        url = UrlAll.filterProduct_Token+ "?filter=whatsnew";
                    }else {
                        url = UrlAll.ProductRelatedToShopUrl + shopID + "&filter=whatsnew&department=" + preferences.getString(GlobalDataCls.DepartmentId, null);
                    }
                    headers.put("Content-Type", "application/json");
                    String token = preferences.getString(GlobalDataCls.toKen, null);
                    headers.put("Authorization", "Token " + token);
                }else {
                    if (GlobalDataCls.boolApplyFilter){
                        url = UrlAll.filterProduct+ "?filter=whatsnew";
                    }else {
                        url = UrlAll.ProductRelatedToShopUrl2 + shopID + "&filter=whatsnew&department=" + preferences.getString(GlobalDataCls.DepartmentId, null);
                    }
                    headers.put("Content-Type", "application/json");
                }
                proListMethod(url);
            }
        });
        btnPriceLowToHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                initializeRecyclrVw();
                dialog.dismiss();
                headers = new HashMap();
                if (preferences.getString(GlobalDataCls.toKen, null) != null){
                    if (GlobalDataCls.boolApplyFilter){
                        url = UrlAll.filterProduct_Token+ "?filter=lowhigh";
                    }else {
                        url = UrlAll.ProductRelatedToShopUrl + shopID + "&filter=lowhigh&department=" + preferences.getString(GlobalDataCls.DepartmentId, null);
                    }
                    headers.put("Content-Type", "application/json");
                    String token = preferences.getString(GlobalDataCls.toKen, null);
                    headers.put("Authorization", "Token " + token);
                }else {
                    if (GlobalDataCls.boolApplyFilter){
                        url = UrlAll.filterProduct+ "?filter=lowhigh";
                    }else {
                        url = UrlAll.ProductRelatedToShopUrl2 + shopID + "&filter=lowhigh&department=" + preferences.getString(GlobalDataCls.DepartmentId, null);
                    }
                    headers.put("Content-Type", "application/json");
                }
                proListMethod(url);
            }
        });
        btnPriceHighToLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                initializeRecyclrVw();
                dialog.dismiss();
                headers = new HashMap();
                if (preferences.getString(GlobalDataCls.toKen, null) != null){
                    if (GlobalDataCls.boolApplyFilter){
                        url = UrlAll.filterProduct_Token+ "?filter=highlow";
                    }else {
                        url = UrlAll.ProductRelatedToShopUrl + shopID + "&filter=highlow&department=" + preferences.getString(GlobalDataCls.DepartmentId, null);
                    }
                    headers.put("Content-Type", "application/json");
                    String token = preferences.getString(GlobalDataCls.toKen, null);
                    headers.put("Authorization", "Token " + token);
                }else {
                    if (GlobalDataCls.boolApplyFilter){
                        url = UrlAll.filterProduct+ "?filter=highlow";
                    }else {
                        url = UrlAll.ProductRelatedToShopUrl2 + shopID + "&filter=highlow&department=" + preferences.getString(GlobalDataCls.DepartmentId, null);
                    }
                    headers.put("Content-Type", "application/json");
                }

                proListMethod(url);
            }
        });
        btnDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                initializeRecyclrVw();
                dialog.dismiss();
                headers = new HashMap();
                if (preferences.getString(GlobalDataCls.toKen, null) != null){
                    if (GlobalDataCls.boolApplyFilter){
                        url = UrlAll.filterProduct_Token+ "?filter=discount";
                    }else {
                        url = UrlAll.ProductRelatedToShopUrl + shopID + "&filter=discount&department=" + preferences.getString(GlobalDataCls.DepartmentId, null);
                    }
                    headers.put("Content-Type", "application/json");
                    String token = preferences.getString(GlobalDataCls.toKen, null);
                    headers.put("Authorization", "Token " + token);
                }else {
                    if (GlobalDataCls.boolApplyFilter){
                        url = UrlAll.filterProduct+ "?filter=discount";
                    }else {
                        url = UrlAll.ProductRelatedToShopUrl2 + shopID + "&filter=discount&department=" + preferences.getString(GlobalDataCls.DepartmentId, null);
                    }
                    headers.put("Content-Type", "application/json");
                }
                proListMethod(url);
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    private void filterMethod() {

        editor.putString(GlobalDataCls.FragmentName, "FragFilter");
        editor.commit();
        Intent intent = new Intent(getContext(), ByProductActivity.class);
        getActivity().startActivity(intent);
    }

}
