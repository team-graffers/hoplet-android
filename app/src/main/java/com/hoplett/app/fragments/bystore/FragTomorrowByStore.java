package com.hoplett.app.fragments.bystore;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hoplett.app.R;
import com.hoplett.app.adapters.byStore.AdaptrResrvToday;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragTomorrowByStore extends Fragment {

	RecyclerView recyclerView;
	 String[] bookingTime = new String[]{"10:00 AM - 12:00 PM", "04:00 PM - 06:00 PM", "12:00 PM - " +
			 "02:00 PM", "06:00 PM - 08:00 PM", "02:00 PM - 04:00 PM", "08:00 PM - 09:00 PM"};

	public FragTomorrowByStore() {
		// Required empty public constructor
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
													 Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_tomorrow_by_store, container, false);

		recyclerView = (RecyclerView) view.findViewById(R.id.reserve_tomorrow_recycle_vw);

		 ArrayList<String> list = new ArrayList<>();
		 ArrayList<Boolean> listBool = new ArrayList<>();
		 for (int i=0; i<bookingTime.length; i++){
				list.add(bookingTime[i]);
				if (i == 0){
					 listBool.add(false);
				}else {
					 listBool.add(true);
				}
		 }

		 AdaptrResrvToday adapter = new AdaptrResrvToday(getContext(), list, listBool);

		 recyclerView.setHasFixedSize(true);
		 recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
		 recyclerView.setAdapter(adapter);

		return view;
	}

}
