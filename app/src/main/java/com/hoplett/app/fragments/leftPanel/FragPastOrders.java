package com.hoplett.app.fragments.leftPanel;


import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.Response;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.adapters.leftPanel.AdaptrPastOrders;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.leftPanel.pastVisits.ItemPastVisits;
import com.hoplett.app.pojoClasses.leftPanel.upcomingVisits.ItemUpcomingPastVisit;
import com.hoplett.app.pojoClasses.leftPanel.upcomingVisits.ResultUpcomingPastVisit;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragPastOrders extends Fragment {

    RecyclerView recyclerView;
    public static AdaptrPastOrders adapter;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;
    GridLayoutManager layoutManager;
    ProgressBar progressLoader;
    String nextPage, previousPage;
    List<ResultUpcomingPastVisit> list;
    private ItemUpcomingPastVisit itemUpcomingPastVisit;
    LinearLayout layoutNoPast;

    public FragPastOrders() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_frag_past_orders, container, false);

        preferences = getActivity().getSharedPreferences(GlobalDataCls.PreferencesName,
                Context.MODE_PRIVATE);
        editor = preferences.edit();

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_vw_upcmng_orders);
        layoutNoPast = (LinearLayout) view.findViewById(R.id.no_product_past);
        progressLoader = (ProgressBar) view.findViewById(R.id.progress_loader);

        list = new ArrayList<>();
        itemUpcomingPastVisit = new ItemUpcomingPastVisit();

        layoutManager = new GridLayoutManager(getContext(), 1);
        adapter = new AdaptrPastOrders(getContext(), list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        String url = UrlAll.PastVisits + GlobalDataCls.latitude + "&long=" + GlobalDataCls.longitude;

        prepareList(url);
        paginationMethod();

        return view;
    }

    private void prepareList(String url) {

        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        String str;
        HashMap headers = new HashMap();
        str = preferences.getString(GlobalDataCls.toKen, null);
        headers.put("Authorization", "Token " + str);


        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            if (response.getInt("count") == 0) {
                                recyclerView.setVisibility(View.GONE);
                                layoutNoPast.setVisibility(View.VISIBLE);
                            } else {
                                recyclerView.setVisibility(View.VISIBLE);
                                layoutNoPast.setVisibility(View.GONE);
                                ItemPastVisits itemPastVisits = new Gson().fromJson(response.toString(),
                                        ItemPastVisits.class);

                                if (itemPastVisits != null) {

                                    if (itemPastVisits.getNext() != null && itemPastVisits.getNext() != "null" && itemPastVisits.getNext().toString().length() > 0) {

                                        nextPage = itemPastVisits.getNext().toString();
                                    } else {
                                        nextPage = null;
                                    }

                                    if (itemPastVisits.getResults() != null && itemPastVisits.getResults().size() > 0) {

                                        ResultUpcomingPastVisit itemVisit;

                                        for (int i = 0; i < itemPastVisits.getResults().size(); i++) {

                                            itemVisit = new ResultUpcomingPastVisit();
                                            itemVisit.setResId(itemPastVisits.getResults().get(i).getResId());
                                            itemVisit.setShopId(itemPastVisits.getResults().get(i).getShopId());
                                            itemVisit.setShopName(itemPastVisits.getResults().get(i).getShopName());
                                            itemVisit.setShopAddress(itemPastVisits.getResults().get(i).getShopAddress());
                                            itemVisit.setDistance(itemPastVisits.getResults().get(i).getDistance());
                                            itemVisit.setDate(itemPastVisits.getResults().get(i).getDate());
                                            itemVisit.setStatus(itemPastVisits.getResults().get(i).getStatus());
                                            itemVisit.setImage(itemPastVisits.getResults().get(i).getImage());
                                            itemVisit.setProductId(itemPastVisits.getResults().get(i).getProductId());

                                            list.add(itemVisit);
                                        }

                                        itemUpcomingPastVisit.setCount(itemPastVisits.getCount());
                                        itemUpcomingPastVisit.setNext(itemPastVisits.getNext());
                                        itemUpcomingPastVisit.setPrevious(itemPastVisits.getPrevious());
                                        itemUpcomingPastVisit.setResults(list);

                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                };
            }
        };

        apiCallMethods.volleyJSONObjectGET(getContext(), url, headers, dialog);
    }

    public void searchMethod(String str) {

        if (adapter != null) {
            if (adapter.getItemCount() > 0) {
                adapter.getFilter().filter(str);
            }
        }
    }


    private void paginationMethod() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {

                    isScrolling = false;
                    if (nextPage != null) {

                        progressLoader.setLayoutParams(new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.MATCH_PARENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM));

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressLoader.setVisibility(View.GONE);
                                prepareList(nextPage);
                            }
                        }, 300);
                    }
                }
            }
        });
    }
}
