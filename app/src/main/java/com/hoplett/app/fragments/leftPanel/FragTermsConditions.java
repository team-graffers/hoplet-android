package com.hoplett.app.fragments.leftPanel;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.hoplett.app.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragTermsConditions extends Fragment {


    public FragTermsConditions() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_terms_conditions, container, false);

        WebView webView = (WebView) view.findViewById(R.id.web_vw);
        webView.loadUrl("https://hoplett.com/terms-of-use/");

        // Enable Javascript
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // Force links and redirects to open in the WebView instead of in a browser
        webView.setWebViewClient(new WebViewClient());
        return view;
    }

}
