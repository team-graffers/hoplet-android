package com.hoplett.app.fragments.leftPanel;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Response;
import com.google.gson.Gson;
import com.hoplett.app.R;
import com.hoplett.app.activities.HopletHome;
import com.hoplett.app.adapters.leftPanel.AdaptrUpcomingOrders;
import com.hoplett.app.classes.ApiCallMethods;
import com.hoplett.app.classes.GlobalDataCls;
import com.hoplett.app.classes.UrlAll;
import com.hoplett.app.pojoClasses.leftPanel.upcomingVisits.ItemUpcomingPastVisit;
import com.hoplett.app.pojoClasses.leftPanel.upcomingVisits.ItemUpcomingVisit;
import com.hoplett.app.pojoClasses.leftPanel.upcomingVisits.ResultUpcomingPastVisit;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragUpcomingOrders extends Fragment {

    RecyclerView recyclerView;
    public static AdaptrUpcomingOrders adapter;
    TextView noOfStores;
    TextView addMoreOrders;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;
    GridLayoutManager layoutManager;
    ProgressBar progressLoader;
    String nextPage, previousPage;
    List<ResultUpcomingPastVisit> list;
    ItemUpcomingPastVisit itemUpcomingPastVisit;

    LinearLayout layoutUpcmng, layoutNoUpcmng;

    public FragUpcomingOrders() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_upcoming_orders, container, false);

        preferences = getActivity().getSharedPreferences(GlobalDataCls.PreferencesName,
                Context.MODE_PRIVATE);
        editor = preferences.edit();

        Log.d("token_token", preferences.getString(GlobalDataCls.toKen, null));

        layoutUpcmng = (LinearLayout) view.findViewById(R.id.upcming_layout);
        layoutNoUpcmng = (LinearLayout) view.findViewById(R.id.no_product_upcmng);

        noOfStores = (TextView) view.findViewById(R.id.no_of_stores);
        addMoreOrders = (TextView) view.findViewById(R.id.add_more_orders);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_vw_upcmng_orders);
        progressLoader = (ProgressBar) view.findViewById(R.id.progress_loader);

        list = new ArrayList<>();
        itemUpcomingPastVisit = new ItemUpcomingPastVisit();

        layoutManager = new GridLayoutManager(getContext(), 1);
        adapter = new AdaptrUpcomingOrders(getContext(), list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        String url = UrlAll.UpcomingVisits + GlobalDataCls.latitude + "&long=" + GlobalDataCls.longitude;

        prepareList(url);
        paginationMethod();

        addMoreOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), HopletHome.class);
                getActivity().startActivity(intent);
            }
        });
        return view;
    }

    private void prepareList(String url) {

        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_category_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();

        String str;
        HashMap headers = new HashMap();
        if (preferences.getString(GlobalDataCls.toKen, null) != null) {
            str = preferences.getString(GlobalDataCls.toKen, null);
            headers.put("Authorization", "Token " + str);
        } else {
            str = GlobalDataCls.toKenDummy;
            headers.put("Authorization", "Token " + str);
        }

        ApiCallMethods apiCallMethods = new ApiCallMethods() {
            @Override
            public Response.Listener<JSONObject> jsonObjectPostReqSuccListener() {
                return new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            if (response.getInt("count") == 0) {

                                layoutUpcmng.setVisibility(View.GONE);
                                layoutNoUpcmng.setVisibility(View.VISIBLE);
                            } else {
                                layoutUpcmng.setVisibility(View.VISIBLE);
                                layoutNoUpcmng.setVisibility(View.GONE);
                                ItemUpcomingVisit upcomingVisit = new Gson().fromJson(response.toString(),
                                        ItemUpcomingVisit.class);

                                if (upcomingVisit != null) {

                                    if (upcomingVisit.getNext() != null && upcomingVisit.getNext() != "null" && upcomingVisit.getNext().toString().length() > 0) {

                                        nextPage = upcomingVisit.getNext().toString();
                                    } else {
                                        nextPage = null;
                                    }

                                    if (upcomingVisit.getCount() > 0) {
                                        noOfStores.setText(upcomingVisit.getCount().toString());
                                    }

                                    if (upcomingVisit.getResults() != null && upcomingVisit.getResults().size() > 0) {

                                        ResultUpcomingPastVisit itemVisit;

                                        for (int i = 0; i < upcomingVisit.getResults().size(); i++) {

                                            itemVisit = new ResultUpcomingPastVisit();
                                            itemVisit.setResId(upcomingVisit.getResults().get(i).getResId());
                                            itemVisit.setShopId(upcomingVisit.getResults().get(i).getShopId());
                                            itemVisit.setShopName(upcomingVisit.getResults().get(i).getShopName());
                                            itemVisit.setShopAddress(upcomingVisit.getResults().get(i).getShopAddress());
                                            itemVisit.setDistance(upcomingVisit.getResults().get(i).getDistance());
                                            itemVisit.setDate(upcomingVisit.getResults().get(i).getDate());
                                            itemVisit.setStatus(upcomingVisit.getResults().get(i).getStatus());
                                            itemVisit.setImage(upcomingVisit.getResults().get(i).getImage());
                                            itemVisit.setProductId(upcomingVisit.getResults().get(i).getProductId());

                                            list.add(itemVisit);
                                        }

                                        itemUpcomingPastVisit.setCount(upcomingVisit.getCount());
                                        itemUpcomingPastVisit.setNext(upcomingVisit.getNext());
                                        itemUpcomingPastVisit.setPrevious(upcomingVisit.getPrevious());
                                        itemUpcomingPastVisit.setResults(list);

                                        adapter.notifyDataSetChanged();
                                    }
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                };
            }
        };
        apiCallMethods.volleyJSONObjectGET(getContext(), url, headers, dialog);
    }

    public void searchUpcomingOrdersMethod(String str) {

        if (adapter != null) {
            if (adapter.getItemCount() > 0) {
                adapter.getFilter().filter(str);
            }
        }
    }


    private void paginationMethod() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {

                    isScrolling = false;
                    if (nextPage != null) {

                        progressLoader.setLayoutParams(new FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.MATCH_PARENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM));

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressLoader.setVisibility(View.GONE);
                                prepareList(nextPage);
                            }
                        }, 300);
                    }
                }
            }
        });
    }
}
