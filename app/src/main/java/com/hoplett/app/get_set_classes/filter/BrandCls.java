package com.hoplett.app.get_set_classes.filter;

/**
 * Created by Sandy on 28-03-2019 at 05:40 PM.
 */
public class BrandCls {
	String brand;

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
}
