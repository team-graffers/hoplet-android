package com.hoplett.app.get_set_classes.filter;

/**
 * Created by Sandy on 28-03-2019 at 05:40 PM.
 */
public class CategoryCls {
	String category;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}
