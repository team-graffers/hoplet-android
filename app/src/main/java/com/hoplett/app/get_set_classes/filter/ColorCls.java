package com.hoplett.app.get_set_classes.filter;

/**
 * Created by Sandy on 28-03-2019 at 05:40 PM.
 */
public class ColorCls {
	String color;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
}
