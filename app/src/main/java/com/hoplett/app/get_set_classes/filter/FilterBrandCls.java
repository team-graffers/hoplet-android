package com.hoplett.app.get_set_classes.filter;

import java.util.List;

/**
 * Created by Sandy on 28-03-2019 at 05:39 PM.
 */
public class FilterBrandCls {
	String department;
	List<ColorCls> colorCls = null;
	List<CategoryCls> categoryCls = null;
	List<SizeCls> sizeCls = null;

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public List<ColorCls> getColorCls() {
		return colorCls;
	}

	public void setColorCls(List<ColorCls> colorCls) {
		this.colorCls = colorCls;
	}

	public List<CategoryCls> getCategoryCls() {
		return categoryCls;
	}

	public void setCategoryCls(List<CategoryCls> categoryCls) {
		this.categoryCls = categoryCls;
	}

	public List<SizeCls> getSizeCls() {
		return sizeCls;
	}

	public void setSizeCls(List<SizeCls> sizeCls) {
		this.sizeCls = sizeCls;
	}
}
