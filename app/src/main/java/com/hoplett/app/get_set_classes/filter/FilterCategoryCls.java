package com.hoplett.app.get_set_classes.filter;

import java.util.List;

/**
 * Created by Sandy on 28-03-2019 at 05:39 PM.
 */
public class FilterCategoryCls {
	String department;
	List<ColorCls> colorCls = null;
	List<BrandCls> brandCls = null;
	List<SizeCls> sizeCls = null;

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public List<ColorCls> getColorCls() {
		return colorCls;
	}

	public void setColorCls(List<ColorCls> colorCls) {
		this.colorCls = colorCls;
	}

	public List<BrandCls> getBrandCls() {
		return brandCls;
	}

	public void setBrandCls(List<BrandCls> brandCls) {
		this.brandCls = brandCls;
	}

	public List<SizeCls> getSizeCls() {
		return sizeCls;
	}

	public void setSizeCls(List<SizeCls> sizeCls) {
		this.sizeCls = sizeCls;
	}
}
