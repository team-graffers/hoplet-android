package com.hoplett.app.get_set_classes.filter;

import java.util.List;

/**
 * Created by Sandy on 28-03-2019 at 05:39 PM.
 */
public class FilterColorCls {
	String department;
	List<BrandCls> brands = null;
	List<CategoryCls> categories = null;
	List<SizeCls> sizes = null;

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public List<BrandCls> getBrandCls() {
		return brands;
	}

	public void setBrandCls(List<BrandCls> brandCls) {
		this.brands = brandCls;
	}

	public List<CategoryCls> getCategoryCls() {
		return categories;
	}

	public void setCategoryCls(List<CategoryCls> categoryCls) {
		this.categories = categoryCls;
	}

	public List<SizeCls> getSizeCls() {
		return sizes;
	}

	public void setSizeCls(List<SizeCls> sizeCls) {
		this.sizes = sizeCls;
	}
}
