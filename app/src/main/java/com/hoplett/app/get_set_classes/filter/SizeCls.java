package com.hoplett.app.get_set_classes.filter;

/**
 * Created by Sandy on 28-03-2019 at 05:40 PM.
 */
public class SizeCls {
	String size;

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}
}
