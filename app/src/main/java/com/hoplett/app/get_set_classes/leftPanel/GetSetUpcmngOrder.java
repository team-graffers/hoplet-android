package com.hoplett.app.get_set_classes.leftPanel;

public class GetSetUpcmngOrder {

	String storeName, address, distance, timeDate, status;
	int imgProduct;

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getTimeDate() {
		return timeDate;
	}

	public void setTimeDate(String timeDate) {
		this.timeDate = timeDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getImgProduct() {
		return imgProduct;
	}

	public void setImgProduct(int imgProduct) {
		this.imgProduct = imgProduct;
	}
}
