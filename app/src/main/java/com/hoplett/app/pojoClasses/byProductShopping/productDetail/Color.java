package com.hoplett.app.pojoClasses.byProductShopping.productDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Color {

	@SerializedName("product_id")
	@Expose
	private Integer productId;
	@SerializedName("color")
	@Expose
	private String color;

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
