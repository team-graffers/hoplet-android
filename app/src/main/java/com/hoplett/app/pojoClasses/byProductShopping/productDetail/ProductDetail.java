package com.hoplett.app.pojoClasses.byProductShopping.productDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Graffersid on 12-03-2019 at 05:44 PM.
 */

public class ProductDetail {

	@SerializedName("data")
	@Expose
	private Data data;
	@SerializedName("message")
	@Expose
	private String message;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}