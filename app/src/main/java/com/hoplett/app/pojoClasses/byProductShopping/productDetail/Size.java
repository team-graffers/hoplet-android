package com.hoplett.app.pojoClasses.byProductShopping.productDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Size {

	@SerializedName("sku_id")
	@Expose
	private Integer skuId;
	@SerializedName("number")
	@Expose
	private String number;
	@SerializedName("size")
	@Expose
	private String size;
	@SerializedName("price")
	@Expose
	private Integer price;
	@SerializedName("inv_price")
	@Expose
	private String invPrice;
	@SerializedName("in_shops")
	@Expose
	private Integer inShops;

	public Integer getSkuId() {
		return skuId;
	}

	public void setSkuId(Integer skuId) {
		this.skuId = skuId;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getInvPrice() {
		return invPrice;
	}

	public void setInvPrice(String invPrice) {
		this.invPrice = invPrice;
	}

	public Integer getInShops() {
		return inShops;
	}

	public void setInShops(Integer inShops) {
		this.inShops = inShops;
	}

}
