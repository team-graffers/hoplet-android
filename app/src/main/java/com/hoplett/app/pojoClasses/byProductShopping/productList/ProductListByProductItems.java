package com.hoplett.app.pojoClasses.byProductShopping.productList;

import java.util.List;

public class ProductListByProductItems {


    private Integer count;

    private String next;

    private Object previous;

    private List<ResultList> results = null;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public Object getPrevious() {
        return previous;
    }

    public void setPrevious(Object previous) {
        this.previous = previous;
    }

    public List<ResultList> getResults() {
        return results;
    }

    public void setResults(List<ResultList> results) {
        this.results = results;
    }

}

