package com.hoplett.app.pojoClasses.byProductShopping.productList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("sku_id")
    @Expose
    private Integer skuId;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("product")
    @Expose
    private String product;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("disc_price")
    @Expose
    private String discPrice;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("shop_count")
    @Expose
    private Integer shopCount;
    @SerializedName("fav")
    @Expose
    private Boolean fav;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDiscPrice() {
        return discPrice;
    }

    public void setDiscPrice(String discPrice) {
        this.discPrice = discPrice;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getShopCount() {
        return shopCount;
    }

    public void setShopCount(Integer shopCount) {
        this.shopCount = shopCount;
    }

    public Boolean getFav() {
        return fav;
    }

    public void setFav(Boolean fav) {
        this.fav = fav;
    }
}
