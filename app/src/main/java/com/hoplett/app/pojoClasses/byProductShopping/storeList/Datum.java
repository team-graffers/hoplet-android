package com.hoplett.app.pojoClasses.byProductShopping.storeList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

	@SerializedName("shop_id")
	@Expose
	private Integer shopId;
	@SerializedName("shop_name")
	@Expose
	private String shopName;
	@SerializedName("address")
	@Expose
	private String address;
	@SerializedName("org_price")
	@Expose
	private Integer orgPrice;
	@SerializedName("disc_price")
	@Expose
	private String discPrice;
	@SerializedName("distance")
	@Expose
	private Double distance;
	@SerializedName("image")
	@Expose
	private String image;

	public Integer getShopId() {
		return shopId;
	}

	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getOrgPrice() {
		return orgPrice;
	}

	public void setOrgPrice(Integer orgPrice) {
		this.orgPrice = orgPrice;
	}

	public String getDiscPrice() {
		return discPrice;
	}

	public void setDiscPrice(String discPrice) {
		this.discPrice = discPrice;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
}
