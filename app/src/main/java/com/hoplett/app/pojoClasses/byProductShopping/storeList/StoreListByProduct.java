package com.hoplett.app.pojoClasses.byProductShopping.storeList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandy on 28-03-2019 at 02:15 PM.
 */

public class StoreListByProduct {

	@SerializedName("data")
	@Expose
	private List<Datum> data = null;
	@SerializedName("message")
	@Expose
	private String message;

	public List<Datum> getData() {
		return data;
	}

	public void setData(List<Datum> data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}