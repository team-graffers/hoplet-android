package com.hoplett.app.pojoClasses.byStoreShopping.expVw.shopList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 19-03-2019 at 12:21 PM.
 */
public class Datum {

	 @SerializedName("shop_id")
	 @Expose
	 private Integer shopId;
	 @SerializedName("shop_name")
	 @Expose
	 private String shopName;
	 @SerializedName("address")
	 @Expose
	 private String address;
	 @SerializedName("product_count")
	 @Expose
	 private Integer productCount;
	 @SerializedName("distance")
	 @Expose
	 private Double distance;
	@SerializedName("image")
	@Expose
	private String image;

	 public Integer getShopId() {
			return shopId;
	 }

	 public void setShopId(Integer shopId) {
			this.shopId = shopId;
	 }

	 public String getShopName() {
			return shopName;
	 }

	 public void setShopName(String shopName) {
			this.shopName = shopName;
	 }

	 public String getAddress() {
			return address;
	 }

	 public void setAddress(String address) {
			this.address = address;
	 }

	 public Integer getProductCount() {
			return productCount;
	 }

	 public void setProductCount(Integer productCount) {
			this.productCount = productCount;
	 }

	 public Double getDistance() {
			return distance;
	 }

	 public void setDistance(Double distance) {
			this.distance = distance;
	 }

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
}
