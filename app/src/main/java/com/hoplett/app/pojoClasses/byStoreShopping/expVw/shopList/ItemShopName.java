package com.hoplett.app.pojoClasses.byStoreShopping.expVw.shopList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandy on 19-03-2019 at 12:19 PM.
 */

public class ItemShopName {

	 @SerializedName("data")
	 @Expose
	 private List<Datum> data = null;
	 @SerializedName("message")
	 @Expose
	 private String message;

	 public List<com.hoplett.app.pojoClasses.byStoreShopping.expVw.shopList.Datum> getData() {
			return data;
	 }

	 public void setData(List<Datum> data) {
			this.data = data;
	 }

	 public String getMessage() {
			return message;
	 }

	 public void setMessage(String message) {
			this.message = message;
	 }

}