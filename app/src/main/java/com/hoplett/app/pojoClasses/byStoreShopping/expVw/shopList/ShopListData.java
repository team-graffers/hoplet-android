package com.hoplett.app.pojoClasses.byStoreShopping.expVw.shopList;

/**
 * Created by Sandy on 19-03-2019 at 02:30 PM.
 */
public class ShopListData {

	 private Integer shopId;

	 private String shopName;

	 private String address;

	 private Integer productCount;

	 private Double distance;
	 String image;


	 public Integer getShopId() {
			return shopId;
	 }

	 public void setShopId(Integer shopId) {
			this.shopId = shopId;
	 }

	 public String getShopName() {
			return shopName;
	 }

	 public void setShopName(String shopName) {
			this.shopName = shopName;
	 }

	 public String getAddress() {
			return address;
	 }

	 public void setAddress(String address) {
			this.address = address;
	 }

	public Integer getProductCount() {
			return productCount;
	 }

	 public void setProductCount(Integer productCount) {
			this.productCount = productCount;
	 }

	 public Double getDistance() {
			return distance;
	 }

	 public void setDistance(Double distance) {
			this.distance = distance;
	 }

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
}
