package com.hoplett.app.pojoClasses.byStoreShopping.expVw.storeList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 19-03-2019 at 12:20 PM.
 */
public class Datum {

	 @SerializedName("store_id")
	 @Expose
	 private Integer storeId;
	 @SerializedName("name")
	 @Expose
	 private String name;
	 @SerializedName("image")
	 @Expose
	 private Object image;
	 @SerializedName("shop_count")
	 @Expose
	 private Integer shopCount;

	 public Integer getStoreId() {
			return storeId;
	 }

	 public void setStoreId(Integer storeId) {
			this.storeId = storeId;
	 }

	 public String getName() {
			return name;
	 }

	 public void setName(String name) {
			this.name = name;
	 }

	 public Object getImage() {
			return image;
	 }

	 public void setImage(Object image) {
			this.image = image;
	 }

	 public Integer getShopCount() {
			return shopCount;
	 }

	 public void setShopCount(Integer shopCount) {
			this.shopCount = shopCount;
	 }

}
