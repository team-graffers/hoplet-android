package com.hoplett.app.pojoClasses.byStoreShopping.expVw.storeList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandy on 19-03-2019 at 12:18 PM.
 */

public class ItemStoreName {

	 @SerializedName("data")
	 @Expose
	 private List<com.hoplett.app.pojoClasses.byStoreShopping.expVw.storeList.Datum> data = null;
	 @SerializedName("message")
	 @Expose
	 private String message;

	 public List<com.hoplett.app.pojoClasses.byStoreShopping.expVw.storeList.Datum> getData() {
			return data;
	 }

	 public void setData(List<Datum> data) {
			this.data = data;
	 }

	 public String getMessage() {
			return message;
	 }

	 public void setMessage(String message) {
			this.message = message;
	 }

}