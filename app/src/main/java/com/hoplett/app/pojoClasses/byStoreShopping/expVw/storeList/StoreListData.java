package com.hoplett.app.pojoClasses.byStoreShopping.expVw.storeList;

/**
 * Created by Sandy on 19-03-2019 at 01:44 PM.
 */
public class StoreListData {

	 private Integer storeId;

	 private String name;

	 private Object image;

	 private Integer shopCount;

	 public Integer getStoreId() {
			return storeId;
	 }

	 public void setStoreId(Integer storeId) {
			this.storeId = storeId;
	 }

	 public String getName() {
			return name;
	 }

	 public void setName(String name) {
			this.name = name;
	 }

	 public Object getImage() {
			return image;
	 }

	 public void setImage(Object image) {
			this.image = image;
	 }

	 public Integer getShopCount() {
			return shopCount;
	 }

	 public void setShopCount(Integer shopCount) {
			this.shopCount = shopCount;
	 }
}
