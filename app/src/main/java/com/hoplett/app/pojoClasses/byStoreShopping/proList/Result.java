
package com.hoplett.app.pojoClasses.byStoreShopping.proList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class Result {

    @Expose
    private String brand;
    @SerializedName("created_date")
    private String createdDate;
    @SerializedName("disc_price")
    private Integer discPrice;
    @Expose
    private Boolean fav;
    @Expose
    private String image;
    @Expose
    private Integer price;
    @Expose
    private String product;
    @SerializedName("product_id")
    private Integer productId;
    @SerializedName("sku_id")
    private Integer skuId;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getDiscPrice() {
        return discPrice;
    }

    public void setDiscPrice(Integer discPrice) {
        this.discPrice = discPrice;
    }

    public Boolean getFav() {
        return fav;
    }

    public void setFav(Boolean fav) {
        this.fav = fav;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

}
