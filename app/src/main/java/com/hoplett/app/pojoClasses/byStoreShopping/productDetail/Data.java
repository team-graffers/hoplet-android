package com.hoplett.app.pojoClasses.byStoreShopping.productDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Graffersid on 12-03-2019 at 05:45 PM.
 */
public class Data {

	 @SerializedName("parent_id")
	 @Expose
	 private Integer parentId;
	 @SerializedName("parent")
	 @Expose
	 private String parent;
	 @SerializedName("product_name")
	 @Expose
	 private String productName;
	 @SerializedName("Brand")
	 @Expose
	 private String brand;
	 @SerializedName("colors")
	 @Expose
	 private List<Color> colors = null;
	 @SerializedName("size")
	 @Expose
	 private List<Size> size = null;

	 public Integer getParentId() {
			return parentId;
	 }

	 public void setParentId(Integer parentId) {
			this.parentId = parentId;
	 }

	 public String getParent() {
			return parent;
	 }

	 public void setParent(String parent) {
			this.parent = parent;
	 }

	 public String getProductName() {
			return productName;
	 }

	 public void setProductName(String productName) {
			this.productName = productName;
	 }

	 public String getBrand() {
			return brand;
	 }

	 public void setBrand(String brand) {
			this.brand = brand;
	 }

	 public List<Color> getColors() {
			return colors;
	 }

	 public void setColors(List<Color> colors) {
			this.colors = colors;
	 }

	 public List<Size> getSize() {
			return size;
	 }

	 public void setSize(List<Size> size) {
			this.size = size;
	 }

}
