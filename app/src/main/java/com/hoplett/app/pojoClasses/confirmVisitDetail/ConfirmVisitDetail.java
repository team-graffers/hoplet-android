package com.hoplett.app.pojoClasses.confirmVisitDetail;

//
// Created by Sandy on 10-Jun-19 at 4:04 PM.
//

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConfirmVisitDetail {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("product_detail")
    @Expose
    private ProductDetail productDetail;
    @SerializedName("store_detail")
    @Expose
    private StoreDetail storeDetail;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public ProductDetail getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(ProductDetail productDetail) {
        this.productDetail = productDetail;
    }

    public StoreDetail getStoreDetail() {
        return storeDetail;
    }

    public void setStoreDetail(StoreDetail storeDetail) {
        this.storeDetail = storeDetail;
    }

}