package com.hoplett.app.pojoClasses.departmentPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Graffersid on 11-03-2019 at 10:21 AM.
 */
public class Datum {

	 @SerializedName("id")
	 @Expose
	 private Integer id;
	 @SerializedName("name")
	 @Expose
	 private String name;
	 @SerializedName("active")
	 @Expose
	 private Boolean active;
	 @SerializedName("image")
	 @Expose
	 private Object image;

	 public Integer getId() {
			return id;
	 }

	 public void setId(Integer id) {
			this.id = id;
	 }

	 public String getName() {
			return name;
	 }

	 public void setName(String name) {
			this.name = name;
	 }

	 public Boolean getActive() {
			return active;
	 }

	 public void setActive(Boolean active) {
			this.active = active;
	 }

	 public Object getImage() {
			return image;
	 }

	 public void setImage(Object image) {
			this.image = image;
	 }

}
