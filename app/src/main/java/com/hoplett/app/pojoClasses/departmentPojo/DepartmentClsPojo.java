package com.hoplett.app.pojoClasses.departmentPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Graffersid on 11-03-2019 at 10:20 AM.
 */

public class DepartmentClsPojo {

	 @SerializedName("data")
	 @Expose
	 private List<Datum> data = null;
	 @SerializedName("message")
	 @Expose
	 private String message;

	 public List<Datum> getData() {
			return data;
	 }

	 public void setData(List<Datum> data) {
			this.data = data;
	 }

	 public String getMessage() {
			return message;
	 }

	 public void setMessage(String message) {
			this.message = message;
	 }

}