package com.hoplett.app.pojoClasses.filter.catName;

/**
 * Created by Sandy on 3/4/19 at 7:11 PM.
 */
public class CatSelectedItem {

    String id;
    boolean bool;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isBool() {
        return bool;
    }

    public void setBool(boolean bool) {
        this.bool = bool;
    }
}
