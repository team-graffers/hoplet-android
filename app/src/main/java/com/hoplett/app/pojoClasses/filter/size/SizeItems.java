package com.hoplett.app.pojoClasses.filter.size;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hoplett.app.pojoClasses.filter.color.Datum;

import java.util.List;

/**
 * Created by Sandy on 29-03-2019 at 02:26 PM.
 */
public class SizeItems {
	@SerializedName("data")
	@Expose
	private List<Datum> data = null;
	@SerializedName("message")
	@Expose
	private String message;

	public List<Datum> getData() {
		return data;
	}

	public void setData(List<Datum> data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
