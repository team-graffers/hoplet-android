package com.hoplett.app.pojoClasses.leftPanel.upcomingVisits;

import java.util.List;

//
// Created by Sandy on 15-May-19 at 6:42 PM.
//
public class ItemUpcomingPastVisit {

    private Integer count;

    private Object next;

    private Object previous;

    private List<ResultUpcomingPastVisit> results = null;


    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Object getNext() {
        return next;
    }

    public void setNext(Object next) {
        this.next = next;
    }

    public Object getPrevious() {
        return previous;
    }

    public void setPrevious(Object previous) {
        this.previous = previous;
    }

    public List<ResultUpcomingPastVisit> getResults() {
        return results;
    }

    public void setResults(List<ResultUpcomingPastVisit> results) {
        this.results = results;
    }
}
