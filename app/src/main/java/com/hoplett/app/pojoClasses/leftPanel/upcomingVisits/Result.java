package com.hoplett.app.pojoClasses.leftPanel.upcomingVisits;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 18-03-2019 at 04:25 PM.
 */
public class Result {

	 @SerializedName("res_id")
	 @Expose
	 private Integer resId;
	 @SerializedName("shop_id")
	 @Expose
	 private Integer shopId;
	 @SerializedName("shop_name")
	 @Expose
	 private String shopName;
	 @SerializedName("shop_address")
	 @Expose
	 private String shopAddress;
	 @SerializedName("distance")
	 @Expose
	 private Double distance;
	 @SerializedName("date")
	 @Expose
	 private String date;
	 @SerializedName("status")
	 @Expose
	 private String status;
	 @SerializedName("image")
	 @Expose
	 private String image;
	 @SerializedName("product_id")
	 @Expose
	 private Integer productId;
	@SerializedName("product_name")
	@Expose
	private String productName;

	 public Integer getResId() {
			return resId;
	 }

	 public void setResId(Integer resId) {
			this.resId = resId;
	 }

	 public Integer getShopId() {
			return shopId;
	 }

	 public void setShopId(Integer shopId) {
			this.shopId = shopId;
	 }

	 public String getShopName() {
			return shopName;
	 }

	 public void setShopName(String shopName) {
			this.shopName = shopName;
	 }

	 public String getShopAddress() {
			return shopAddress;
	 }

	 public void setShopAddress(String shopAddress) {
			this.shopAddress = shopAddress;
	 }

	 public Double getDistance() {
			return distance;
	 }

	 public void setDistance(Double distance) {
			this.distance = distance;
	 }

	 public String getDate() {
			return date;
	 }

	 public void setDate(String date) {
			this.date = date;
	 }

	 public String getStatus() {
			return status;
	 }

	 public void setStatus(String status) {
			this.status = status;
	 }

	 public String getImage() {
			return image;
	 }

	 public void setImage(String image) {
			this.image = image;
	 }

	 public Integer getProductId() {
			return productId;
	 }

	 public void setProductId(Integer productId) {
			this.productId = productId;
	 }

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
}
