package com.hoplett.app.pojoClasses.leftPanel.viewProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 22-03-2019 at 06:55 PM.
 */
public class Data {

	 @SerializedName("image")
	 @Expose
	 private Object image;
	 @SerializedName("user_name")
	 @Expose
	 private String userName;
	 @SerializedName("user_no")
	 @Expose
	 private Object userNo;
	 @SerializedName("email")
	 @Expose
	 private String email;
	 @SerializedName("pin")
	 @Expose
	 private String pin;

	 public Object getImage() {
			return image;
	 }

	 public void setImage(Object image) {
			this.image = image;
	 }

	 public String getUserName() {
			return userName;
	 }

	 public void setUserName(String userName) {
			this.userName = userName;
	 }

	 public Object getUserNo() {
			return userNo;
	 }

	 public void setUserNo(Object userNo) {
			this.userNo = userNo;
	 }

	 public String getEmail() {
			return email;
	 }

	 public void setEmail(String email) {
			this.email = email;
	 }

	 public String getPin() {
			return pin;
	 }

	 public void setPin(String pin) {
			this.pin = pin;
	 }

}
