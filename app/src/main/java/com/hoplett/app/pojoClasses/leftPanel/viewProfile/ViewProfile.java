package com.hoplett.app.pojoClasses.leftPanel.viewProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 22-03-2019 at 06:54 PM.
 */

public class ViewProfile {

	 @SerializedName("data")
	 @Expose
	 private Data data;
	 @SerializedName("message")
	 @Expose
	 private String message;

	 public Data getData() {
			return data;
	 }

	 public void setData(Data data) {
			this.data = data;
	 }

	 public String getMessage() {
			return message;
	 }

	 public void setMessage(String message) {
			this.message = message;
	 }

}