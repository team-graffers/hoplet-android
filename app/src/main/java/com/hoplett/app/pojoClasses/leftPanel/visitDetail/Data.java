package com.hoplett.app.pojoClasses.leftPanel.visitDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

	@SerializedName("shop")
	@Expose
	private Shop shop;
	@SerializedName("prod")
	@Expose
	private Prod prod;

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Prod getProd() {
		return prod;
	}

	public void setProd(Prod prod) {
		this.prod = prod;
	}

}
