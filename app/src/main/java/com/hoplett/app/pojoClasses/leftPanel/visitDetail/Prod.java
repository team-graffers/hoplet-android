package com.hoplett.app.pojoClasses.leftPanel.visitDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Prod {

	@SerializedName("prod_name ")
	@Expose
	private String prodName;
	@SerializedName("brand")
	@Expose
	private String brand;
	@SerializedName("color")
	@Expose
	private String color;
	@SerializedName("price")
	@Expose
	private Integer price;
	@SerializedName("size")
	@Expose
	private String size;
	@SerializedName("prod_image ")
	@Expose
	private String prodImage;

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getProdImage() {
		return prodImage;
	}

	public void setProdImage(String prodImage) {
		this.prodImage = prodImage;
	}

}
