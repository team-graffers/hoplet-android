package com.hoplett.app.pojoClasses.leftPanel.visitDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shop {

	@SerializedName("shop_name")
	@Expose
	private String shopName;
	@SerializedName("shop_address")
	@Expose
	private String shopAddress;
	@SerializedName("distance")
	@Expose
	private Double distance;
	@SerializedName("time_from")
	@Expose
	private String timeFrom;
	@SerializedName("image")
	@Expose
	private String image;

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopAddress() {
		return shopAddress;
	}

	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public String getTimeFrom() {
		return timeFrom;
	}

	public void setTimeFrom(String timeFrom) {
		this.timeFrom = timeFrom;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
