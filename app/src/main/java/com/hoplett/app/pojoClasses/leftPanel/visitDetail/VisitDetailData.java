package com.hoplett.app.pojoClasses.leftPanel.visitDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 26-03-2019 at 07:05 PM.
 */

public class VisitDetailData {

	@SerializedName("data")
	@Expose
	private Data data;
	@SerializedName("message")
	@Expose
	private String message;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}