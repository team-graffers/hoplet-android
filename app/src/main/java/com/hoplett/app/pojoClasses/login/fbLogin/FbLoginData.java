package com.hoplett.app.pojoClasses.login.fbLogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 22-03-2019 at 04:54 PM.
 */

public class FbLoginData {

	 @SerializedName("id")
	 @Expose
	 private String id;
	 @SerializedName("name")
	 @Expose
	 private String name;
	 @SerializedName("email")
	 @Expose
	 private String email;
	 @SerializedName("picture")
	 @Expose
	 private Picture picture;

	 public String getId() {
			return id;
	 }

	 public void setId(String id) {
			this.id = id;
	 }

	 public String getName() {
			return name;
	 }

	 public void setName(String name) {
			this.name = name;
	 }

	 public String getEmail() {
			return email;
	 }

	 public void setEmail(String email) {
			this.email = email;
	 }

	 public Picture getPicture() {
			return picture;
	 }

	 public void setPicture(Picture picture) {
			this.picture = picture;
	 }

}
