package com.hoplett.app.pojoClasses.login.fbLogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 22-03-2019 at 04:55 PM.
 */
public class Picture {

	 @SerializedName("data")
	 @Expose
	 private Data data;

	 public Data getData() {
			return data;
	 }

	 public void setData(Data data) {
			this.data = data;
	 }

}
