package com.hoplett.app.pojoClasses.login.googleLogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandy on 22-03-2019 at 04:55 PM.
 */
public class GoogleLoginData {

	 @SerializedName("id")
	 @Expose
	 private String id;
	 @SerializedName("email")
	 @Expose
	 private String email;
	 @SerializedName("displayName")
	 @Expose
	 private String displayName;
	 @SerializedName("givenName")
	 @Expose
	 private String givenName;
	 @SerializedName("familyName")
	 @Expose
	 private String familyName;
	 @SerializedName("photoUrl")
	 @Expose
	 private String photoUrl;
	 @SerializedName("expirationTime")
	 @Expose
	 private Integer expirationTime;
	 @SerializedName("obfuscatedIdentifier")
	 @Expose
	 private String obfuscatedIdentifier;
	 @SerializedName("grantedScopes")
	 @Expose
	 private List<String> grantedScopes = null;

	 public String getId() {
			return id;
	 }

	 public void setId(String id) {
			this.id = id;
	 }

	 public String getEmail() {
			return email;
	 }

	 public void setEmail(String email) {
			this.email = email;
	 }

	 public String getDisplayName() {
			return displayName;
	 }

	 public void setDisplayName(String displayName) {
			this.displayName = displayName;
	 }

	 public String getGivenName() {
			return givenName;
	 }

	 public void setGivenName(String givenName) {
			this.givenName = givenName;
	 }

	 public String getFamilyName() {
			return familyName;
	 }

	 public void setFamilyName(String familyName) {
			this.familyName = familyName;
	 }

	 public String getPhotoUrl() {
			return photoUrl;
	 }

	 public void setPhotoUrl(String photoUrl) {
			this.photoUrl = photoUrl;
	 }

	 public Integer getExpirationTime() {
			return expirationTime;
	 }

	 public void setExpirationTime(Integer expirationTime) {
			this.expirationTime = expirationTime;
	 }

	 public String getObfuscatedIdentifier() {
			return obfuscatedIdentifier;
	 }

	 public void setObfuscatedIdentifier(String obfuscatedIdentifier) {
			this.obfuscatedIdentifier = obfuscatedIdentifier;
	 }

	 public List<String> getGrantedScopes() {
			return grantedScopes;
	 }

	 public void setGrantedScopes(List<String> grantedScopes) {
			this.grantedScopes = grantedScopes;
	 }

}