package com.hoplett.app.pojoClasses.res_cls;

//
// Created by Sandy on 10-Jun-19 at 5:33 PM.
//


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResCls {

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("message")
    @Expose
    private String message;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}