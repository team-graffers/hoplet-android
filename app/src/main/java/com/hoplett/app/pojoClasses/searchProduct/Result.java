package com.hoplett.app.pojoClasses.searchProduct;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {

    @SerializedName("size")
    @Expose
    private Size size;
    @SerializedName("product")
    @Expose
    private Product product;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("parent_sku")
    @Expose
    private String parentSku;
    @SerializedName("inventory")
    @Expose
    private List<Inventory> inventory = null;
    @SerializedName("price")
    @Expose
    private Integer price;

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getParentSku() {
        return parentSku;
    }

    public void setParentSku(String parentSku) {
        this.parentSku = parentSku;
    }

    public List<Inventory> getInventory() {
        return inventory;
    }

    public void setInventory(List<Inventory> inventory) {
        this.inventory = inventory;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

}
