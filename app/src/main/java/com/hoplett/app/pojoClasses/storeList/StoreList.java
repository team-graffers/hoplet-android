
package com.hoplett.app.pojoClasses.storeList;

import java.util.List;
import com.google.gson.annotations.Expose;

public class StoreList {

    @Expose
    private Long count;
    @Expose
    private List<Datum> data;
    @Expose
    private String message;

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
