package com.hoplett.app.pojoClasses.wishlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Datum {

	@SerializedName("sku_id")
	@Expose
	private Integer skuId;
	@SerializedName("parent_id")
	@Expose
	private Integer parentId;
	@SerializedName("brand")
	@Expose
	private String brand;
	@SerializedName("product_id")
	@Expose
	private Integer productId;
	@SerializedName("product_name")
	@Expose
	private String productName;
	@SerializedName("parent_name")
	@Expose
	private String parentName;
	@SerializedName("price")
	@Expose
	private Integer price;
	@SerializedName("image")
	@Expose
	private String image;
	@SerializedName("shop_count")
	@Expose
	private Integer shopCount;
	@SerializedName("size")
	@Expose
	private List<Size> size = null;

	public Integer getSkuId() {
		return skuId;
	}

	public void setSkuId(Integer skuId) {
		this.skuId = skuId;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Integer getShopCount() {
		return shopCount;
	}

	public void setShopCount(Integer shopCount) {
		this.shopCount = shopCount;
	}

	public List<Size> getSize() {
		return size;
	}

	public void setSize(List<Size> size) {
		this.size = size;
	}

}
