package com.hoplett.app.pojoClasses.wishlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandy on 18-03-2019 at 10:16 AM.
 */

public class ItemWishListPojo {

	@SerializedName("data")
	@Expose
	private List<Datum> data = null;
	@SerializedName("count")
	@Expose
	private Integer count;
	@SerializedName("message")
	@Expose
	private String message;

	public List<Datum> getData() {
		return data;
	}

	public void setData(List<Datum> data) {
		this.data = data;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
