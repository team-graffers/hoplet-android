package com.hoplett.app.pojoClasses.wishlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Size {

	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("sku_id")
	@Expose
	private Integer skuId;
	@SerializedName("number")
	@Expose
	private String number;
	@SerializedName("price")
	@Expose
	private Integer price;
	@SerializedName("inv_price")
	@Expose
	private String invPrice;
	@SerializedName("in_shop")
	@Expose
	private Integer inShop;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSkuId() {
		return skuId;
	}

	public void setSkuId(Integer skuId) {
		this.skuId = skuId;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getInvPrice() {
		return invPrice;
	}

	public void setInvPrice(String invPrice) {
		this.invPrice = invPrice;
	}

	public Integer getInShop() {
		return inShop;
	}

	public void setInShop(Integer inShop) {
		this.inShop = inShop;
	}

}
